import React from 'react';
import DefaultNavbar from 'components/navbars/DefaultNavbar';
import { makeStyles, createStyles } from '@material-ui/styles';
import { Theme, Container } from '@material-ui/core';

const HomePage = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <DefaultNavbar />
      <main className={classes.content}>
        <Container className={classes.container}>Home page</Container>
      </main>
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    content: {
      flexGrow: 1,
      marginTop: 49,
      height: 'calc(100vh - 49px)',
      overflowY: 'auto',
    },
    container: {
      height: '100%',
      backgroundColor: theme.palette.background.default,
      paddingTop: 24,
      paddingBottom: 24,
    },
  })
);

export default HomePage;
