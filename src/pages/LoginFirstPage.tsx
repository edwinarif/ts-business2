import React from 'react';
import { useSelector } from 'react-redux';
import { AppState } from 'index';
import DefaultNavbar from 'components/navbars/DefaultNavbar';
import { FirebaseState } from 'types';

const LoginFirstPage = () => {
  const firebaseReducer = useSelector<AppState, FirebaseState>(
    state => state.firebaseReducer as FirebaseState
  );
  const user = firebaseReducer.auth;

  return (
    <>
      <DefaultNavbar />
      {user.isEmpty && <div>Login first</div>}
    </>
  );
};

export default LoginFirstPage;
