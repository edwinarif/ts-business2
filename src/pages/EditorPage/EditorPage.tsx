import React, { useEffect, useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';
import DefaultNavbar from 'components/navbars/DefaultNavbar';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentPage, setKeyValue } from 'redux/actions/localActions';
import PropsManagerCard from 'components/cards/PropsManagerCard';
import MainNavCard from 'components/cards/MainNavCard';
import EditorBottomNavbar from 'components/navbars/EditorBottomNavbar';
import { AppState } from 'index';
import { useFirebase, useFirebaseConnect } from 'react-redux-firebase';
import { IPage, RenderedComponentType, CustomComponentType } from 'types';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import Editor from 'components/editorDnd/Editor';
import at from 'lodash/at';
import { getBlockList } from 'blockConfig/componentsMapper';
import { staticBlockToComponent } from 'utils/staticBlockToComponent';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';
import { parseBlockComponent } from 'utils/parseBlockComponent';
import { config } from 'themes/template1';
// import { useInitManualDataCreation } from './useInitManualDataCreation';
import './styles.css';
import isEmpty from 'lodash/isEmpty';
import { parseCustomComponent } from 'utils/parseCustomComponent';
import FileNameTabs from 'components/tabs/FileNameTabs';
import { LocalReducerType } from 'redux/reducers/localReducer';

const EditorPage = () => {
  const firebase = useFirebase();
  const classes = useStyles();
  const dispatch = useDispatch();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const {
    currentProjectId,
    currentPageId,
    project,
    page,
    renderedBlockComponents,
  } = localReducer;

  const [currentPageTheme, setCurrentPageTheme] = useState<
    string | undefined
  >();
  const [currentThemeConfig, setCurrentThemeConfig] = useState(config);
  const [isReloadBlocks, setIsReloadBlocks] = useState(false);
  const [isReloadCustomBlocks, setIsReloadCustomBlocks] = useState(false);

  if (!currentProjectId) {
    window.location.href = '/projects';
  }

  // Manual Create Themes if not in database, COMMENT THIS CODE AFTER USE
  // useInitManualDataCreation();

  // Listen to projects
  useEffect(() => {
    if (currentProjectId) {
      firebase.ref(`projects/${currentProjectId}`).on('value', snapshot => {
        dispatch(setKeyValue({ key: 'project', value: snapshot.val() }));
      });
    }

    return function cleanup() {
      if (currentProjectId) {
        firebase.ref(`projects/${currentProjectId}`).off('value');
      }
    };
  }, [dispatch, currentProjectId, firebase]);

  // Create a default page (Home) if no pages found
  useEffect(() => {
    if (project && project.name && !project.pages) {
      const page: IPage = {
        name: 'Home',
        themeId: project.defaultThemeId || 'theme1',
        createdAt: Date.now(),
        updatedAt: Date.now(),
      };
      firebase.push(`projects/${currentProjectId}/pages/`, page).then(res => {
        if (res.key) dispatch(setCurrentPage(res.key, page));
      });
    }
  }, [dispatch, firebase, project, currentProjectId]);

  // Load current page
  useEffect(() => {
    if (currentPageId) {
      const page = at<any>(project, `pages.${currentPageId}`)[0];
      dispatch(setCurrentPage(currentPageId, page));
    }
  }, [dispatch, project, currentPageId]);

  useFirebaseConnect(
    (page && page.blockIds ? Object.keys(page.blockIds) : []).map(
      blockId => `blocks/${blockId}`
    )
  );

  // Load Theme config
  useEffect(() => {
    if (!page) return;
    if (page.themeId === currentPageTheme) return;
    const themRef = firebase.ref(`/themes/${page.themeId}/config`);

    themRef.once('value', function(snapshot) {
      const customThemeConfig = snapshot.exists ? snapshot.val() : config;
      setCurrentThemeConfig(customThemeConfig);
      setCurrentPageTheme(page.themeId);
      setIsReloadBlocks(true);
      setIsReloadCustomBlocks(true);
    });
  }, [firebase, page, currentPageTheme]);

  // Load static component blocks
  useEffect(() => {
    if (!page || !currentPageTheme) return;
    if (!isReloadBlocks) return;

    const staticBlocks = getBlockList(['icons']);
    const renderedBlockComponents = Object.keys(staticBlocks).reduce(
      (res, blockId) => {
        const component = staticBlockToComponent(staticBlocks[blockId]);
        if (component && getIsBlockComponent(component)) {
          const block = parseBlockComponent(component, currentThemeConfig);
          return { ...res, [blockId]: block as RenderedComponentType };
        }

        return res;
      },
      {} as { [key: string]: RenderedComponentType }
    );
    dispatch(
      setKeyValue({
        key: 'renderedBlockComponents',
        value: renderedBlockComponents,
      })
    );
    setIsReloadBlocks(false);
  }, [
    dispatch,
    firebase,
    page,
    currentPageTheme,
    isReloadBlocks,
    currentThemeConfig,
  ]);

  // Load Rendered Custom Components
  useEffect(() => {
    if (!project || !currentPageTheme) return;
    if (!isReloadCustomBlocks) return;

    const { customComponents } = project;
    if (!customComponents || isEmpty(customComponents)) return;

    const renderedCustomComponents = Object.keys(customComponents).reduce(
      (res, customComponentId) => {
        const customComponent = {
          id: customComponentId,
          ...customComponents[customComponentId],
        } as CustomComponentType;
        const renderedComponent = parseCustomComponent({
          customComponent,
          themeConfig: currentThemeConfig,
          customComponents: project.customComponents,
        });
        return {
          ...res,
          [customComponentId]: renderedComponent,
        };
      },
      {} as { [key: string]: RenderedComponentType }
    );

    dispatch(
      setKeyValue({
        key: 'renderedCustomComponents',
        value: renderedCustomComponents,
      })
    );
    setIsReloadCustomBlocks(false);
  }, [
    dispatch,
    firebase,
    project,
    isReloadCustomBlocks,
    currentPageTheme,
    currentThemeConfig,
  ]);

  if (!currentPageTheme || isEmpty(renderedBlockComponents)) return null;

  return (
    <DndProvider backend={HTML5Backend}>
      <div className={classes.root}>
        <DefaultNavbar />
        <MainNavCard />
        <main className={classes.content}>
          <FileNameTabs />
          {project && project.name && <Editor />}
        </main>
        <PropsManagerCard />
      </div>
      <EditorBottomNavbar />
    </DndProvider>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      marginTop: 49,
      marginLeft: 89,
      marginRight: 287,
      minHeight: 'calc(100vh - 84px)',
    },
  })
);

export default EditorPage;
