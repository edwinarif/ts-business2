import { useEffect } from 'react';
import { useFirebase } from 'react-redux-firebase';
import { config as mainSiteConfig } from 'themes/mainSite';
import { config as template1Config } from 'themes/template1';

export const useInitManualDataCreation = () => {
  const firebase = useFirebase();

  // Manual Theme Creation - /themes/{themeId}
  useEffect(() => {
    firebase.ref('themes').set({
      theme1: {
        config: mainSiteConfig,
        name: 'Theme 1',
        ownerId: 'QIo6hXbufDSDMIqhEiaLCCVkAw92',
      },
      theme2: {
        config: template1Config,
        name: 'Theme 2',
        ownerId: 'QIo6hXbufDSDMIqhEiaLCCVkAw92',
      },
    });
  }, [firebase]);
};
