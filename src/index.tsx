import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import { Provider } from 'react-redux';
import { createStore, compose, combineReducers } from 'redux';
import theme from 'themes/mainSite';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import * as firebaseui from 'firebaseui';
import App from './App';
import * as serviceWorker from './serviceWorker';
import localReducer from 'redux/reducers/localReducer';
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import { firebaseConfig, reduxFirebase } from './firebase/config';
import { firebaseReducer } from 'react-redux-firebase';
import { HookedBrowserRouter } from 'components/HookedBrowserRouter';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { FirebaseState } from 'types';

// Set redux
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}
const composeEnhancers =
  process.env.NODE_ENV === 'production'
    ? undefined
    : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rootReducer = combineReducers({
  localReducer,
  firebaseReducer,
});
const store = createStore(
  rootReducer,
  composeEnhancers ? composeEnhancers() : undefined
);

// redux root type
export type AppState = {
  localReducer: LocalReducerType;
  firebaseReducer: FirebaseState;
};

// Set firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
export const firebaseAuth = firebaseApp.auth();
export const authui = new firebaseui.auth.AuthUI(firebaseAuth);
firebaseAuth.setPersistence(
  process.env.NODE_ENV === 'test'
    ? firebase.auth.Auth.Persistence.NONE
    : firebase.auth.Auth.Persistence.LOCAL
);

ReactDOM.render(
  <Provider store={store}>
    <ReactReduxFirebaseProvider
      firebase={firebase}
      config={reduxFirebase}
      dispatch={store.dispatch}
    >
      <HookedBrowserRouter>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <App />
        </ThemeProvider>
      </HookedBrowserRouter>
    </ReactReduxFirebaseProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
