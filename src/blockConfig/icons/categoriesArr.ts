export default {
  action: {
    title: 'Action',
    icons: [
      {
        id: 'lQ5EPXa2',
        name: 'ThreeDRotation',
      },
      {
        id: 'fkwl2MTwx',
        name: 'Accessibility',
      },
      {
        id: 'w59IEkRbc',
        name: 'AccessibilityNew',
      },
      {
        id: '5aJel0eOP',
        name: 'Accessible',
      },
      {
        id: 'Pq71ty31s',
        name: 'AccessibleForward',
      },
      {
        id: 'TD$BFPJES',
        name: 'AccountBalance',
      },
      {
        id: 'zu7te5sKr',
        name: 'AccountBalanceWallet',
      },
      {
        id: '3P15BgsMt',
        name: 'AccountBox',
      },
      {
        id: 'CGvO1LWkC',
        name: 'AccountCircle',
      },
      {
        id: 'IwNQyc49w',
        name: 'AddShoppingCart',
      },
      {
        id: 'juTeDTGgQ',
        name: 'Alarm',
      },
      {
        id: 'DWSnwbLC1',
        name: 'AlarmAdd',
      },
      {
        id: 'nuNhggDQ0',
        name: 'AlarmOff',
      },
      {
        id: '6y3JijYiW',
        name: 'AlarmOn',
      },
      {
        id: 'gSq0aIntH',
        name: 'AllInbox',
      },
      {
        id: 'oEieIDleW',
        name: 'AllOut',
      },
      {
        id: 'ZTUiP7Qkqo',
        name: 'Android',
      },
      {
        id: 'HhL3Y7QXqv',
        name: 'Announcement',
      },
      {
        id: 'DnJKuo3TC5',
        name: 'ArrowRightAlt',
      },
      {
        id: 'vOcwKKPg3A',
        name: 'AspectRatio',
      },
      {
        id: 'RcFFXQ0v8$',
        name: 'Assessment',
      },
      {
        id: 'XzL4BEJeeQ',
        name: 'Assignment',
      },
      {
        id: 'lh5qL_8y8p',
        name: 'AssignmentInd',
      },
      {
        id: 'sD4StapBJs',
        name: 'AssignmentLate',
      },
      {
        id: 'NxdwUMP20a',
        name: 'AssignmentReturn',
      },
      {
        id: 'Y3ojSSgO8n',
        name: 'AssignmentReturned',
      },
      {
        id: 'QP983nQQaa',
        name: 'AssignmentTurnedIn',
      },
      {
        id: 'msNjJEr8Hg',
        name: 'Autorenew',
      },
      {
        id: 'mHDg668FcT',
        name: 'Backup',
      },
      {
        id: '9E0mrdV$zt',
        name: 'Book',
      },
      {
        id: 'Mx09ZIOm6x',
        name: 'Bookmark',
      },
      {
        id: '32HSk4AE$z',
        name: 'BookmarkBorder',
      },
      {
        id: 'IFY8iencxb',
        name: 'Bookmarks',
      },
      {
        id: '5CdojeUVFF',
        name: 'BugReport',
      },
      {
        id: 'EEH5_wHsst',
        name: 'Build',
      },
      {
        id: 'XBmZP0UrAj',
        name: 'Cached',
      },
      {
        id: '5ma6Q53xt9',
        name: 'CalendarToday',
      },
      {
        id: 'HZ9qyITsaH',
        name: 'CalendarViewDay',
      },
      {
        id: 'O1XnbreQ_5',
        name: 'CameraEnhance',
      },
      {
        id: 'azYFrTm3sm',
        name: 'CancelScheduleSend',
      },
      {
        id: 'q5BXH2CZWY',
        name: 'CardGiftcard',
      },
      {
        id: 'eIZL67zFJv',
        name: 'CardMembership',
      },
      {
        id: 'ESFfAT8SJ6',
        name: 'CardTravel',
      },
      {
        id: 'h0dzD9NJ9Q',
        name: 'ChangeHistory',
      },
      {
        id: 'wnSNcqoJw6',
        name: 'CheckCircle',
      },
      {
        id: 'ou9ByI8gpp',
        name: 'CheckCircleOutline',
      },
      {
        id: 'KoWYbEXFh2',
        name: 'ChromeReaderMode',
      },
      {
        id: 'e3A60ldYT2',
        name: 'Class',
      },
      {
        id: 'MB_0FfFx6C',
        name: 'Code',
      },
      {
        id: 'IiEVIaW5OT',
        name: 'Commute',
      },
      {
        id: 'y2QfQIIzSC',
        name: 'CompareArrows',
      },
      {
        id: '0YU6E1NNzn',
        name: 'ContactSupport',
      },
      {
        id: 'P7ThYU9yCm',
        name: 'Contactless',
      },
      {
        id: 'Cmp7_WREMS',
        name: 'Copyright',
      },
      {
        id: 'CkLb2QrtHh',
        name: 'CreditCard',
      },
      {
        id: '6RW0FJMKWv',
        name: 'Dashboard',
      },
      {
        id: '1ghK2pxcTR',
        name: 'DateRange',
      },
      {
        id: '$psjII9oqz',
        name: 'Delete',
      },
      {
        id: 'ElfCYtPY1W',
        name: 'DeleteForever',
      },
      {
        id: 'et0tj9i_Pg',
        name: 'DeleteOutline',
      },
      {
        id: 'ckR0X29rfK',
        name: 'Description',
      },
      {
        id: 'c9S8uTHZ7w',
        name: 'Dns',
      },
      {
        id: 'CBkkT$H84h',
        name: 'Done',
      },
      {
        id: 'LloeqGsRLL',
        name: 'DoneAll',
      },
      {
        id: 'Alduh2Qbff',
        name: 'DoneOutline',
      },
      {
        id: '27JS04Q9iv',
        name: 'DonutLarge',
      },
      {
        id: '$nqnnqoTV0',
        name: 'DonutSmall',
      },
      {
        id: 'PrwWu0sA9S',
        name: 'DragIndicator',
      },
      {
        id: 'SIdxrEEQJU',
        name: 'Eco',
      },
      {
        id: 'yf33VPYc$A',
        name: 'Eject',
      },
      {
        id: 'lYlo0Ffmf1',
        name: 'EuroSymbol',
      },
      {
        id: '3OCkmHeH2X',
        name: 'Event',
      },
      {
        id: '399Kp$yl50',
        name: 'EventSeat',
      },
      {
        id: '79NrPGHfuD',
        name: 'ExitToApp',
      },
      {
        id: 'Tqfmd4p7q7',
        name: 'Explore',
      },
      {
        id: '3kKm0sm0Jn',
        name: 'ExploreOff',
      },
      {
        id: 'ifuVF0dZMm',
        name: 'Extension',
      },
      {
        id: 'a9Uwo_WyFA',
        name: 'Face',
      },
      {
        id: '3nLupn4E$L',
        name: 'Favorite',
      },
      {
        id: 'RydUFVJdLB',
        name: 'FavoriteBorder',
      },
      {
        id: 'P6JGK0iVVW',
        name: 'Feedback',
      },
      {
        id: 'iitzySE6Yp',
        name: 'FindInPage',
      },
      {
        id: 'QlKUP1VPnZ',
        name: 'FindReplace',
      },
      {
        id: 'ylH1BmogmS',
        name: 'Fingerprint',
      },
      {
        id: 'JHMI$8998d',
        name: 'FlightLand',
      },
      {
        id: 'Z4oF4nWgXt',
        name: 'FlightTakeoff',
      },
      {
        id: 'HnPf98iASt',
        name: 'FlipToBack',
      },
      {
        id: 'gcm1UD1h7Z',
        name: 'FlipToFront',
      },
      {
        id: 'TUNKcXcOL3',
        name: 'GTranslate',
      },
      {
        id: 'm_eQzforEy',
        name: 'Gavel',
      },
      {
        id: 'cHP7VXKGNt',
        name: 'GetApp',
      },
      {
        id: 'QYD4HQCwVN',
        name: 'Gif',
      },
      {
        id: '5aXnyFSTsE',
        name: 'Grade',
      },
      {
        id: 'pnsk455qAE',
        name: 'GroupWork',
      },
      {
        id: '6qK_rkDcft',
        name: 'Help',
      },
      {
        id: 'Hsd9Au_rL6',
        name: 'HelpOutline',
      },
      {
        id: '03uLAHbkY2',
        name: 'HighlightOff',
      },
      {
        id: 'j69cH_6gdB',
        name: 'History',
      },
      {
        id: 'CIDLogaNP3',
        name: 'Home',
      },
      {
        id: '0J_2DjnhW9',
        name: 'HorizontalSplit',
      },
      {
        id: 'bZij9vYLVq',
        name: 'HourglassEmpty',
      },
      {
        id: 'vzKEj4Z7X2',
        name: 'HourglassFull',
      },
      {
        id: 'DCzx8KWMSH',
        name: 'Http',
      },
      {
        id: 'ImdHQoKxVW',
        name: 'Https',
      },
      {
        id: 'nRL4FBzBlL',
        name: 'ImportantDevices',
      },
      {
        id: '$g84VPBPSt',
        name: 'Info',
      },
      {
        id: 'fQrmKr67mm',
        name: 'Input',
      },
      {
        id: '$ZrMTYmCjJ',
        name: 'InvertColors',
      },
      {
        id: 'kjBWftpiFp',
        name: 'Label',
      },
      {
        id: 'GKOrPbw8mX',
        name: 'LabelImportant',
      },
      {
        id: 'QfibY1ogM1',
        name: 'LabelOff',
      },
      {
        id: 'WbDAjqfYaU',
        name: 'Language',
      },
      {
        id: 'g5kxBS48eq',
        name: 'Launch',
      },
      {
        id: 'pdOGC7F9pL',
        name: 'LineStyle',
      },
      {
        id: 'xjnTOAEs$I',
        name: 'LineWeight',
      },
      {
        id: 'NK1Gem_pHC',
        name: 'List',
      },
      {
        id: 'quXIQRVLah',
        name: 'Lock',
      },
      {
        id: 'duTVJNjV7U',
        name: 'LockOpen',
      },
      {
        id: 'mhSzCtCrsq',
        name: 'Loyalty',
      },
      {
        id: 'efvLTjEYQ9',
        name: 'MarkunreadMailbox',
      },
      {
        id: 'b2t1Cmv43n',
        name: 'Maximize',
      },
      {
        id: 'FGM5ImYKxx',
        name: 'Minimize',
      },
      {
        id: 'b$ngl1iKky',
        name: 'Motorcycle',
      },
      {
        id: 'k04a5p6RI2',
        name: 'NoteAdd',
      },
      {
        id: 'RqbhfNBWBg',
        name: 'OfflineBolt',
      },
      {
        id: '0tWvYO3eU0',
        name: 'OfflinePin',
      },
      {
        id: 'jXojXv2fjN',
        name: 'Opacity',
      },
      {
        id: 'WjRswxM459',
        name: 'OpenInBrowser',
      },
      {
        id: '9_2AP5i6Uf',
        name: 'OpenInNew',
      },
      {
        id: 'vqf$AO5EI9',
        name: 'OpenWith',
      },
      {
        id: 'L68Dydq_Je',
        name: 'Pageview',
      },
      {
        id: '_ALGskRh$K',
        name: 'PanTool',
      },
      {
        id: 'HQqgS79m7o',
        name: 'Payment',
      },
      {
        id: 'cey3bpNNv1',
        name: 'PermCameraMic',
      },
      {
        id: 'Q2DkJ7oEwR',
        name: 'PermContactCalendar',
      },
      {
        id: 'IBGMZViLI5',
        name: 'PermDataSetting',
      },
      {
        id: 'aX2NJ8uYSd',
        name: 'PermDeviceInformation',
      },
      {
        id: 'TV1vnUzUmo',
        name: 'PermIdentity',
      },
      {
        id: '4SIaqBrWUN',
        name: 'PermMedia',
      },
      {
        id: 'qNjmwzxBjW',
        name: 'PermPhoneMsg',
      },
      {
        id: '0cDeUeRm96',
        name: 'PermScanWifi',
      },
      {
        id: 'y7OTafw6FX',
        name: 'Pets',
      },
      {
        id: 'KKmt7xSbiE',
        name: 'PictureInPicture',
      },
      {
        id: 'Rb_vzHTRgi',
        name: 'PictureInPictureAlt',
      },
      {
        id: 'eaWiMW5_fR',
        name: 'PlayForWork',
      },
      {
        id: 'lI2TwZhF9H',
        name: 'Polymer',
      },
      {
        id: 'A$ohdpGQfb',
        name: 'PowerSettingsNew',
      },
      {
        id: 'gReLTXgJaO',
        name: 'PregnantWoman',
      },
      {
        id: 'RXbklmGdD4',
        name: 'Print',
      },
      {
        id: 'TwalZCKDHF',
        name: 'QueryBuilder',
      },
      {
        id: 'JO2lTzgOag',
        name: 'QuestionAnswer',
      },
      {
        id: 'ZBWumafoJN',
        name: 'Receipt',
      },
      {
        id: 'rrOuM5jTNS',
        name: 'RecordVoiceOver',
      },
      {
        id: 'J8oOVL515s',
        name: 'Redeem',
      },
      {
        id: '68bx26A9IK',
        name: 'RemoveShoppingCart',
      },
      {
        id: 'AZwYEksyGN',
        name: 'Reorder',
      },
      {
        id: 'nDOqD18aZz',
        name: 'ReportProblem',
      },
      {
        id: 'nwEnlBDdjt',
        name: 'Restore',
      },
      {
        id: '$n4DogzLgU',
        name: 'RestoreFromTrash',
      },
      {
        id: 'BDoaGhKfgL',
        name: 'RestorePage',
      },
      {
        id: 'tMGMeUHtoX',
        name: 'Room',
      },
      {
        id: 'v6m$mT$HjZ',
        name: 'RoundedCorner',
      },
      {
        id: 'VjY_3RDlzB',
        name: 'Rowing',
      },
      {
        id: 'E5aZAQQ40p',
        name: 'Schedule',
      },
      {
        id: 'h5GEaf2$Zg',
        name: 'Search',
      },
      {
        id: 'osxhvcgz9I',
        name: 'SettingsApplications',
      },
      {
        id: '8_KZT7Pdfd',
        name: 'SettingsBackupRestore',
      },
      {
        id: 'HKg3SrD9vk',
        name: 'SettingsBluetooth',
      },
      {
        id: '951cvR2$NY',
        name: 'SettingsBrightness',
      },
      {
        id: '$Jknqxwi13',
        name: 'SettingsCell',
      },
      {
        id: '36IKWyXQUu',
        name: 'SettingsEthernet',
      },
      {
        id: 'OE3XGX$biw',
        name: 'SettingsInputAntenna',
      },
      {
        id: 'dCrFTHaoMv',
        name: 'SettingsInputComponent',
      },
      {
        id: 'jEfdIutYIi',
        name: 'SettingsInputComposite',
      },
      {
        id: 'dViuvk8X3c',
        name: 'SettingsInputHdmi',
      },
      {
        id: 'sE5V_FIp5p',
        name: 'SettingsInputSvideo',
      },
      {
        id: '4ScQao5nFV',
        name: 'SettingsOverscan',
      },
      {
        id: 'Uc7WDrbIjo',
        name: 'SettingsPhone',
      },
      {
        id: 'vsi4aX4L9X',
        name: 'SettingsPower',
      },
      {
        id: 'rZhkIhw7y7',
        name: 'SettingsRemote',
      },
      {
        id: 'Ow$izx5GJc',
        name: 'SettingsVoice',
      },
      {
        id: 'K4w5ykR4BK',
        name: 'Shop',
      },
      {
        id: 'RCOBh7Dz6H',
        name: 'ShopTwo',
      },
      {
        id: 'VmUPHlVs$q',
        name: 'ShoppingBasket',
      },
      {
        id: 'zPD8sAgOqu',
        name: 'ShoppingCart',
      },
      {
        id: 'huZIzukNL5',
        name: 'SpeakerNotes',
      },
      {
        id: 'P3skC_ehjT',
        name: 'SpeakerNotesOff',
      },
      {
        id: 'yau1kudiU4',
        name: 'Spellcheck',
      },
      {
        id: '7u44g66qZf',
        name: 'Stars',
      },
      {
        id: '6$nQldlfEp',
        name: 'Store',
      },
      {
        id: 'Q0VRp4fVgy',
        name: 'Subject',
      },
      {
        id: 'ElUNu6tHVg',
        name: 'SupervisedUserCircle',
      },
      {
        id: 'MmzAcOr7MQ',
        name: 'SupervisorAccount',
      },
      {
        id: '1x88FrT18j',
        name: 'SwapHoriz',
      },
      {
        id: '5JHOiLw49P',
        name: 'SwapHorizontalCircle',
      },
      {
        id: 'VxzZo6GFUt',
        name: 'SwapVert',
      },
      {
        id: 'uXRUGZ$OOT',
        name: 'SwapVerticalCircle',
      },
      {
        id: 'Hsws4h9BV2',
        name: 'SyncAlt',
      },
      {
        id: 'K5KMdSNXU0',
        name: 'SystemUpdateAlt',
      },
      {
        id: 'B3OaWS0tCL',
        name: 'Tab',
      },
      {
        id: 'q1uK2cEGEh',
        name: 'TabUnselected',
      },
      {
        id: 'Q8CWXlwSPX',
        name: 'TextRotateUp',
      },
      {
        id: '4WTBo_IySi',
        name: 'TextRotateVertical',
      },
      {
        id: 'oLRvQWOMzN',
        name: 'TextRotationAngledown',
      },
      {
        id: 'bFOBjWMQ_u',
        name: 'TextRotationAngleup',
      },
      {
        id: 'To84htAuc4',
        name: 'TextRotationDown',
      },
      {
        id: '48GTPQZMSs',
        name: 'TextRotationNone',
      },
      {
        id: 'kgRiyOKrX9',
        name: 'Theaters',
      },
      {
        id: '$ghLMLBax6',
        name: 'ThumbDown',
      },
      {
        id: 'EB8wbxMxB7',
        name: 'ThumbUp',
      },
      {
        id: 'ufxiKLzXb4',
        name: 'ThumbsUpDown',
      },
      {
        id: 'bue$iBCLOP',
        name: 'Timeline',
      },
      {
        id: 'BdgQJEi9xa',
        name: 'Toc',
      },
      {
        id: 'KAC6VGthIo',
        name: 'Today',
      },
      {
        id: 'm6Mpusm16w',
        name: 'Toll',
      },
      {
        id: '48ls_6C8EJ',
        name: 'TouchApp',
      },
      {
        id: 'aCLH_vvsa2',
        name: 'TrackChanges',
      },
      {
        id: 'F7q88yac3h',
        name: 'Translate',
      },
      {
        id: '_2MRjqoufc',
        name: 'TrendingDown',
      },
      {
        id: 'hnyv6_i4Ne',
        name: 'TrendingFlat',
      },
      {
        id: 'HWzB623b5K',
        name: 'TrendingUp',
      },
      {
        id: 'SrmhUl5ckM',
        name: 'TurnedIn',
      },
      {
        id: 'DtvWFJk7Ss',
        name: 'TurnedInNot',
      },
      {
        id: 'C5lGWIrA_L',
        name: 'Update',
      },
      {
        id: 'j0hy92RNHG',
        name: 'VerifiedUser',
      },
      {
        id: 'i1MDi6zVRZ',
        name: 'VerticalSplit',
      },
      {
        id: 'hRPQ98H$Le',
        name: 'ViewAgenda',
      },
      {
        id: 'q9eBa_L9QM',
        name: 'ViewArray',
      },
      {
        id: 'tl6bk18DA7',
        name: 'ViewCarousel',
      },
      {
        id: 'Q3O0FmLw5l',
        name: 'ViewColumn',
      },
      {
        id: 'sk5jbKwznt',
        name: 'ViewDay',
      },
      {
        id: 'rv2ZFLXb1S',
        name: 'ViewHeadline',
      },
      {
        id: 'Fpj_6rENOC',
        name: 'ViewList',
      },
      {
        id: 'bBHKZRRAwp',
        name: 'ViewModule',
      },
      {
        id: 'CoAxFpg7L9',
        name: 'ViewQuilt',
      },
      {
        id: 'qJ32ecAv36',
        name: 'ViewStream',
      },
      {
        id: 'tefvC0449l',
        name: 'ViewWeek',
      },
      {
        id: 'LsMwSxcoTF',
        name: 'Visibility',
      },
      {
        id: 'LD3fqLW5z1',
        name: 'VisibilityOff',
      },
      {
        id: 'ZNFPRC4aev',
        name: 'VoiceOverOff',
      },
      {
        id: '2V39sFdyS$',
        name: 'WatchLater',
      },
      {
        id: '4$ApdHHMmt',
        name: 'Work',
      },
      {
        id: '0h3PbO0L0y',
        name: 'WorkOff',
      },
      {
        id: 'SnvxqZYwa_',
        name: 'WorkOutline',
      },
      {
        id: 'hHH5TwUxr0',
        name: 'YoutubeSearchedFor',
      },
      {
        id: '1_JS7iWG$r',
        name: 'ZoomIn',
      },
      {
        id: 'Hqo5Auu01C',
        name: 'ZoomOut',
      },
    ],
  },
  alert: {
    title: 'Alert',
    icons: [
      {
        id: 'HYoMknrmZx',
        name: 'AddAlert',
      },
      {
        id: 'WoUt5KEPgR',
        name: 'Error',
      },
      {
        id: 'E8yDbMH73H',
        name: 'ErrorOutline',
      },
      {
        id: 'hYIfEWyvyR',
        name: 'NotificationImportant',
      },
      {
        id: 'Av2oPCKBaU',
        name: 'Warning',
      },
    ],
  },
  av: {
    title: 'AV',
    icons: [
      {
        id: '9TFGBCxfJy',
        name: 'FourK',
      },
      {
        id: 'NdaM6zT2lG',
        name: 'AddToQueue',
      },
      {
        id: 'NqUcl0KWYs',
        name: 'Airplay',
      },
      {
        id: 'dHpg4Bz8no',
        name: 'Album',
      },
      {
        id: 'RQ6Xnlt_2ZY',
        name: 'ArtTrack',
      },
      {
        id: 'Cvt8WN62ebl',
        name: 'AvTimer',
      },
      {
        id: 'UL95umR2O82',
        name: 'BrandingWatermark',
      },
      {
        id: 'ggkhcJ7srEo',
        name: 'CallToAction',
      },
      {
        id: 'PvDuBfEJ6gJ',
        name: 'ClosedCaption',
      },
      {
        id: '1LSFGNTC1W6',
        name: 'ControlCamera',
      },
      {
        id: 'SaKAr0LwRME',
        name: 'Equalizer',
      },
      {
        id: 'XL64CgpEh23',
        name: 'Explicit',
      },
      {
        id: 'RSaDUG4FJXG',
        name: 'FastForward',
      },
      {
        id: 'sALHwhxN3Ls',
        name: 'FastRewind',
      },
      {
        id: '$nMFBPMrVQl',
        name: 'FeaturedPlayList',
      },
      {
        id: 'Cg7mqex1NSw',
        name: 'FeaturedVideo',
      },
      {
        id: 'sV5$16WohWn',
        name: 'FiberDvr',
      },
      {
        id: '0dpf7FbPsO0',
        name: 'FiberManualRecord',
      },
      {
        id: 'AOiWT463Uvq',
        name: 'FiberNew',
      },
      {
        id: '8SEholEa5ux',
        name: 'FiberPin',
      },
      {
        id: 'ZajYjFsOxIn',
        name: 'FiberSmartRecord',
      },
      {
        id: 'jj7zLdbV$gZ',
        name: 'Forward10',
      },
      {
        id: '$n43xI8jvl8',
        name: 'Forward30',
      },
      {
        id: 'jC5d4c$i4k3',
        name: 'Forward5',
      },
      {
        id: 'TUK78mL3Q5n',
        name: 'Games',
      },
      {
        id: '0YywtbY0DTj',
        name: 'Hd',
      },
      {
        id: 'yHnm5OBr0mk',
        name: 'Hearing',
      },
      {
        id: 'wrlke9fyp1M',
        name: 'HighQuality',
      },
      {
        id: 'ws5QIh8tjnX',
        name: 'LibraryAdd',
      },
      {
        id: 'vRyULTXbHwJ',
        name: 'LibraryBooks',
      },
      {
        id: 'PtyoHtjE7S6',
        name: 'LibraryMusic',
      },
      {
        id: 'oDT_AAFu_27',
        name: 'Loop',
      },
      {
        id: '98lMU1LG9Ju',
        name: 'Mic',
      },
      {
        id: 'xrVs$DuZXvh',
        name: 'MicNone',
      },
      {
        id: 'Yzl4ZNPHbEH',
        name: 'MicOff',
      },
      {
        id: 'N0ReEELDmOX',
        name: 'MissedVideoCall',
      },
      {
        id: '9u7Voik2oPC',
        name: 'Movie',
      },
      {
        id: 'Q0qmtu4JJ8$',
        name: 'MusicVideo',
      },
      {
        id: 'cTONsHiTAuW',
        name: 'NewReleases',
      },
      {
        id: 'b0$OW$Uw892',
        name: 'NotInterested',
      },
      {
        id: 'mCxg4Do1BuX',
        name: 'Note',
      },
      {
        id: 'e0_4RWRSzf7',
        name: 'Pause',
      },
      {
        id: 'ZRV$40GrDtF',
        name: 'PauseCircleFilled',
      },
      {
        id: 'qzT4jIAZL85',
        name: 'PauseCircleOutline',
      },
      {
        id: 'vnDFCkTX278',
        name: 'PlayArrow',
      },
      {
        id: 'i1OHvrsDfgS',
        name: 'PlayCircleFilled',
      },
      {
        id: 'QYoxJ5mJXfD',
        name: 'PlayCircleOutline',
      },
      {
        id: 'NvAFGSZvLdo',
        name: 'PlaylistAdd',
      },
      {
        id: 'oRNRkv83htt',
        name: 'PlaylistAddCheck',
      },
      {
        id: 'BVITYpj2Vuo',
        name: 'PlaylistPlay',
      },
      {
        id: 'JEFE2Wgazi$',
        name: 'Queue',
      },
      {
        id: 'EeH5f$s5sQ_',
        name: 'QueueMusic',
      },
      {
        id: 'Iq8hfhaXPDp',
        name: 'QueuePlayNext',
      },
      {
        id: 'AxcsJGWvCZi',
        name: 'Radio',
      },
      {
        id: 's3msiUM9_pg',
        name: 'RecentActors',
      },
      {
        id: 'ItPphGdYTR5',
        name: 'RemoveFromQueue',
      },
      {
        id: 'tI7tN4wQI0a',
        name: 'Repeat',
      },
      {
        id: 'AF9RhUZCrOO',
        name: 'RepeatOne',
      },
      {
        id: 'HE34JPoFYaz',
        name: 'Replay',
      },
      {
        id: '8yw8a6fWPBF',
        name: 'Replay10',
      },
      {
        id: 'HubEEhwdqYb',
        name: 'Replay30',
      },
      {
        id: 'D0Ip5iTVPan',
        name: 'Replay5',
      },
      {
        id: 'SDmV4S0duef',
        name: 'Shuffle',
      },
      {
        id: 'U7N$fVoyUH9',
        name: 'SkipNext',
      },
      {
        id: 'Zb_uVjX4bBL',
        name: 'SkipPrevious',
      },
      {
        id: 'IJ5uIo7cZat',
        name: 'SlowMotionVideo',
      },
      {
        id: 'a2EA84tBlBA',
        name: 'Snooze',
      },
      {
        id: '02MCoh4wCNS',
        name: 'SortByAlpha',
      },
      {
        id: 'Szf80FAnIcQ',
        name: 'Speed',
      },
      {
        id: '6lAOkFEhDL5',
        name: 'Stop',
      },
      {
        id: 'oVXftWiAoT2',
        name: 'Subscriptions',
      },
      {
        id: 'kIHj1Ussk4k',
        name: 'Subtitles',
      },
      {
        id: 'sA45xNwcd9a',
        name: 'SurroundSound',
      },
      {
        id: '2YRiGxnaqWc',
        name: 'VideoCall',
      },
      {
        id: 'HWrFU4Tjsae',
        name: 'VideoLabel',
      },
      {
        id: 'i0W17y4gDs2',
        name: 'VideoLibrary',
      },
      {
        id: '91zqjLm3OVr',
        name: 'Videocam',
      },
      {
        id: 'pDe0Xy7nEfJ',
        name: 'VideocamOff',
      },
      {
        id: 'fIT6uqjlbsC',
        name: 'VolumeDown',
      },
      {
        id: 'ADk$ejobXCf',
        name: 'VolumeMute',
      },
      {
        id: 'iOZD002VX6q',
        name: 'VolumeOff',
      },
      {
        id: 'pmJNGrjkJU4',
        name: 'VolumeUp',
      },
      {
        id: 'meLbY778gpu',
        name: 'Web',
      },
      {
        id: 'L7R9cq43L52',
        name: 'WebAsset',
      },
    ],
  },
  communication: {
    title: 'Communication',
    icons: [
      {
        id: 'WTbvDcwsMi1',
        name: 'Business',
      },
      {
        id: 'u3yO16f8yqG',
        name: 'Call',
      },
      {
        id: 'z66yZS1obzF',
        name: 'CallEnd',
      },
      {
        id: 'wycJOLaoNCp',
        name: 'CallMade',
      },
      {
        id: 'Hxp7EIChUz7',
        name: 'CallMerge',
      },
      {
        id: '25GwnHGCK5c',
        name: 'CallMissed',
      },
      {
        id: 'WFsZosi1azc',
        name: 'CallMissedOutgoing',
      },
      {
        id: 'y1eX2oVGE1w',
        name: 'CallReceived',
      },
      {
        id: 'J1XV04Gj33x',
        name: 'CallSplit',
      },
      {
        id: 'LfzEDJRKfNS',
        name: 'CancelPresentation',
      },
      {
        id: 'kQ6ocMhkhdB',
        name: 'Chat',
      },
      {
        id: 'Zzk8Xossl6M',
        name: 'ChatBubble',
      },
      {
        id: '451zHudfWOl',
        name: 'ChatBubbleOutline',
      },
      {
        id: 'ff8ttp7yM3j',
        name: 'ClearAll',
      },
      {
        id: 'IOoD7UPqC32',
        name: 'Comment',
      },
      {
        id: 'IaIYnHVUl6k',
        name: 'ContactMail',
      },
      {
        id: '3Frq9yOCfxk',
        name: 'ContactPhone',
      },
      {
        id: 'lj87rMsU6wl',
        name: 'Contacts',
      },
      {
        id: 'XbOFwyN3JLY',
        name: 'DesktopAccessDisabled',
      },
      {
        id: 'JXNzGHuEq7O',
        name: 'DialerSip',
      },
      {
        id: '8zF5fI5nl0d',
        name: 'Dialpad',
      },
      {
        id: 'RaeT_RXMJzQ',
        name: 'DomainDisabled',
      },
      {
        id: 'Epiye3fpbR6',
        name: 'Duo',
      },
      {
        id: 'JlKZi_FJJM$',
        name: 'Email',
      },
      {
        id: 'kgwIBMvMwQQ',
        name: 'Forum',
      },
      {
        id: 'IjuHUoKqGDs',
        name: 'ImportContacts',
      },
      {
        id: 'gC6OcuNjycS',
        name: 'ImportExport',
      },
      {
        id: 'B4wCtWN4b5Q',
        name: 'InvertColorsOff',
      },
      {
        id: 'lCRNQByP2TE',
        name: 'ListAlt',
      },
      {
        id: 'PalWqKGdfID',
        name: 'LiveHelp',
      },
      {
        id: 'XO1Cy4jtu93',
        name: 'MailOutline',
      },
      {
        id: 'FrEovIl2bfk',
        name: 'Message',
      },
      {
        id: 'sq6cvFhrReD',
        name: 'MobileScreenShare',
      },
      {
        id: 'AfhXq_QDrvH',
        name: 'NoSim',
      },
      {
        id: '5sximu04G33',
        name: 'PausePresentation',
      },
      {
        id: 'fzIOwPJUyw8',
        name: 'PersonAddDisabled',
      },
      {
        id: 'pXURKpA4Cyt',
        name: 'Phone',
      },
      {
        id: 'njB8EfHNl$u',
        name: 'PhoneDisabled',
      },
      {
        id: '$o5FKHUHeB2',
        name: 'PhoneEnabled',
      },
      {
        id: 'PMCosz6gGV4',
        name: 'PhonelinkErase',
      },
      {
        id: '6mt7FlbFhJe',
        name: 'PhonelinkLock',
      },
      {
        id: 'jHG20e89rbY',
        name: 'PhonelinkRing',
      },
      {
        id: 'FoLO$8RqlFJ',
        name: 'PhonelinkSetup',
      },
      {
        id: '_Rk6FfLGq7x',
        name: 'PortableWifiOff',
      },
      {
        id: 'NvA_ZLqTF88',
        name: 'PresentToAll',
      },
      {
        id: '7SeMQdrKtYs',
        name: 'PrintDisabled',
      },
      {
        id: 'zrGOmBhsT0Z',
        name: 'RingVolume',
      },
      {
        id: '2NPSyDJMt_G',
        name: 'RssFeed',
      },
      {
        id: 'CINEZvhoq58',
        name: 'ScreenShare',
      },
      {
        id: '4IpZi3EhBgf',
        name: 'SentimentSatisfiedAlt',
      },
      {
        id: 'EW4e7ROJbFl',
        name: 'SpeakerPhone',
      },
      {
        id: 'lKRJrLKedM8',
        name: 'StayCurrentLandscape',
      },
      {
        id: 'eyBXl0UVDE2',
        name: 'StayCurrentPortrait',
      },
      {
        id: 'RYPAqYORZRj',
        name: 'StayPrimaryLandscape',
      },
      {
        id: 'Ev8n8eP0sfA',
        name: 'StayPrimaryPortrait',
      },
      {
        id: 'gXUT0eYJlCf',
        name: 'StopScreenShare',
      },
      {
        id: 'mUHi$8fIoOH',
        name: 'SwapCalls',
      },
      {
        id: 'LB54BHoG$BR',
        name: 'Textsms',
      },
      {
        id: 'l2h6U_FVlvM',
        name: 'Unsubscribe',
      },
      {
        id: 'GULjHmbvmsf',
        name: 'Voicemail',
      },
      {
        id: 'm0Sfno414yS',
        name: 'VpnKey',
      },
    ],
  },
  content: {
    title: 'Content',
    icons: [
      {
        id: 'KINvNRRHvVe',
        name: 'Add',
      },
      {
        id: '7vQzuJxZ3At',
        name: 'AddBox',
      },
      {
        id: 'pvjsDDvmBFG',
        name: 'AddCircle',
      },
      {
        id: '2oSyB4kKfn7',
        name: 'AddCircleOutline',
      },
      {
        id: 'rB0xqA4h_BP',
        name: 'AmpStories',
      },
      {
        id: 'lTu1aL255Ez',
        name: 'Archive',
      },
      {
        id: '6RMY5trZThJ',
        name: 'Backspace',
      },
      {
        id: '19fJsJIjC_o',
        name: 'Ballot',
      },
      {
        id: 'YRqg6u5qMGk',
        name: 'Block',
      },
      {
        id: 'ot29798la9X',
        name: 'Clear',
      },
      {
        id: 'PtjVQAo2svs',
        name: 'Create',
      },
      {
        id: 'ox8VDIhSHJZ',
        name: 'DeleteSweep',
      },
      {
        id: 'X4ftxFXxBGU',
        name: 'Drafts',
      },
      {
        id: 'ECuInrQ5am5',
        name: 'DynamicFeed',
      },
      {
        id: '0WiZulGwEen',
        name: 'FileCopy',
      },
      {
        id: 'gmDZL6lY$Z3',
        name: 'FilterList',
      },
      {
        id: 'jgKrn9qlAkw',
        name: 'Flag',
      },
      {
        id: '_91SEks7m$x',
        name: 'FontDownload',
      },
      {
        id: 'dQrNQJfBdmS',
        name: 'Forward',
      },
      {
        id: 'BPB$izQgwdg',
        name: 'Gesture',
      },
      {
        id: 'nLaReOCXkFN',
        name: 'HowToReg',
      },
      {
        id: 'TixyOtU$lwz',
        name: 'HowToVote',
      },
      {
        id: 'ipILHFg1pGw',
        name: 'Inbox',
      },
      {
        id: '_th5Gd2xiQm',
        name: 'Link',
      },
      {
        id: 't50pe5r$tiX',
        name: 'LinkOff',
      },
      {
        id: 'aWQwGkk2niL',
        name: 'LowPriority',
      },
      {
        id: 'XDhRWduLIai',
        name: 'Mail',
      },
      {
        id: 'f9otI55byKG',
        name: 'Markunread',
      },
      {
        id: 'OAhb59H3kvj',
        name: 'MoveToInbox',
      },
      {
        id: 'T1_UHlRLgur',
        name: 'NextWeek',
      },
      {
        id: '$R_vKuPXUw0',
        name: 'OutlinedFlag',
      },
      {
        id: 'Lt1TCQodMJp',
        name: 'Policy',
      },
      {
        id: 'pPHKGiVH6RL',
        name: 'Redo',
      },
      {
        id: 'fzHe$bPhiag',
        name: 'Remove',
      },
      {
        id: '3cHn5MjQjCM',
        name: 'RemoveCircle',
      },
      {
        id: 'uByEJeU$P_C',
        name: 'RemoveCircleOutline',
      },
      {
        id: 'qub_irTF9l6',
        name: 'Reply',
      },
      {
        id: 'C0qT6SjWz$Q',
        name: 'ReplyAll',
      },
      {
        id: 'V1_yeUg_uTX',
        name: 'Report',
      },
      {
        id: 'a43iKxoBP2K',
        name: 'ReportOff',
      },
      {
        id: 'Qzv6fZbW2oA',
        name: 'Save',
      },
      {
        id: 'usZWaVGgo7A',
        name: 'SaveAlt',
      },
      {
        id: 'hMa5d2FMBEo',
        name: 'SelectAll',
      },
      {
        id: 'uBQy3j9u4na',
        name: 'Send',
      },
      {
        id: 'zhp$XBj81M8',
        name: 'Sort',
      },
      {
        id: 'W5fj4rPuazc',
        name: 'SquareFoot',
      },
      {
        id: 't6trJGjxrnE',
        name: 'TextFormat',
      },
      {
        id: 'tOeM3s2dpwk',
        name: 'Unarchive',
      },
      {
        id: 'zJVfdmysLa6',
        name: 'Undo',
      },
      {
        id: 'FOZ4_fxhiNc',
        name: 'Waves',
      },
      {
        id: '5DC9igNQ6Sw',
        name: 'WhereToVote',
      },
    ],
  },
  device: {
    title: 'Device',
    icons: [
      {
        id: 'o$BP6wWel0k',
        name: 'AccessAlarm',
      },
      {
        id: 'HK23Q2lAIJ0',
        name: 'AccessAlarms',
      },
      {
        id: 'rSFpU4vynQc',
        name: 'AccessTime',
      },
      {
        id: 'hTZI9eiurZ5',
        name: 'AddAlarm',
      },
      {
        id: 'We9ATRIagdA',
        name: 'AddToHomeScreen',
      },
      {
        id: 'OWGW0UcFoqX',
        name: 'AirplanemodeActive',
      },
      {
        id: 'GFYtXk$M8Zz',
        name: 'AirplanemodeInactive',
      },
      {
        id: 'KoxcXnsh0Tk',
        name: 'BatteryAlert',
      },
      {
        id: 'H2hsy4rCwKa',
        name: 'BatteryChargingFull',
      },
      {
        id: 'wJOo7QvKEUk',
        name: 'BatteryFull',
      },
      {
        id: 'sOFZIffGBj4',
        name: 'BatteryStd',
      },
      {
        id: 'MmuvlY3x0f5',
        name: 'BatteryUnknown',
      },
      {
        id: 'kU4$cBhVZBF',
        name: 'Bluetooth',
      },
      {
        id: 'p6JcceY40sX',
        name: 'BluetoothConnected',
      },
      {
        id: 'o3bMnRToKpU',
        name: 'BluetoothDisabled',
      },
      {
        id: 'LpTkOKKucBb',
        name: 'BluetoothSearching',
      },
      {
        id: 'jesecD61KD2',
        name: 'BrightnessAuto',
      },
      {
        id: '1zaVK8XKyDW',
        name: 'BrightnessHigh',
      },
      {
        id: 'KPpV_jaeC27',
        name: 'BrightnessLow',
      },
      {
        id: '3NkE8vB8Cha',
        name: 'BrightnessMedium',
      },
      {
        id: 'YDQcqhUBYGZ',
        name: 'DataUsage',
      },
      {
        id: '5_guF0CUWdr',
        name: 'DeveloperMode',
      },
      {
        id: 'PepxshlBY_Q',
        name: 'Devices',
      },
      {
        id: 'EgdPaHn$W7O',
        name: 'Dvr',
      },
      {
        id: 'l5y43E_nVgX',
        name: 'GpsFixed',
      },
      {
        id: 'D96virfBkD$',
        name: 'GpsNotFixed',
      },
      {
        id: 'aJEEkRVddQ8',
        name: 'GpsOff',
      },
      {
        id: 'uT$OS5cSzFX',
        name: 'GraphicEq',
      },
      {
        id: 'GXivCLsmxKh',
        name: 'LocationDisabled',
      },
      {
        id: 'iP9XMv7fGOh',
        name: 'LocationSearching',
      },
      {
        id: 'DCG5Q4wuk0e',
        name: 'MobileFriendly',
      },
      {
        id: 'O8EM4n3bKcX',
        name: 'MobileOff',
      },
      {
        id: 'C3ov6YYQLLI',
        name: 'Nfc',
      },
      {
        id: 'aV08JfEKTJH',
        name: 'ScreenLockLandscape',
      },
      {
        id: 'Npv5u0rRJoK',
        name: 'ScreenLockPortrait',
      },
      {
        id: 'or4hg9ANXm2',
        name: 'ScreenLockRotation',
      },
      {
        id: 'AP9Fv125MSo',
        name: 'ScreenRotation',
      },
      {
        id: 'w$bCmiTEEtL',
        name: 'SdStorage',
      },
      {
        id: 'xcb5NC2$lfj',
        name: 'SettingsSystemDaydream',
      },
      {
        id: 'PPLKMgD6QaI',
        name: 'SignalCellular4Bar',
      },
      {
        id: 'NqmOHTqjBag',
        name: 'SignalCellularAlt',
      },
      {
        id: 'c994BXVehP1',
        name: 'SignalCellularConnectedNoInternet4Bar',
      },
      {
        id: 'kEXdfERqSbg',
        name: 'SignalCellularNoSim',
      },
      {
        id: '8C55AmDatC0',
        name: 'SignalCellularNull',
      },
      {
        id: 'l1nys$PKlxU',
        name: 'SignalCellularOff',
      },
      {
        id: 'o9$CHsQBqRF',
        name: 'SignalWifi4Bar',
      },
      {
        id: 'RpIa9ko_T9W',
        name: 'SignalWifi4BarLock',
      },
      {
        id: 'nXS0UImQU51',
        name: 'SignalWifiOff',
      },
      {
        id: 'CUbQ82WZMg0',
        name: 'Storage',
      },
      {
        id: 'XpjDElqExog',
        name: 'Usb',
      },
      {
        id: 'JefL5L0bAtB',
        name: 'Wallpaper',
      },
      {
        id: 'nuWZKFY1EoG',
        name: 'Widgets',
      },
      {
        id: 'fpdaG2wApGZ',
        name: 'WifiLock',
      },
      {
        id: 'b18Q0F8XnsI',
        name: 'WifiTethering',
      },
    ],
  },
  editor: {
    title: 'Editor',
    icons: [
      {
        id: 'a$MlMrbW3o5',
        name: 'AddComment',
      },
      {
        id: 'MFc_iR7kacU',
        name: 'AttachFile',
      },
      {
        id: 'tmU9B3P5KjH',
        name: 'AttachMoney',
      },
      {
        id: 'AJ7Ynckilat',
        name: 'BarChart',
      },
      {
        id: 'HUdB9FMhVTC',
        name: 'BorderAll',
      },
      {
        id: '5XUxKVyuY1s',
        name: 'BorderBottom',
      },
      {
        id: '588JCsXyRSR',
        name: 'BorderClear',
      },
      {
        id: 'Ru9VJXSSOmu',
        name: 'BorderHorizontal',
      },
      {
        id: 'fi0A9dnGsE5',
        name: 'BorderInner',
      },
      {
        id: 'nxULSdDqZwf',
        name: 'BorderLeft',
      },
      {
        id: '2Fhmq$lFAto',
        name: 'BorderOuter',
      },
      {
        id: 'l_iRDrOWz3F',
        name: 'BorderRight',
      },
      {
        id: '3X2m4PMmAIJ',
        name: 'BorderStyle',
      },
      {
        id: 'MN02FCBZb8z',
        name: 'BorderTop',
      },
      {
        id: 'FLnvl6HnejY',
        name: 'BorderVertical',
      },
      {
        id: 'j6aA7Z$b5rt',
        name: 'BubbleChart',
      },
      {
        id: 'LnXy0t6vdhr',
        name: 'DragHandle',
      },
      {
        id: 'lr0VQv6yBDJ',
        name: 'FormatAlignCenter',
      },
      {
        id: 'Gfy$Ixa9sUp',
        name: 'FormatAlignJustify',
      },
      {
        id: '18SbwCENU99',
        name: 'FormatAlignLeft',
      },
      {
        id: 'aSCCuLPlVo2',
        name: 'FormatAlignRight',
      },
      {
        id: 'zqSqxCKrMjU',
        name: 'FormatBold',
      },
      {
        id: '9xh9frRQcuI',
        name: 'FormatClear',
      },
      {
        id: 'dyuF846Qvke',
        name: 'FormatColorReset',
      },
      {
        id: 'KUVudMXcsQu',
        name: 'FormatIndentDecrease',
      },
      {
        id: 'PuTvYJniXJC',
        name: 'FormatIndentIncrease',
      },
      {
        id: '7wveO3GGUsI',
        name: 'FormatItalic',
      },
      {
        id: 'fPhVrW5XOWD',
        name: 'FormatLineSpacing',
      },
      {
        id: 'WRM5402dfF2',
        name: 'FormatListBulleted',
      },
      {
        id: 'CveuU6n1vRQ',
        name: 'FormatListNumbered',
      },
      {
        id: 'Ky0Ft7r9atv',
        name: 'FormatListNumberedRtl',
      },
      {
        id: 'VF_RPc8ZDvy',
        name: 'FormatPaint',
      },
      {
        id: 'acAbXob16G9',
        name: 'FormatQuote',
      },
      {
        id: '1Ga6jW2QGPN',
        name: 'FormatShapes',
      },
      {
        id: 'i0jSyX0wdzV',
        name: 'FormatSize',
      },
      {
        id: 'Qfzyq3dXFM6',
        name: 'FormatStrikethrough',
      },
      {
        id: 'Mo$0zloSpKQ',
        name: 'FormatTextdirectionLToR',
      },
      {
        id: 'xtWa7$BeTRO',
        name: 'FormatTextdirectionRToL',
      },
      {
        id: 'qx1GVDgxbYV',
        name: 'FormatUnderlined',
      },
      {
        id: 'i1LNlTTqSsU',
        name: 'Functions',
      },
      {
        id: 'B5pSJjTGNHc',
        name: 'Height',
      },
      {
        id: 'yDBCFpcGZQk',
        name: 'Highlight',
      },
      {
        id: '0nnYltcqqze',
        name: 'InsertChart',
      },
      {
        id: 'Kf2yp8K45rT',
        name: 'InsertChartOutlined',
      },
      {
        id: 'n$qJiPPUgQb',
        name: 'InsertComment',
      },
      {
        id: 'T6UMrKNrFMF',
        name: 'InsertDriveFile',
      },
      {
        id: 'XPRjmt$pgqK',
        name: 'InsertEmoticon',
      },
      {
        id: 'rT$ddTmyxPk',
        name: 'InsertInvitation',
      },
      {
        id: 'GGIB9un31pi',
        name: 'InsertLink',
      },
      {
        id: 'JP$622Xtg72',
        name: 'InsertPhoto',
      },
      {
        id: 'VEpDe3UoaS8',
        name: 'LinearScale',
      },
      {
        id: 'RO0AfxSFRFB',
        name: 'MergeType',
      },
      {
        id: 'g31UCqwn5l_',
        name: 'ModeComment',
      },
      {
        id: 'P9bsueUWQGS',
        name: 'MonetizationOn',
      },
      {
        id: '1j19zOLrMtn',
        name: 'MoneyOff',
      },
      {
        id: '7LEW$IFctso',
        name: 'MultilineChart',
      },
      {
        id: '4yBU6Gsfa9z',
        name: 'Notes',
      },
      {
        id: 'S0pwJkJwJm2',
        name: 'PieChart',
      },
      {
        id: 'UBvFmdrm98g',
        name: 'PostAdd',
      },
      {
        id: 'Ax$iQjll_rr',
        name: 'Publish',
      },
      {
        id: 'k2X$0BgAYRM',
        name: 'ScatterPlot',
      },
      {
        id: '1yrj4TyMA5a',
        name: 'Score',
      },
      {
        id: 'EPkyLYTpkvJ',
        name: 'ShortText',
      },
      {
        id: 'XZpsAAwSvnI',
        name: 'ShowChart',
      },
      {
        id: '7mrGWnqMPuv',
        name: 'SpaceBar',
      },
      {
        id: 'oDAdI0GRZ4Z',
        name: 'StrikethroughS',
      },
      {
        id: 'iwUQiKz8qr3',
        name: 'TableChart',
      },
      {
        id: 'LKhJF0VN9yl',
        name: 'TextFields',
      },
      {
        id: 'QR$3Kz99XYQ',
        name: 'Title',
      },
      {
        id: '6peHiluz83u',
        name: 'VerticalAlignBottom',
      },
      {
        id: 'smGDATvdMA8',
        name: 'VerticalAlignCenter',
      },
      {
        id: 'IPmsHq9jzKq',
        name: 'VerticalAlignTop',
      },
      {
        id: 'AcJjHrQMxzM',
        name: 'WrapText',
      },
    ],
  },
  file: {
    title: 'File',
    icons: [
      {
        id: 'xj033l_eaUt',
        name: 'Attachment',
      },
      {
        id: 'FMDlVjKoaiu',
        name: 'Cloud',
      },
      {
        id: 'nNVIaMIKmih',
        name: 'CloudCircle',
      },
      {
        id: 'ajGh9RARBjc',
        name: 'CloudDone',
      },
      {
        id: '3Rn_j74XskQ',
        name: 'CloudDownload',
      },
      {
        id: 'LPCK_ps4792',
        name: 'CloudOff',
      },
      {
        id: 'QkoNkFzkN_1',
        name: 'CloudQueue',
      },
      {
        id: 'AJtJCwuQb64',
        name: 'CloudUpload',
      },
      {
        id: 'MZov$ORQbhU',
        name: 'CreateNewFolder',
      },
      {
        id: 'FZE4n8$MoE4',
        name: 'Folder',
      },
      {
        id: 's$v8FmZcwok',
        name: 'FolderOpen',
      },
      {
        id: '2A2alWmY0Dy',
        name: 'FolderShared',
      },
    ],
  },
  hardware: {
    title: 'Hardware',
    icons: [
      {
        id: 'CPB1RPKcqZw',
        name: 'Cast',
      },
      {
        id: 'MbtVJNmBAiA',
        name: 'CastConnected',
      },
      {
        id: 'pv3LiZ8GwR3',
        name: 'Computer',
      },
      {
        id: 'WYRPiSrKnYO',
        name: 'DesktopMac',
      },
      {
        id: '86DWU7YjTsE',
        name: 'DesktopWindows',
      },
      {
        id: '21IesdljstS',
        name: 'DeveloperBoard',
      },
      {
        id: 'YqrQxds7v6L',
        name: 'DeviceHub',
      },
      {
        id: 'unG1TOFtAMZ',
        name: 'DeviceUnknown',
      },
      {
        id: '8Y$gFwdJnqe',
        name: 'DevicesOther',
      },
      {
        id: 'LlUpAzPOpG2',
        name: 'Dock',
      },
      {
        id: 'v9UhMSS2v5B',
        name: 'Gamepad',
      },
      {
        id: '5ri9IvrBVXq',
        name: 'Headset',
      },
      {
        id: 'ppvaOhCk1cz',
        name: 'HeadsetMic',
      },
      {
        id: 'PqauDvbm0WR',
        name: 'Keyboard',
      },
      {
        id: 'N_CytZjrcbw',
        name: 'KeyboardArrowDown',
      },
      {
        id: '0WDr2Ki$QYb',
        name: 'KeyboardArrowLeft',
      },
      {
        id: '19o0Y0792oc',
        name: 'KeyboardArrowRight',
      },
      {
        id: 'FKjvPPtDDO7',
        name: 'KeyboardArrowUp',
      },
      {
        id: 'xYnB3zluXJ9',
        name: 'KeyboardBackspace',
      },
      {
        id: 'D86h0qM67Fs',
        name: 'KeyboardCapslock',
      },
      {
        id: '92YJAEGPGj2',
        name: 'KeyboardHide',
      },
      {
        id: '4SCraNJuM4D',
        name: 'KeyboardReturn',
      },
      {
        id: 'JNu3yKnnOs9',
        name: 'KeyboardTab',
      },
      {
        id: 'O0QkNGDZ4UK',
        name: 'KeyboardVoice',
      },
      {
        id: 'QEwBqPN_10R',
        name: 'Laptop',
      },
      {
        id: 'XKtkXCrlLy4',
        name: 'LaptopChromebook',
      },
      {
        id: 'Vo9xJkqF2ZD',
        name: 'LaptopMac',
      },
      {
        id: '3bGaFdq8eYx',
        name: 'LaptopWindows',
      },
      {
        id: 'mmu8eMKEK0u',
        name: 'Memory',
      },
      {
        id: 'aPUN_BGXP57',
        name: 'Mouse',
      },
      {
        id: 'lr6u$1jofVi',
        name: 'PhoneAndroid',
      },
      {
        id: '0ecYZrM$L13',
        name: 'PhoneIphone',
      },
      {
        id: 'czxpDbZfBja',
        name: 'Phonelink',
      },
      {
        id: 'GoBvYKLgkvu',
        name: 'PhonelinkOff',
      },
      {
        id: 'cXHQ4Wfm5Dq',
        name: 'PowerInput',
      },
      {
        id: 'eEF_VsObSdd',
        name: 'Router',
      },
      {
        id: 'r6iAF22H$f$',
        name: 'Scanner',
      },
      {
        id: 'U5itnyfw73z',
        name: 'Security',
      },
      {
        id: '7pB1mKaM7sk',
        name: 'SimCard',
      },
      {
        id: 'SFEML5y$oNp',
        name: 'Smartphone',
      },
      {
        id: 'XFuSxrFG7Io',
        name: 'Speaker',
      },
      {
        id: 'ugNbfu6HZVa',
        name: 'SpeakerGroup',
      },
      {
        id: '_HmVCVbCLlW',
        name: 'Tablet',
      },
      {
        id: 'OwCuSS_tIFm',
        name: 'TabletAndroid',
      },
      {
        id: 'ir58qVYwuHP',
        name: 'TabletMac',
      },
      {
        id: 'VDggm0Z7Vyn',
        name: 'Toys',
      },
      {
        id: 't1s1dAl9YLg',
        name: 'Tv',
      },
      {
        id: 'N$4rdpSlREm',
        name: 'VideogameAsset',
      },
      {
        id: 'ilMwGFG9zWr',
        name: 'Watch',
      },
    ],
  },
  image: {
    title: 'Image',
    icons: [
      {
        id: 'IvnKuNLiOYd',
        name: 'AddAPhoto',
      },
      {
        id: 'Q51b37JowKW',
        name: 'AddPhotoAlternate',
      },
      {
        id: 'hrtE3NqCUOP',
        name: 'AddToPhotos',
      },
      {
        id: 'NP4igXXoR2m',
        name: 'Adjust',
      },
      {
        id: 'eriSbikdMXv',
        name: 'Assistant',
      },
      {
        id: 'aBVRTmyMuj$',
        name: 'AssistantPhoto',
      },
      {
        id: 'UceTplOadRj',
        name: 'Audiotrack',
      },
      {
        id: 'CkDud5Vvnxx',
        name: 'BlurCircular',
      },
      {
        id: '$sSFvIjWi3t',
        name: 'BlurLinear',
      },
      {
        id: '2HUxYHt_MMR',
        name: 'BlurOff',
      },
      {
        id: 'YpjDhR8V8j8',
        name: 'BlurOn',
      },
      {
        id: 'ZHahGdm35E5',
        name: 'Brightness1',
      },
      {
        id: 'hpXxKT5lnoP',
        name: 'Brightness2',
      },
      {
        id: 'V3U$eC4SzmC',
        name: 'Brightness3',
      },
      {
        id: '7yGIkrusqpf',
        name: 'Brightness4',
      },
      {
        id: '10f_d3dydHm',
        name: 'Brightness5',
      },
      {
        id: 'Evt1Aka9bm9',
        name: 'Brightness6',
      },
      {
        id: 'n$iHRbKNzFX',
        name: 'Brightness7',
      },
      {
        id: '2uHykShoPDJ',
        name: 'BrokenImage',
      },
      {
        id: 'x4Ygi1q3U7l',
        name: 'Brush',
      },
      {
        id: 'aaPhVeiuySg',
        name: 'BurstMode',
      },
      {
        id: '2nAzM0GcWU1',
        name: 'Camera',
      },
      {
        id: 'bzm30Zqza05',
        name: 'CameraAlt',
      },
      {
        id: 'RMVi$gQF7J8',
        name: 'CameraFront',
      },
      {
        id: 'Ls3sNLMDbIy',
        name: 'CameraRear',
      },
      {
        id: 'C4kN8HGWEuE',
        name: 'CameraRoll',
      },
      {
        id: 'jy4bt0H1Kr$',
        name: 'CenterFocusStrong',
      },
      {
        id: 'BSgzddxCYtM',
        name: 'CenterFocusWeak',
      },
      {
        id: 'FmYeBug4xMn',
        name: 'Collections',
      },
      {
        id: 'lB$RGEJzv$l',
        name: 'CollectionsBookmark',
      },
      {
        id: 'sJ1yy9sdCVl',
        name: 'ColorLens',
      },
      {
        id: 'DyzAtHrCbPk',
        name: 'Colorize',
      },
      {
        id: 'LEEkM0Pzw7e',
        name: 'Compare',
      },
      {
        id: 'JT747sBaput',
        name: 'ControlPoint',
      },
      {
        id: 's9o3GNNZIdd',
        name: 'ControlPointDuplicate',
      },
      {
        id: '5I6Es$zKBoS',
        name: 'Crop',
      },
      {
        id: 'B9az__Y_DDF',
        name: 'Crop169',
      },
      {
        id: '3uF_6PGEVeS',
        name: 'Crop32',
      },
      {
        id: 'n8ZmZqYPv5w',
        name: 'Crop54',
      },
      {
        id: 'bHrMiKDOtCR',
        name: 'Crop75',
      },
      {
        id: '7B4_Rm0FqTS',
        name: 'CropDin',
      },
      {
        id: 'ihY30Q8BG87',
        name: 'CropFree',
      },
      {
        id: 'Xvos$1z21r8',
        name: 'CropLandscape',
      },
      {
        id: '7oZNbsr47PH',
        name: 'CropOriginal',
      },
      {
        id: 'VaPduE9lRYj',
        name: 'CropPortrait',
      },
      {
        id: 'ugRQWPLvCjm',
        name: 'CropRotate',
      },
      {
        id: 'WOwb4sCQxMO',
        name: 'CropSquare',
      },
      {
        id: 'AsYTxAP2SZ3',
        name: 'Dehaze',
      },
      {
        id: 'ZAdm7frWAHJ',
        name: 'Details',
      },
      {
        id: 'HFGEeOmV6KX',
        name: 'Edit',
      },
      {
        id: '4iluSdNkmiL',
        name: 'Euro',
      },
      {
        id: '2d_dxJ9sPOJ',
        name: 'Exposure',
      },
      {
        id: 'nZLpo6UslIz',
        name: 'ExposureNeg1',
      },
      {
        id: '$kIPAYOz1To',
        name: 'ExposureNeg2',
      },
      {
        id: 'ryBwsCGawAh',
        name: 'ExposurePlus1',
      },
      {
        id: 'iIHkQyUVK$G',
        name: 'ExposurePlus2',
      },
      {
        id: 'dMlNrfkSuIZ',
        name: 'ExposureZero',
      },
      {
        id: 'R8YutlQki0W',
        name: 'Filter',
      },
      {
        id: 'tQJ0_hdTrib',
        name: 'Filter1',
      },
      {
        id: 'S5ln1IDFFN3',
        name: 'Filter2',
      },
      {
        id: 'nfsev4EQ7oy',
        name: 'Filter3',
      },
      {
        id: 'TEcvGhvgFsQ',
        name: 'Filter4',
      },
      {
        id: 'oiEYCF$YrGq',
        name: 'Filter5',
      },
      {
        id: 'xzi3ohVwQ4_',
        name: 'Filter6',
      },
      {
        id: 'wrdZayaHmH2',
        name: 'Filter7',
      },
      {
        id: 'NbPURin7srt',
        name: 'Filter8',
      },
      {
        id: 'MAXbAHrjI2s',
        name: 'Filter9',
      },
      {
        id: 'IJQbLKgLQD8',
        name: 'Filter9Plus',
      },
      {
        id: 'HEQCCMwd0Bn',
        name: 'FilterBAndW',
      },
      {
        id: 'JwFV1iGbqmO',
        name: 'FilterCenterFocus',
      },
      {
        id: 't26AEIAxyFi',
        name: 'FilterDrama',
      },
      {
        id: 'hA$qxGTHxqa',
        name: 'FilterFrames',
      },
      {
        id: '_MWu_c6$KNS',
        name: 'FilterHdr',
      },
      {
        id: 'ywpokbTLy6W',
        name: 'FilterNone',
      },
      {
        id: '1B7D6qJ7AaQ',
        name: 'FilterTiltShift',
      },
      {
        id: 'dmv9DiKD2RE',
        name: 'FilterVintage',
      },
      {
        id: 'rwqT6lqE8dn',
        name: 'Flare',
      },
      {
        id: 'fK4fWGHmsJ1',
        name: 'FlashAuto',
      },
      {
        id: 'xFq85e6E7Di',
        name: 'FlashOff',
      },
      {
        id: 'bN8302M2Xhc',
        name: 'FlashOn',
      },
      {
        id: '4aVqbULGh3P',
        name: 'Flip',
      },
      {
        id: 'srto_JNjJcT',
        name: 'FlipCameraAndroid',
      },
      {
        id: '3OvmSUEKy1_',
        name: 'FlipCameraIos',
      },
      {
        id: 'yuFu7bLFORV',
        name: 'Gradient',
      },
      {
        id: '7ouiDWo406f',
        name: 'Grain',
      },
      {
        id: 'Ehczkac4R5D',
        name: 'GridOff',
      },
      {
        id: 'uajfno2guf2',
        name: 'GridOn',
      },
      {
        id: 'swkEfN2fM8D',
        name: 'HdrOff',
      },
      {
        id: '2Dh$arf0d36',
        name: 'HdrOn',
      },
      {
        id: 'Aov0g1i$GBF',
        name: 'HdrStrong',
      },
      {
        id: '4xeYWLo7gHj',
        name: 'HdrWeak',
      },
      {
        id: 'USm4Z7shATK',
        name: 'Healing',
      },
      {
        id: 'bJxV_QMI$8J',
        name: 'Image',
      },
      {
        id: 'QYlGdzBhOXf',
        name: 'ImageAspectRatio',
      },
      {
        id: 'BNF8U7NwzXB',
        name: 'ImageSearch',
      },
      {
        id: 'y4dlc8mdDIq',
        name: 'Iso',
      },
      {
        id: 'Pv2Yh354Sz_',
        name: 'Landscape',
      },
      {
        id: 'iuLCvtbhFDP',
        name: 'LeakAdd',
      },
      {
        id: 'Q0m0m6oyHz_',
        name: 'LeakRemove',
      },
      {
        id: 'fjTtOVt3Yjr',
        name: 'Lens',
      },
      {
        id: '3reXCY1yjj5',
        name: 'LinkedCamera',
      },
      {
        id: 'bAMz5G3thRe',
        name: 'Looks',
      },
      {
        id: 'KSGUiwfaOfM',
        name: 'Looks3',
      },
      {
        id: 'kzm1vPuIlhA',
        name: 'Looks4',
      },
      {
        id: 'SB1w9u5QqeQ',
        name: 'Looks5',
      },
      {
        id: 'nuwZ4hXo04b',
        name: 'Looks6',
      },
      {
        id: '9l9TFPP_Gmd',
        name: 'LooksOne',
      },
      {
        id: 'ZgizJTo2oZK',
        name: 'LooksTwo',
      },
      {
        id: 'PZzZWJFb3n3',
        name: 'Loupe',
      },
      {
        id: 'o8cQSNC6p5H',
        name: 'MonochromePhotos',
      },
      {
        id: '3VAgP$z4Ggz',
        name: 'MovieCreation',
      },
      {
        id: '2NQwO$adtnX',
        name: 'MovieFilter',
      },
      {
        id: 'T$7IUIwnRQc',
        name: 'MusicNote',
      },
      {
        id: 'HuYjgvmBhEJ',
        name: 'MusicOff',
      },
      {
        id: 'zSqB8BZP7So',
        name: 'Nature',
      },
      {
        id: 'o9Zc1kZw0rr',
        name: 'NaturePeople',
      },
      {
        id: 'COV7jQpujEX',
        name: 'NavigateBefore',
      },
      {
        id: 'hyRBiX7GPZn',
        name: 'NavigateNext',
      },
      {
        id: '5$UwI7yKFyB',
        name: 'Palette',
      },
      {
        id: 'D$SJcVjO5cz',
        name: 'Panorama',
      },
      {
        id: 'JRavfbgttKF',
        name: 'PanoramaFishEye',
      },
      {
        id: 'LUAh$6vhIUE',
        name: 'PanoramaHorizontal',
      },
      {
        id: 'IdDuickPiYL',
        name: 'PanoramaVertical',
      },
      {
        id: '8Hz$PMFbDpH',
        name: 'PanoramaWideAngle',
      },
      {
        id: 'VFEq9Yt4unH',
        name: 'Photo',
      },
      {
        id: 'XRt$AoW1qGG',
        name: 'PhotoAlbum',
      },
      {
        id: '6iyXII5p0DM',
        name: 'PhotoCamera',
      },
      {
        id: 'RnF5cXtzCdJ',
        name: 'PhotoFilter',
      },
      {
        id: 'Xpa1cxczqEB',
        name: 'PhotoLibrary',
      },
      {
        id: 'KVetOpV8o$w',
        name: 'PhotoSizeSelectActual',
      },
      {
        id: 'qTxPh2JOD3F',
        name: 'PhotoSizeSelectLarge',
      },
      {
        id: 'of1MKWqJfeg',
        name: 'PhotoSizeSelectSmall',
      },
      {
        id: 'nrw0NWlZzy8',
        name: 'PictureAsPdf',
      },
      {
        id: 'DQixetGpHmI',
        name: 'Portrait',
      },
      {
        id: 'MITaBt_v9Za',
        name: 'RemoveRedEye',
      },
      {
        id: 'T0nEW13ZkUL',
        name: 'Rotate90DegreesCcw',
      },
      {
        id: 'fMZ2GZ3t8cQ',
        name: 'RotateLeft',
      },
      {
        id: '270cM96yayZ',
        name: 'RotateRight',
      },
      {
        id: 'RJj0WceMi2_',
        name: 'ShutterSpeed',
      },
      {
        id: 'e589Zedyi$c',
        name: 'Slideshow',
      },
      {
        id: 'AGN5fDr6Lk0',
        name: 'Straighten',
      },
      {
        id: 'vc0MwJFPfLv',
        name: 'Style',
      },
      {
        id: 'RTimVLBwhSn',
        name: 'SwitchCamera',
      },
      {
        id: 'gRTKZSMn3yB',
        name: 'SwitchVideo',
      },
      {
        id: 'rPdnj7R1oA_',
        name: 'TagFaces',
      },
      {
        id: 'BJYZ6n6kY3y',
        name: 'Texture',
      },
      {
        id: '_319zNSm6kZ',
        name: 'Timelapse',
      },
      {
        id: 'ItGI8oeGG4P',
        name: 'Timer',
      },
      {
        id: 'nPWL9JAO75o',
        name: 'Timer10',
      },
      {
        id: 'NqqCLuZ2uTA',
        name: 'Timer3',
      },
      {
        id: 'eGRcqOF_c7k',
        name: 'TimerOff',
      },
      {
        id: '2O7KNM7T7Ft',
        name: 'Tonality',
      },
      {
        id: 'w7_MAVCp7iq',
        name: 'Transform',
      },
      {
        id: 'MFCKy0liIKk',
        name: 'Tune',
      },
      {
        id: 'Tse6AFhsvcV',
        name: 'ViewComfy',
      },
      {
        id: '5xylt2Z2cvw',
        name: 'ViewCompact',
      },
      {
        id: 'NWyxGWyAH$Y',
        name: 'Vignette',
      },
      {
        id: 'sYKr2v$wvld',
        name: 'WbAuto',
      },
      {
        id: 'vunrF5ouqG3',
        name: 'WbCloudy',
      },
      {
        id: 'Tco$_69CfHv',
        name: 'WbIncandescent',
      },
      {
        id: 'gF1OCOFgPg_',
        name: 'WbIridescent',
      },
      {
        id: '3NRuqNbzOBE',
        name: 'WbSunny',
      },
    ],
  },
  maps: {
    title: 'Maps',
    icons: [
      {
        id: 'C7MMCFeRPsH',
        name: 'ThreeSixty',
      },
      {
        id: '6AZFR6bLngQ',
        name: 'Atm',
      },
      {
        id: 'OhyIWMGP4iT',
        name: 'Beenhere',
      },
      {
        id: 'TxolaLlDjwk',
        name: 'Category',
      },
      {
        id: 'LhhQgNuHsFy',
        name: 'CompassCalibration',
      },
      {
        id: 't_3HeoZBmsh',
        name: 'DepartureBoard',
      },
      {
        id: 'Af$_FzLL1a1',
        name: 'Directions',
      },
      {
        id: 'O8Po5M7MbiJ',
        name: 'DirectionsBike',
      },
      {
        id: '33QMfBSxZdf',
        name: 'DirectionsBoat',
      },
      {
        id: 'I7o2E6FZjLd',
        name: 'DirectionsBus',
      },
      {
        id: 'ybvVIWF3DKN',
        name: 'DirectionsCar',
      },
      {
        id: 'BoMhGabYsUl',
        name: 'DirectionsRailway',
      },
      {
        id: 'SMAno4NdAmV',
        name: 'DirectionsRun',
      },
      {
        id: 'bWdlagsRJJJ',
        name: 'DirectionsSubway',
      },
      {
        id: 'omvuhX13THu',
        name: 'DirectionsTransit',
      },
      {
        id: '0uNnwqJ80ub',
        name: 'DirectionsWalk',
      },
      {
        id: 'hbJ0jp1bLFB',
        name: 'EditAttributes',
      },
      {
        id: 'irTSG8PIUGN',
        name: 'EvStation',
      },
      {
        id: 'F$ZzMyy$KRl',
        name: 'Fastfood',
      },
      {
        id: 'bhDncK$jI1M',
        name: 'Flight',
      },
      {
        id: 'z0Z2ZYR3DLd',
        name: 'Hotel',
      },
      {
        id: 'L6MIwoX4Spo',
        name: 'Layers',
      },
      {
        id: 'rLt$lqhMk1G',
        name: 'LayersClear',
      },
      {
        id: 'sejUPKFKvmM',
        name: 'LocalActivity',
      },
      {
        id: '9tBBOoj839i',
        name: 'LocalAirport',
      },
      {
        id: 'e16ffRVQXwT',
        name: 'LocalAtm',
      },
      {
        id: 'C9QvkIMYuhw',
        name: 'LocalBar',
      },
      {
        id: '64iSdN7659F',
        name: 'LocalCafe',
      },
      {
        id: 'yJ9JhDX_fKv',
        name: 'LocalCarWash',
      },
      {
        id: 'ffpBCp240g9',
        name: 'LocalConvenienceStore',
      },
      {
        id: 'sCKv_0fMB6L',
        name: 'LocalDining',
      },
      {
        id: 'fAFbQC_Bggo',
        name: 'LocalDrink',
      },
      {
        id: 'Mjon3PmsDwy',
        name: 'LocalFlorist',
      },
      {
        id: 'lEKUYVwYBGy',
        name: 'LocalGasStation',
      },
      {
        id: 'kD05a0CwhKF',
        name: 'LocalGroceryStore',
      },
      {
        id: 'oZTJ2KxWQFe',
        name: 'LocalHospital',
      },
      {
        id: '4hismMM0TXN',
        name: 'LocalHotel',
      },
      {
        id: 'SA3ETFYUu7r',
        name: 'LocalLaundryService',
      },
      {
        id: 'Zf$ONRUIg4Z',
        name: 'LocalLibrary',
      },
      {
        id: 'C9sZd3_iHNB',
        name: 'LocalMall',
      },
      {
        id: 'IfAr3HO2k1k',
        name: 'LocalMovies',
      },
      {
        id: 'zI4Te_WzPPJ',
        name: 'LocalOffer',
      },
      {
        id: 'Vw4TdDhIUjR',
        name: 'LocalParking',
      },
      {
        id: '1hbbBrkV035',
        name: 'LocalPharmacy',
      },
      {
        id: '7karhChLz5C',
        name: 'LocalPhone',
      },
      {
        id: 'PfgKfHfX3$a',
        name: 'LocalPizza',
      },
      {
        id: 'duP1XHVWGOZ',
        name: 'LocalPlay',
      },
      {
        id: '_p0Q43b33Wo',
        name: 'LocalPostOffice',
      },
      {
        id: '9vU$SWnANog',
        name: 'LocalPrintshop',
      },
      {
        id: 'o0bNWgzvtyJ',
        name: 'LocalSee',
      },
      {
        id: 'McY_CWfmEQe',
        name: 'LocalShipping',
      },
      {
        id: '5qmzzhsyhwm',
        name: 'LocalTaxi',
      },
      {
        id: '3SAIQRXGeQ0',
        name: 'Map',
      },
      {
        id: 'ca5Z$zLgOMl',
        name: 'MenuBook',
      },
      {
        id: 'Lo2_4WY6Qi_',
        name: 'Money',
      },
      {
        id: 'G$IFFYXDv$$',
        name: 'Museum',
      },
      {
        id: 'SkPV35KsHXE',
        name: 'MyLocation',
      },
      {
        id: 'NdszSZYsDeT',
        name: 'Navigation',
      },
      {
        id: 'lkYdDEVD16e',
        name: 'NearMe',
      },
      {
        id: 'ugYx1WyCDg0',
        name: 'PersonPin',
      },
      {
        id: '9t0Osgf_I7U',
        name: 'RateReview',
      },
      {
        id: 'E4t2vjf0YE3',
        name: 'Restaurant',
      },
      {
        id: 'slRDYD0hdOU',
        name: 'RestaurantMenu',
      },
      {
        id: '$0G6K_crQ2f',
        name: 'Satellite',
      },
      {
        id: 'rIhegPeUESy',
        name: 'StoreMallDirectory',
      },
      {
        id: 'R$BpTafNgrH',
        name: 'Streetview',
      },
      {
        id: '7BOH24GtNif',
        name: 'Subway',
      },
      {
        id: 'As3ypnsscGT',
        name: 'Terrain',
      },
      {
        id: 'TOIE97NyKUZ',
        name: 'Traffic',
      },
      {
        id: 'pdOTRNtmG7r',
        name: 'Train',
      },
      {
        id: 'dEj2XC93l1G',
        name: 'Tram',
      },
      {
        id: 'Y2n80TxqB75',
        name: 'TransferWithinAStation',
      },
      {
        id: 'v6DpIiKhav_',
        name: 'TransitEnterexit',
      },
      {
        id: 'wQi19rtwjXR',
        name: 'TripOrigin',
      },
      {
        id: 'a_qGFkGmoPr',
        name: 'ZoomOutMap',
      },
    ],
  },
  navigation: {
    title: 'Navigation',
    icons: [
      {
        id: 'o$XbrwRHj2V',
        name: 'Apps',
      },
      {
        id: 'AIjLwpPGx0S',
        name: 'ArrowBack',
      },
      {
        id: 'PwsVwjW56Ha',
        name: 'ArrowBackIos',
      },
      {
        id: '8fpMybr9SBE',
        name: 'ArrowDownward',
      },
      {
        id: 'FD0DgoqQCgT',
        name: 'ArrowDropDown',
      },
      {
        id: 'gBQ_G85Stcw',
        name: 'ArrowDropDownCircle',
      },
      {
        id: 'AjEA0n2dZxW',
        name: 'ArrowDropUp',
      },
      {
        id: 'K_upLsELoHU',
        name: 'ArrowForward',
      },
      {
        id: 'RHF1qX3buGs',
        name: 'ArrowForwardIos',
      },
      {
        id: '3krEM$tXWK6',
        name: 'ArrowLeft',
      },
      {
        id: 'mt28S9KWDAT',
        name: 'ArrowRight',
      },
      {
        id: 'HZCVJ_PoK3b',
        name: 'ArrowUpward',
      },
      {
        id: 'YEM4_g9LGiA',
        name: 'Cancel',
      },
      {
        id: '3sj4toTK9Vf',
        name: 'Check',
      },
      {
        id: 'rSQQJ$4ug35',
        name: 'ChevronLeft',
      },
      {
        id: 'YspDrbJlsX9',
        name: 'ChevronRight',
      },
      {
        id: 'pjWmRVHH9oG',
        name: 'Close',
      },
      {
        id: 'Y6l7NJ1xSNY',
        name: 'DoubleArrow',
      },
      {
        id: 'm0YHu_gyzCm',
        name: 'ExpandLess',
      },
      {
        id: 'Wqa1EWduuuV',
        name: 'ExpandMore',
      },
      {
        id: 'DzFWSobeZgZ',
        name: 'FirstPage',
      },
      {
        id: 'uT73Sh2gXVz',
        name: 'Fullscreen',
      },
      {
        id: 'uQJ6g9aePU4',
        name: 'FullscreenExit',
      },
      {
        id: 'LbugBdQe3wl',
        name: 'HomeWork',
      },
      {
        id: 'LBxJZAUHJpN',
        name: 'LastPage',
      },
      {
        id: '2NfTRZQ1yem',
        name: 'Menu',
      },
      {
        id: 'Wewj0TeC5l9',
        name: 'MenuOpen',
      },
      {
        id: 'eIytNJ7lQhp',
        name: 'MoreHoriz',
      },
      {
        id: 'yMZTP9ZY88b',
        name: 'MoreVert',
      },
      {
        id: 'Bg7mnsauKDK',
        name: 'Refresh',
      },
      {
        id: 'Vz3_xaOzy52',
        name: 'SubdirectoryArrowLeft',
      },
      {
        id: 'xdob3X0ngsG',
        name: 'SubdirectoryArrowRight',
      },
      {
        id: '40g$0lQQSuR',
        name: 'UnfoldLess',
      },
      {
        id: 'HTdm1oYG_Rg',
        name: 'UnfoldMore',
      },
    ],
  },
  notification: {
    title: 'Notification',
    icons: [
      {
        id: 'U$xWU4vnf6K',
        name: 'AccountTree',
      },
      {
        id: 'F8_EXIzNl44',
        name: 'Adb',
      },
      {
        id: '9QjgLyeoHjq',
        name: 'AirlineSeatFlat',
      },
      {
        id: 'CZDo4PV0iJa',
        name: 'AirlineSeatFlatAngled',
      },
      {
        id: 'eEQnWaHQyHr',
        name: 'AirlineSeatIndividualSuite',
      },
      {
        id: 'M4RtSCFQaxx',
        name: 'AirlineSeatLegroomExtra',
      },
      {
        id: 'xVB5lOIm1T2',
        name: 'AirlineSeatLegroomNormal',
      },
      {
        id: 'Oio1J2b_HGN',
        name: 'AirlineSeatLegroomReduced',
      },
      {
        id: 'jBr5Ra1xEyd',
        name: 'AirlineSeatReclineExtra',
      },
      {
        id: 'qNPoW9Jm4le',
        name: 'AirlineSeatReclineNormal',
      },
      {
        id: 'By0SWOJhbxe',
        name: 'BluetoothAudio',
      },
      {
        id: 'dmsAKjmG_X4',
        name: 'ConfirmationNumber',
      },
      {
        id: '3D1Mxu47Qtj',
        name: 'DiscFull',
      },
      {
        id: 'zZyOGdTDRrj',
        name: 'DriveEta',
      },
      {
        id: 'SlPinyskV1n',
        name: 'EnhancedEncryption',
      },
      {
        id: 'vxB02Dxyxvj',
        name: 'EventAvailable',
      },
      {
        id: 'LPYJ2N1hkYO',
        name: 'EventBusy',
      },
      {
        id: 'fXHdti1SMZY',
        name: 'EventNote',
      },
      {
        id: 'Oi45uV5GjYI',
        name: 'FolderSpecial',
      },
      {
        id: 'KzVyh4tZtpX',
        name: 'LiveTv',
      },
      {
        id: 'pVKCZ4neRli',
        name: 'Mms',
      },
      {
        id: 'asxVDc8CSst',
        name: 'More',
      },
      {
        id: 'vaVXnHhIjZ3',
        name: 'NetworkCheck',
      },
      {
        id: 'cddLGhaKrHo',
        name: 'NetworkLocked',
      },
      {
        id: '9NiUgbq_xhk',
        name: 'NoEncryption',
      },
      {
        id: '6V9fYkirwFj',
        name: 'OndemandVideo',
      },
      {
        id: 'sEv8VQ$itjQ',
        name: 'PersonalVideo',
      },
      {
        id: 'KjV2kdZFCVf',
        name: 'PhoneBluetoothSpeaker',
      },
      {
        id: 'zemlp_YEsIh',
        name: 'PhoneCallback',
      },
      {
        id: 'AzXgsrfVlBh',
        name: 'PhoneForwarded',
      },
      {
        id: 'c$dlyXez9zJ',
        name: 'PhoneInTalk',
      },
      {
        id: 'EEhtY7apvrH',
        name: 'PhoneLocked',
      },
      {
        id: 'mvFCiwabObZ',
        name: 'PhoneMissed',
      },
      {
        id: 'ewtIkqdgL$s',
        name: 'PhonePaused',
      },
      {
        id: 'poXLKwmK2wU',
        name: 'Power',
      },
      {
        id: 'LM0nLGEiafE',
        name: 'PowerOff',
      },
      {
        id: 'tUcPCbF$lcr',
        name: 'PriorityHigh',
      },
      {
        id: 'X150Lavv4OD',
        name: 'SdCard',
      },
      {
        id: 'S_D78p1I_RD',
        name: 'Sms',
      },
      {
        id: 't4yOmVHE43i',
        name: 'SmsFailed',
      },
      {
        id: 'bGQcGm4dguP',
        name: 'Sync',
      },
      {
        id: 'vcyqaosq2Xz',
        name: 'SyncDisabled',
      },
      {
        id: '6NEFrIPiNr7',
        name: 'SyncProblem',
      },
      {
        id: '5pH2vRkCpnz',
        name: 'SystemUpdate',
      },
      {
        id: 'BV1LuX$pNEE',
        name: 'TapAndPlay',
      },
      {
        id: 'CZdg5kinI2M',
        name: 'TimeToLeave',
      },
      {
        id: '1ZpxteUOn2_',
        name: 'TvOff',
      },
      {
        id: 'qQF$SjSbavg',
        name: 'Vibration',
      },
      {
        id: '67iit9JJKnO',
        name: 'VoiceChat',
      },
      {
        id: 'mJqPnM5WS46',
        name: 'VpnLock',
      },
      {
        id: 'YLQCjL$eDid',
        name: 'Wc',
      },
      {
        id: '3H3HFkPeOtC',
        name: 'Wifi',
      },
      {
        id: 'agXspu790Yr',
        name: 'WifiOff',
      },
    ],
  },
  places: {
    title: 'Places',
    icons: [
      {
        id: '3NCYsQC3HPs',
        name: 'AcUnit',
      },
      {
        id: 'X_hda$XqJEy',
        name: 'AirportShuttle',
      },
      {
        id: 'glu85ZiFzaS',
        name: 'AllInclusive',
      },
      {
        id: 'gzfexW9w3I4',
        name: 'Apartment',
      },
      {
        id: 's$NvNQoS_10',
        name: 'Bathtub',
      },
      {
        id: 'h_f7_xtPg$W',
        name: 'BeachAccess',
      },
      {
        id: 'MA5mQHEmvln',
        name: 'BusinessCenter',
      },
      {
        id: 'Ku9TBZdHYlD',
        name: 'Casino',
      },
      {
        id: 'ccy1UcV9Xr$',
        name: 'ChildCare',
      },
      {
        id: 'iMZN9YxawN8',
        name: 'ChildFriendly',
      },
      {
        id: 'W55D6Ve_Rvx',
        name: 'FitnessCenter',
      },
      {
        id: 'ry0E3PvirDL',
        name: 'FreeBreakfast',
      },
      {
        id: 'EyjSRIWln7d',
        name: 'GolfCourse',
      },
      {
        id: '_jop8wzwPTR',
        name: 'HotTub',
      },
      {
        id: '_dHv0bvQjns',
        name: 'House',
      },
      {
        id: '9b35kQeRk5q',
        name: 'Kitchen',
      },
      {
        id: 'ZPuZWKKd22F',
        name: 'MeetingRoom',
      },
      {
        id: 'vN5P$vKBDwf',
        name: 'NoMeetingRoom',
      },
      {
        id: '91U_HuiInJn',
        name: 'Pool',
      },
      {
        id: '887t$Ioe0KN',
        name: 'RoomService',
      },
      {
        id: 'GefxIG$88uA',
        name: 'RvHookup',
      },
      {
        id: '4UEH8G69ze8',
        name: 'SmokeFree',
      },
      {
        id: '94j$wb_dcnG',
        name: 'SmokingRooms',
      },
      {
        id: 'XDkngWWwgwb',
        name: 'Spa',
      },
      {
        id: 'mijHXrdUlV9',
        name: 'Storefront',
      },
    ],
  },
  social: {
    title: 'Social',
    icons: [
      {
        id: 'MEYOBcog4QV',
        name: 'Cake',
      },
      {
        id: 'RLvV5YtOVvq',
        name: 'Deck',
      },
      {
        id: '$gEYeELZTDd',
        name: 'EmojiEmotions',
      },
      {
        id: '3QK20Qxg7nT',
        name: 'EmojiEvents',
      },
      {
        id: 'zAKf1T$QtYf',
        name: 'EmojiFlags',
      },
      {
        id: 'ZMZegK8yKJC',
        name: 'EmojiFoodBeverage',
      },
      {
        id: '9JHep3diSkD',
        name: 'EmojiNature',
      },
      {
        id: 'Q4EdevgfF5L',
        name: 'EmojiObjects',
      },
      {
        id: 'R6mKC8AvEPN',
        name: 'EmojiPeople',
      },
      {
        id: 'DF7PCEbzP2l',
        name: 'EmojiSymbols',
      },
      {
        id: 'WeS5HcLOxpE',
        name: 'EmojiTransportation',
      },
      {
        id: 'ChWBAk9T4I3',
        name: 'Fireplace',
      },
      {
        id: 'y2F1u4Is$Zy',
        name: 'Group',
      },
      {
        id: '7Nt$jbNUXIH',
        name: 'GroupAdd',
      },
      {
        id: 'vpiCitG_jfH',
        name: 'KingBed',
      },
      {
        id: '2tY4flH_OhD',
        name: 'LocationCity',
      },
      {
        id: 'sw1lnoG$xvN',
        name: 'Mood',
      },
      {
        id: 'suYha6xXYhW',
        name: 'MoodBad',
      },
      {
        id: 'a3pNWlIl_Es',
        name: 'NightsStay',
      },
      {
        id: 'NA6evd0qfps',
        name: 'Notifications',
      },
      {
        id: '1cHLhARhrGr',
        name: 'NotificationsActive',
      },
      {
        id: 'NrUHmF0dzgp',
        name: 'NotificationsNone',
      },
      {
        id: 'sQnVvGHTOkY',
        name: 'NotificationsOff',
      },
      {
        id: 'Df37KMxNVuT',
        name: 'NotificationsPaused',
      },
      {
        id: '4_IaLFk9xGJ',
        name: 'OutdoorGrill',
      },
      {
        id: '4HdRYqnlBpr',
        name: 'Pages',
      },
      {
        id: 'GEKiDiJNW1n',
        name: 'PartyMode',
      },
      {
        id: '$AzXc4VFatS',
        name: 'People',
      },
      {
        id: '_FshLFnm$Uw',
        name: 'PeopleAlt',
      },
      {
        id: 'NRI5wtM4RZa',
        name: 'PeopleOutline',
      },
      {
        id: 'bTJxYZX1_ca',
        name: 'Person',
      },
      {
        id: 'piNibPOWAgC',
        name: 'PersonAdd',
      },
      {
        id: 'HWlygAdPnKs',
        name: 'PersonOutline',
      },
      {
        id: 'p7xOqPveYwR',
        name: 'PlusOne',
      },
      {
        id: 'lcoVjOZhikv',
        name: 'Poll',
      },
      {
        id: 'rDWdv0xaUOo',
        name: 'Public',
      },
      {
        id: 'V6jBSn1jt4e',
        name: 'School',
      },
      {
        id: 'de1RV3cq96Y',
        name: 'SentimentDissatisfied',
      },
      {
        id: 'bjl1wi6Zt9E',
        name: 'SentimentSatisfied',
      },
      {
        id: 'XjA28$SrxVB',
        name: 'SentimentVeryDissatisfied',
      },
      {
        id: 'jSrLmupYxN7',
        name: 'SentimentVerySatisfied',
      },
      {
        id: '04nGnDc1T4t',
        name: 'Share',
      },
      {
        id: 'daKJkYiSHIR',
        name: 'SingleBed',
      },
      {
        id: 'BFvo8xPzRxZ',
        name: 'Sports',
      },
      {
        id: 'RqnM$grMUH2',
        name: 'SportsBaseball',
      },
      {
        id: 'h00wHH3zlWo',
        name: 'SportsBasketball',
      },
      {
        id: 'AZT1ciXha2d',
        name: 'SportsCricket',
      },
      {
        id: 'hv8q4gk6CDf',
        name: 'SportsEsports',
      },
      {
        id: '_weyeybnajn',
        name: 'SportsFootball',
      },
      {
        id: 'qhUdul$34xg',
        name: 'SportsGolf',
      },
      {
        id: 'zjtJXNbxWN8',
        name: 'SportsHandball',
      },
      {
        id: '8CsjubjkTt9',
        name: 'SportsHockey',
      },
      {
        id: 'RxLVvn2WyuT',
        name: 'SportsKabaddi',
      },
      {
        id: 'L6vjH8pDOmv',
        name: 'SportsMma',
      },
      {
        id: 'YyVquJu_JhS',
        name: 'SportsMotorsports',
      },
      {
        id: 'GHRtsAp0mnW',
        name: 'SportsRugby',
      },
      {
        id: 'mfTJPUQURBx',
        name: 'SportsSoccer',
      },
      {
        id: 'snvPguton3R',
        name: 'SportsTennis',
      },
      {
        id: 'Rc4L5lQk6PK',
        name: 'SportsVolleyball',
      },
      {
        id: 'cJtIO63fGoD',
        name: 'ThumbDownAlt',
      },
      {
        id: 'AvxALTICoI0',
        name: 'ThumbUpAlt',
      },
      {
        id: 'n3WjueM_TCJ',
        name: 'Whatshot',
      },
    ],
  },
  toggle: {
    title: 'Toggle',
    icons: [
      {
        id: 'B1NLyze7min',
        name: 'CheckBox',
      },
      {
        id: 'qWNWyqtuqSD',
        name: 'CheckBoxOutlineBlank',
      },
      {
        id: 'bBhwtZFLXau',
        name: 'IndeterminateCheckBox',
      },
      {
        id: 'Vw1HIsvyXaY',
        name: 'RadioButtonChecked',
      },
      {
        id: 'rfkvC7ok3hm',
        name: 'RadioButtonUnchecked',
      },
      {
        id: 'xWbZaJyX9R6',
        name: 'Star',
      },
      {
        id: 'pdbEjBAr54r',
        name: 'StarBorder',
      },
      {
        id: 'Bkx6xJ5IOY_',
        name: 'StarHalf',
      },
      {
        id: 'kUxo7jDZlLk',
        name: 'ToggleOff',
      },
      {
        id: 'b81sjLeTh2B',
        name: 'ToggleOn',
      },
    ],
  },
};
