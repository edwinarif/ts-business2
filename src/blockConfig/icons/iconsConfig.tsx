import { StaticBlockVariant } from 'blockConfig/componentsMapper';
import categoriesArr from './categoriesArr';

export const iconsConfig: StaticBlockVariant[] = Object.keys(categoriesArr).map(
  category => {
    return {
      [category]: {
        label: (categoriesArr as any)[category].title,
        blocks: (categoriesArr as any)[category].icons.map((icon: any) => ({
          id: icon.id,
          blockId: '',
          blockType: `MuiIcon${icon.name}`,
        })),
      },
    };
  }
);
