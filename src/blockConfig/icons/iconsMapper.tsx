import React from 'react';
import categoriesArr from './categoriesArr';
import Icon from '@material-ui/core/Icon';
import decamelize from 'decamelize';

export default Object.keys(categoriesArr).reduce((res, category) => {
  const childs = (categoriesArr as any)[category].icons.reduce(
    (res2: any, prev: any) => {
      return {
        ...res2,
        [`MuiIcon${prev.name}`]: {
          Component: () => <Icon>{decamelize(prev.name)}</Icon>,
          importStatement: `import ${prev.name}Icon from '@material-ui/icons/${prev.name}'`,
          componentName: `${prev.name}Icon`,
        },
      };
    },
    {} as { [key: string]: any }
  );

  return {
    ...res,
    ...childs,
  };
}, {} as { [key: string]: any });
