import React from 'react';
import { ComponentMapper, ComponentMapperTyped } from './componentsMapper';
import muiConstants from '../muiConstants';
import Grid, { GridProps } from 'muicore/Grid';
import Button, { ButtonProps } from 'muicore/Button';
import Typography, { TypographyProps } from 'muicore/Typography';
import Paper, { PaperProps } from 'muicore/Paper';
import Checkbox, { CheckboxProps } from 'muicore/Checkbox';
import Slider, { SliderProps } from 'muicore/Slider';
import Container, { ContainerProps } from 'muicore/Container';
import TextField, { TextFieldProps } from 'muicore/TextField';
import AppBar, { AppBarProps } from 'muicore/AppBar';
import Toolbar, { ToolbarProps } from 'muicore/Toolbar';
import ButtonGroup, { ButtonGroupProps } from 'muicore/ButtonGroup';
import Fab, { FabProps } from 'muicore/Fab';
import IconButton, { IconButtonProps } from 'muicore/IconButton';
import iconsMapper from './icons/iconsMapper';

export type BlockTypes =
  | 'HTMLDivElement'
  | 'MuiContainer'
  | 'MuiGrid'
  | 'MuiButton'
  | 'MuiFab'
  | 'MuiBasicIconButton'
  | 'MuiButtonGroup'
  | 'MuiTypography'
  | 'MuiPaper'
  | 'MuiCheckbox'
  | 'MuiSlider'
  | 'MuiTextField'
  | 'MuiAppBar'
  | 'MuiToolbar';

export type ComponentPropsType =
  | HTMLDivElement
  | ContainerProps
  | GridProps
  | ButtonProps
  | FabProps
  | IconButtonProps
  | ButtonGroupProps
  | TypographyProps
  | PaperProps
  | CheckboxProps
  | SliderProps
  | TextFieldProps
  | AppBarProps
  | ToolbarProps;

const getMuiCoreImport = (componentName: string) =>
  `import {${componentName}} from '@material-ui/core'`;

export const componentsMapper: ComponentMapper = {
  ...iconsMapper,
  MuiAppBar: {
    Component: AppBar,
    importStatement: getMuiCoreImport('AppBar'),
    componentName: 'AppBar',
    propsConfig: {
      position: {
        default: 'fixed',
        inputType: 'Select',
        options: ['absolute', 'fixed', 'relative', 'static', 'sticky'],
      },
    },
  } as ComponentMapperTyped<AppBarProps>,
  MuiToolbar: {
    Component: Toolbar,
    importStatement: getMuiCoreImport('Toolbar'),
    componentName: 'Toolbar',
  } as ComponentMapperTyped<ToolbarProps>,
  HTMLDivElement: {
    Component: () => <div />,
    importStatement: '',
    componentName: 'div',
  } as ComponentMapperTyped<HTMLDivElement>,
  MuiContainer: {
    Component: Container,
    importStatement: getMuiCoreImport('Container'),
    componentName: 'Container',
    propsConfig: {
      children: { default: <></>, value: <></> },
      fixed: {
        default: false,
        inputType: 'Switch',
      },
      maxWidth: {
        default: 'lg',
        inputType: 'Select',
        options: ['xs', 'sm', 'md', 'lg', 'xl', false],
      },
    },
  } as ComponentMapperTyped<ContainerProps>,
  MuiButton: {
    Component: Button,
    importStatement: getMuiCoreImport('Button'),
    componentName: 'Button',
    propsConfig: {
      children: {
        default: undefined,
        label: 'Text',
        inputType: 'TextField',
        required: true,
      },
      // href: {
      //   default: '',
      //   label: 'href',
      //   componentType: 'TextField',
      // },
      color: {
        default: 'default',
        inputType: 'Select',
        options: muiConstants.color,
      },
      variant: {
        default: 'text',
        inputType: 'Select',
        options: ['text', 'outlined', 'contained'],
      },
      size: {
        default: 'medium',
        inputType: 'Select',
        options: ['small', 'medium', 'large'],
      },
      fullWidth: { default: false, inputType: 'Switch' },
      disableElevation: { default: false, inputType: 'Switch' },
      startIcon: {
        default: undefined,
        inputType: 'Node',
      },
      endIcon: {
        default: undefined,
        inputType: 'Node',
      },
      disabled: { default: false, inputType: 'Switch' },
      disableRipple: { default: false, inputType: 'Switch' },
      disableFocusRipple: { default: false, inputType: 'Switch' },
    },
    customConfig: {
      droppable: false,
    },
  } as ComponentMapperTyped<ButtonProps>,
  MuiFab: {
    Component: Fab,
    importStatement: getMuiCoreImport('Fab'),
    componentName: 'Fab',
    propsConfig: {
      children: {
        default: undefined,
        label: 'Text',
        inputType: 'TextField',
        required: true,
      },
      color: {
        default: 'default',
        inputType: 'Select',
        options: muiConstants.color,
      },
      variant: {
        default: 'round',
        inputType: 'Select',
        options: ['round', 'extended'],
      },
      size: {
        default: 'medium',
        inputType: 'Select',
        options: ['small', 'medium', 'large'],
      },
      disabled: { default: false, inputType: 'Switch' },
      disableRipple: { default: false, inputType: 'Switch' },
      disableFocusRipple: { default: false, inputType: 'Switch' },
    },
    customConfig: {
      droppable: false,
    },
  } as ComponentMapperTyped<FabProps>,
  MuiBasicIconButton: {
    Component: IconButton,
    importStatement: getMuiCoreImport('IconButton'),
    componentName: 'IconButton',
    propsConfig: {
      children: {
        default: undefined,
        label: 'Text',
        inputType: 'TextField',
        required: true,
      },
      color: {
        default: 'default',
        inputType: 'Select',
        options: muiConstants.color,
      },
      edge: {
        default: undefined,
        inputType: 'Select',
        options: ['start', 'end'],
      },
      size: {
        default: 'medium',
        inputType: 'Select',
        options: ['small', 'medium'],
      },
      disabled: { default: false, inputType: 'Switch' },
      disableRipple: { default: false, inputType: 'Switch' },
      disableFocusRipple: { default: false, inputType: 'Switch' },
    },
  } as ComponentMapperTyped<IconButtonProps>,
  MuiButtonGroup: {
    Component: ButtonGroup,
    importStatement: getMuiCoreImport('ButtonGroup'),
    componentName: 'ButtonGroup',
    propsConfig: {
      color: {
        default: 'default',
        inputType: 'Select',
        options: muiConstants.color,
      },
      variant: {
        default: 'text',
        inputType: 'Select',
        options: ['text', 'outlined', 'contained'],
      },
      size: {
        default: 'medium',
        inputType: 'Select',
        options: ['small', 'medium', 'large'],
      },
      orientation: {
        default: 'horizontal',
        inputType: 'Select',
        options: ['horizontal', 'vertical'],
      },
      fullWidth: { default: false, inputType: 'Switch' },
      disabled: { default: false, inputType: 'Switch' },
      disableRipple: { default: false, inputType: 'Switch' },
      disableFocusRipple: { default: false, inputType: 'Switch' },
    },
  } as ComponentMapperTyped<ButtonGroupProps>,
  MuiPaper: {
    Component: Paper,
    componentName: 'Paper',
    importStatement: getMuiCoreImport('Paper'),
    propsConfig: {
      elevation: {
        default: 1,
        label: 'elevation',
        inputType: 'TextField',
        type: 'number',
      },
      square: {
        default: false,
        inputType: 'Switch',
      },
    },
  } as ComponentMapperTyped<PaperProps>,
  MuiTypography: {
    Component: Typography,
    componentName: 'Typography',
    importStatement: getMuiCoreImport('Typography'),
    propsConfig: {
      children: {
        default: '',
        value: 'Typography',
        label: 'Text',
        inputType: 'TextField',
      },
      variant: {
        default: 'body1',
        inputType: 'Select',
        options: muiConstants.themeStyle,
      },
      align: {
        default: 'inherit',
        inputType: 'Select',
        options: muiConstants.align,
      },
      color: {
        default: 'initial',
        inputType: 'Select',
        options: [
          'initial',
          'inherit',
          'primary',
          'secondary',
          'textPrimary',
          'textSecondary',
          'error',
        ],
      },
      display: {
        default: 'initial',
        inputType: 'Select',
        options: ['initial', 'block', 'inline'],
      },
      gutterBottom: { default: false, inputType: 'Switch' },
      noWrap: { default: false, inputType: 'Switch' },
      paragraph: { default: false, inputType: 'Switch' },
    },
    customConfig: {
      droppable: false,
    },
  } as ComponentMapperTyped<TypographyProps>,
  MuiSlider: {
    Component: Slider,
    componentName: 'Slider',
    importStatement: getMuiCoreImport('Slider'),
    propsConfig: {
      disabled: {
        default: false,
        inputType: 'Switch',
      },
      min: {
        default: 0,
        inputType: 'TextField',
      },
      max: {
        default: 100,
        inputType: 'TextField',
      },
      step: {
        default: 1,
        inputType: 'TextField',
      },
      orientation: {
        default: 'horizontal',
        inputType: 'Select',
        options: ['horizontal', 'vertical'],
      },
      valueLabelDisplay: {
        default: 'off',
        inputType: 'Select',
        options: ['on', 'auto', 'off'],
      },
    },
    customConfig: {
      droppable: false,
    },
  } as ComponentMapperTyped<SliderProps>,
  MuiCheckbox: {
    Component: Checkbox,
    componentName: 'Checkbox',
    importStatement: getMuiCoreImport('Checkbox'),
    propsConfig: {
      id: {
        default: '',
        inputType: 'TextField',
      },
      value: {
        default: '',
        inputType: 'TextField',
      },
      checked: {
        default: true,
        inputType: 'Switch',
      },
      defaultChecked: {
        default: true,
        inputType: 'Switch',
      },
      color: {
        default: 'secondary',
        inputType: 'Select',
        options: ['primary', 'secondary', 'default'],
      },
      size: {
        default: 'medium',
        inputType: 'Select',
        options: ['small', 'medium'],
      },
      disabled: {
        default: false,
        inputType: 'Switch',
      },
      disableRipple: {
        default: false,
        inputType: 'Switch',
      },
      indeterminate: {
        default: false,
        inputType: 'Switch',
      },
      onChange: {
        default: () => {},
      },
    },
    customConfig: {
      droppable: false,
    },
  } as ComponentMapperTyped<CheckboxProps>,
  MuiGrid: {
    Component: Grid,
    componentName: 'Grid',
    importStatement: getMuiCoreImport('Grid'),
    propsConfig: {
      children: { default: '', label: 'Label', inputType: 'TextField' },
      container: { default: false, inputType: 'Switch' },
      item: { default: false, inputType: 'Switch' },
      alignContent: {
        default: 'stretch',
        inputType: 'Select',
        options: [
          'stretch',
          'center',
          'flex-start',
          'flex-end',
          'space-between',
          'space-around',
        ],
      },
      alignItems: {
        default: 'stretch',
        inputType: 'Select',
        options: ['flex-start', 'center', 'flex-end', 'stretch', 'baseline'],
      },
      direction: {
        default: 'row',
        inputType: 'Select',
        options: ['row', 'row-reverse', 'column', 'column-reverse'],
      },
      spacing: {
        default: 0,
        inputType: 'Select',
        options: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      },
      justify: {
        default: 'flex-start',
        inputType: 'Select',
        options: [
          'flex-start',
          'center',
          'flex-end',
          'space-between',
          'space-around',
          'space-evenly',
        ],
      },
      wrap: {
        default: 'wrap',
        inputType: 'Select',
        options: ['nowrap', 'wrap', 'wrap-reverse'],
      },
      xs: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
      sm: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
      md: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
      lg: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
      xl: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
    },
  } as ComponentMapperTyped<GridProps>,
  MuiTextField: {
    Component: TextField,
    componentName: 'TextField',
    importStatement: getMuiCoreImport('TextField'),
    propsConfig: {
      variant: {
        default: 'standard',
        inputType: 'Select',
        options: ['standard', 'outlined', 'filled'],
      },
      label: { default: '', label: 'Label', inputType: 'TextField' },
      value: { default: '', label: 'Value', inputType: 'TextField' },
      defaultValue: {
        default: '',
        label: 'defaultValue',
        inputType: 'TextField',
      },
      helperText: { default: '', label: 'helperText', inputType: 'TextField' },
      // children: { default: '', label: 'Text', inputType: 'TextField' },
      placeholder: {
        default: '',
        label: 'placeholder',
        inputType: 'TextField',
      },
      autoComplete: {
        default: '',
        label: 'autoComplete',
        inputType: 'TextField',
      },
      autoFocus: { default: false, inputType: 'Switch' },
      disabled: { default: false, inputType: 'Switch' },
      error: { default: false, inputType: 'Switch' },
      // FormHelperTextProps?: Partial<FormHelperTextProps>;
      fullWidth: { default: false, inputType: 'Switch' },
      // InputLabelProps?: Partial<InputLabelProps>;
      // inputRef?: React.Ref<any>;
      // margin?: PropTypes.Margin;
      multiline: { default: false, inputType: 'Switch' },
      name: { default: '', label: 'name', inputType: 'TextField' },
      required: { default: false, inputType: 'Switch' },
      rows: { default: '', label: 'rows', inputType: 'TextField' },
      rowsMax: { default: '', label: 'rowsMax', inputType: 'TextField' },
      select: { default: false, inputType: 'Switch' },
      // SelectProps?: Partial<SelectProps>;
      type: { default: '', label: 'type', inputType: 'TextField' },
    },
  } as ComponentMapperTyped<TextFieldProps>,
};
