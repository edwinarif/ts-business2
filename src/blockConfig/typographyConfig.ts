import { StaticBlockVariant } from './componentsMapper';
import { GeneralComponentType } from 'types';

export const typographyConfig: StaticBlockVariant[] = [
  {
    id: 'BwALAAgPCQk',
    blockType: 'MuiTypography',
    props: {
      variant: 'h1',
      children: 'h1',
    },
  },
  {
    id: 'FQHorhPV1sm',
    blockType: 'MuiTypography',
    props: {
      variant: 'h2',
      children: 'h2',
    },
  },
  {
    id: 'QCYXur0es0X',
    blockType: 'MuiTypography',
    props: {
      variant: 'h3',
      children: 'h3',
    },
  },
  {
    id: 'xK75DTDhakp',
    blockType: 'MuiTypography',
    props: {
      variant: 'h4',
      children: 'h4',
    },
  },
  {
    id: 'pdhdMSOQkJ5',
    blockType: 'MuiTypography',
    props: {
      variant: 'h5',
      children: 'h5',
    },
  },
  {
    id: 'PbTvsNPxSiM',
    blockType: 'MuiTypography',
    props: {
      variant: 'h6',
      children: 'h6',
    },
  },
  {
    id: 'jwLEUgoSWvU',
    blockType: 'MuiTypography',
    props: {
      variant: 'button',
      children: 'button text',
    },
  },
  {
    id: 'IaGqDcJSokI',
    blockType: 'MuiTypography',
    props: {
      variant: 'caption',
      children: 'caption text',
    },
  },
  {
    id: 'CzoN2nndlxN',
    blockType: 'MuiTypography',
    props: {
      variant: 'overline',
      children: 'overline text',
    },
  },
  {
    id: 'JUcEvEqWIO3',
    blockType: 'MuiTypography',
    props: {
      variant: 'subtitle1',
      children:
        'subtitle1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur',
    },
  },
  {
    id: 'RhPAFkCwdbK',
    blockType: 'MuiTypography',
    props: {
      variant: 'subtitle2',
      children:
        'subtitle2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur',
    },
  },
  {
    id: 'uN6lbIroEjF',
    blockType: 'MuiTypography',
    props: {
      variant: 'body1',
      children:
        'body1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos laborum fugiat deleniti? Eum quasi quidem quibusdam.',
    },
  },
  {
    id: 'xLobCqTtV4K',
    blockType: 'MuiTypography',
    props: {
      variant: 'body2',
      children:
        'body2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos laborum fugiat deleniti? Eum quasi quidem quibusdam.',
    },
  },
].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[];
