import React from 'react';
import { CSSProperties } from '@material-ui/styles';
import { TextFieldProps } from 'muicore/TextField';
import { buttonsConfig } from './buttons/buttonsConfig';
import { typographyConfig } from './typographyConfig';
import {
  componentsMapper,
  ComponentPropsType as ComponentPropsLocal,
  BlockTypes as BlockTypesLocal,
} from './componentsMapperConfig';
import { GeneralComponentType } from 'types';
import { layoutsConfig } from './layoutsConfig';
import { iconsConfig } from './icons/iconsConfig';
import { omit } from 'utils/mixins';
import { checkboxesConfig } from './checkboxesConfig';

export type ComponentMapBase = {
  Component: React.FunctionComponent | React.ComponentType;
  componentName: string;
  importStatement: string;
  style?: CSSProperties;
};

export type PropsConfig<T extends ComponentPropsLocal> = {
  [key in keyof T]: {
    default: T[key];
    value?: T[key];
    inputType?: 'TextField' | 'Switch' | 'Select' | 'Node';
    label?: string;
    options?: Array<any>;
    required?: boolean;
    type?: string;
  };
};

export type ComponentMapperTyped<T extends ComponentPropsLocal> = {
  propsConfig?: PropsConfig<T>;
} & ComponentMapBase;

export type ComponentMapper = {
  [key in BlockTypes]: ComponentMapperTyped<ComponentPropsLocal>;
};

export const componentsKeys = Object.keys(componentsMapper) as Array<
  BlockTypes
>;

export const blockCategories: {
  [key in string]: {
    title: string;
    subTitle?: string;
    subCategories?: { [subCategoryId: string]: { title: string } };
  };
} = {
  customComponents: {
    title: 'Custom Components',
    subTitle: 'Your own created components',
  },
  layouts: {
    title: 'Layouts',
    subTitle: 'Div, Box, Grid, Container, +...',
  },
  buttons: {
    title: 'Buttons',
    subCategories: {
      containedButtons: { title: 'Contained Buttons' },
      textButtons: { title: 'Text Buttons' },
      outlinedButtons: { title: 'Outlined Buttons' },
      fabButtons: { title: 'Floating Action Buttons' },
      groupedButtons: { title: 'Grouped Buttons' },
      splitButtons: { title: 'Split Buttons' },
      withIconButtons: { title: 'With Icons and Label' },
      iconButtons: { title: 'Icons Buttons' },
    },
  },
  checkboxes: {
    title: 'Checkboxes',
    subCategories: {
      general: { title: '' },
      withFormControlLabel: { title: 'With FormControlLabel' },
      withFormGroup: { title: 'With FormGroup' },
      labelPlacement: { title: 'Label placement' },
    },
  },
  typography: {
    title: 'Typography',
  },
  icons: {
    title: 'Icons',
    subCategories: {
      action: { title: 'Action' },
      alert: { title: 'Alert' },
      av: { title: 'Av' },
      communication: { title: 'Communication' },
      content: { title: 'Content' },
      device: { title: 'Device' },
      editor: { title: 'Editor' },
      file: { title: 'File' },
      hardware: { title: 'Hardware' },
      image: { title: 'Image' },
      maps: { title: 'Maps' },
      navigation: { title: 'Navigation' },
      notification: { title: 'Notification' },
      places: { title: 'Places' },
      social: { title: 'Social' },
      toggle: { title: 'Toggle' },
    },
  },
  inputs: {
    title: 'Inputs',
    subTitle: 'Inputs, Text Fields, Forms, +...',
  },
};
export type BlockCategories = keyof typeof blockCategories;
export const categoryKeys = Object.keys(blockCategories) as BlockCategories[];

export type StaticBlockVariant =
  | GeneralComponentType
  | {
      [key: string]: {
        label: string;
        blocks: GeneralComponentType[];
      };
    };

export type StaticBlocks = {
  [key in BlockCategories]: StaticBlockVariant[];
};

export const defaultBlocks: StaticBlocks = {
  customComponents: [],
  layouts: layoutsConfig,
  buttons: buttonsConfig,
  checkboxes: checkboxesConfig,
  typography: typographyConfig,
  icons: iconsConfig,
  inputs: [
    {
      id: 'HFV5aPE0z0z',
      blockType: 'MuiTextField',
      blockId: '',
      label: 'Standard TextField',
      props: {
        label: 'Standard TextField',
      } as TextFieldProps,
    },
  ],
};

function getIsGeneralComponent(
  block: StaticBlockVariant
): block is GeneralComponentType {
  return (block as GeneralComponentType).id !== undefined;
}

export const getBlockList = (exclude?: string[]) => {
  return (Object.keys(
    exclude ? omit(defaultBlocks, exclude) : defaultBlocks
  ) as BlockCategories[]).reduce((prev, category) => {
    const blocks = defaultBlocks[category].reduce((prev, block) => {
      if (getIsGeneralComponent(block)) {
        return {
          ...prev,
          [block.id]: {
            ...block,
            category,
            label: block.label || '',
          },
        };
      }

      const subBlocks = Object.keys(block).reduce((subPrev, subCategory) => {
        return {
          ...subPrev,
          ...block[subCategory].blocks.reduce(
            (blockPrev, staticBlock) => ({
              ...blockPrev,
              [staticBlock.id]: {
                ...staticBlock,
                category,
                subCategory,
                label: staticBlock.label || '',
              },
            }),
            {} as { [key: string]: GeneralComponentType }
          ),
        };
      }, {} as { [key: string]: GeneralComponentType });

      return {
        ...prev,
        ...subBlocks,
      };
    }, {} as { [key: string]: GeneralComponentType });

    return { ...prev, ...blocks };
  }, {} as { [key: string]: GeneralComponentType });
};

export default componentsMapper;
export type ComponentPropsType = ComponentPropsLocal;
export type BlockTypes = BlockTypesLocal;
