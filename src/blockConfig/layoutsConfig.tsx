import { StaticBlockVariant } from './componentsMapper';
import { GeneralComponentType } from 'types';
import { GridProps } from 'muicore/Grid';

export const layoutsConfig: StaticBlockVariant[] = [
  {
    id: 'C5xQ9XUFhkg',
    blockType: 'MuiAppBar',
    label: 'AppBar',
    acceptDrop: 'box',
    customHtmlBlock: '<div>AppBar</div>',
    props: {
      position: 'static',
    },
    blockStyleCss: {
      outline: '2px dashed rgba(41,182,246,1)',
    },
  },
  {
    id: 'XrbG17JKb7b',
    blockType: 'MuiToolbar',
    label: 'Toolbar',
    acceptDrop: 'box',
  },
  {
    id: 'zrRv6fgaDt6',
    blockType: 'HTMLDivElement',
    label: 'div',
    acceptDrop: 'box',
    blockStyleCss: {
      outline: '2px dashed rgba(41,182,246,1)',
    },
  },
  {
    id: 'AgMJBwcDAg4',
    blockType: 'MuiContainer',
    label: 'Container',
    acceptDrop: 'box',
    blockStyleCss: {
      outline: '2px dashed rgba(41,182,246,1)',
    },
  },
  {
    id: 'AAYECwcCAwI',
    blockType: 'MuiPaper',
    label: 'Paper',
    acceptDrop: 'box',
  },
  {
    id: 'kGZxwtIaWXu',
    blockType: 'MuiGrid',
    label: 'Grid Container',
    acceptDrop: 'box',
    props: {
      container: true,
    } as GridProps,
    blockStyleCss: {
      outline: '2px dashed rgba(251,192,45,1)',
    },
  },
  // {
  //   id: 'xjqj8St0p8K',
  //   blockType: 'MuiGrid',
  //   label: 'Grid Item 12',
  //   acceptDrop: 'box',
  //   props: {
  //     item: true,
  //     xs: 12,
  //   } as GridProps,
  //   blockStyleCss: {
  //     outline: '2px dashed rgba(102,187,106,1)',
  //   },
  // },
  {
    id: 'RiMRPmzUAqC',
    blockType: 'MuiGrid',
    label: 'Grid Item 6',
    acceptDrop: 'box',
    props: {
      item: true,
      xs: 6,
    } as GridProps,
    blockStyleCss: {
      outline: '2px dashed rgba(102,187,106,1)',
    },
  },
  {
    id: 'ERq15a3VdiA',
    blockType: 'MuiGrid',
    label: 'Grid Item 4',
    acceptDrop: 'box',
    props: {
      item: true,
      xs: 4,
    } as GridProps,
    blockStyleCss: {
      outline: '2px dashed rgba(102,187,106,1)',
    },
  },
  {
    id: 'gsYYAoJBMQv',
    blockType: 'MuiGrid',
    label: 'Grid Item 2',
    acceptDrop: 'box',
    props: {
      item: true,
      xs: 2,
    } as GridProps,
    blockStyleCss: {
      outline: '2px dashed rgba(102,187,106,1)',
    },
  },
].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[];
