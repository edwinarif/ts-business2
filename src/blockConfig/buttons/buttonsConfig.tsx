import { ButtonProps } from '@material-ui/core/Button';
import { StaticBlockVariant } from '../componentsMapper';
import { GeneralComponentType } from 'types';
import { getGroupedButtons } from './getGroupedButtons';

export const buttonsConfig: StaticBlockVariant[] = [
  {
    containedButtons: {
      label: 'Contained Buttons',
      blocks: [
        {
          id: 'BAcMDAcGCw8',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            children: 'Default',
          } as ButtonProps,
        },
        {
          id: 'GPT3SWnsvTu',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'primary',
            children: 'Primary',
          } as ButtonProps,
        },
        {
          id: 'VvGsqzpC4Sq',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'secondary',
            children: 'Secondary',
          } as ButtonProps,
        },
        {
          id: 'dg5j9QIWvx6',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'secondary',
            disabled: true,
            children: 'Disabled',
          } as ButtonProps,
        },
        {
          id: 'CN1juPVG5sN',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            href: '#contained-buttons',
            children: 'Link',
          } as ButtonProps,
        },
        {
          id: 'I7CS1ahCGFH',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'primary',
            disableElevation: true,
            children: 'Disable elevation',
          } as ButtonProps,
        },
      ].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[],
    },
    textButtons: {
      label: 'Text Buttons',
      blocks: [
        {
          id: 'OjRQcT6PRfk',
          blockType: 'MuiButton',
          props: {} as ButtonProps,
          isExcludeAddItem: true,
        },
        {
          id: 'oZXhiCgVzZf',
          blockType: 'MuiButton',
          props: {
            children: 'Default',
          } as ButtonProps,
        },
        {
          id: '4ZtnJr7WFug',
          blockType: 'MuiButton',
          props: {
            color: 'primary',
            children: 'Primary',
          } as ButtonProps,
        },
        {
          id: 'yKXjtsE3Sy5',
          blockType: 'MuiButton',
          props: {
            color: 'secondary',
            children: 'Secondary',
          } as ButtonProps,
        },
        {
          id: 'GSSxBCpSQKs',
          blockType: 'MuiButton',
          props: {
            disabled: true,
            children: 'Disabled',
          } as ButtonProps,
        },
        {
          id: 'tvcSrbwJxle',
          blockType: 'MuiButton',
          props: {
            href: '#text-buttons',
            children: 'Link',
          } as ButtonProps,
        },
      ].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[],
    },
    outlinedButtons: {
      label: 'Outlined Buttons',
      blocks: [
        {
          id: 'ksUh9566ZB0',
          blockType: 'MuiButton',
          props: {
            variant: 'outlined',
            children: 'Default',
          } as ButtonProps,
        },
        {
          id: 'BetOsQIupVl',
          blockType: 'MuiButton',
          props: {
            variant: 'outlined',
            color: 'primary',
            children: 'Primary',
          } as ButtonProps,
        },
        {
          id: 'hCVkThOQ5TC',
          blockType: 'MuiButton',
          props: {
            variant: 'outlined',
            color: 'secondary',
            children: 'Secondary',
          } as ButtonProps,
        },
        {
          id: '3OyBRTIDdXU',
          blockType: 'MuiButton',
          props: {
            variant: 'outlined',
            disabled: true,
            children: 'Disabled',
          } as ButtonProps,
        },
        {
          id: '9Ldql9l8xXJ',
          blockType: 'MuiButton',
          props: {
            variant: 'outlined',
            href: '#outlined-buttons',
            children: 'Link',
          } as ButtonProps,
        },
        {
          id: 'xBGeYuU2GwI',
          blockType: 'MuiButton',
          props: {
            variant: 'outlined',
            color: 'inherit',
            children: 'Inherit',
          } as ButtonProps,
        },
      ].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[],
    },
    groupedButtons: {
      label: 'Grouped Buttons',
      blocks: [
        {
          id: 'gJxzUHAPZBN',
          blockType: 'MuiButtonGroup',
          acceptDrop: 'box',
          props: {},
          ...getGroupedButtons([
            { id: 'nwHF1mHazqQ', children: 'One' },
            { id: 'utlENA9ceTj', children: 'Two' },
            { id: 'vnAefZsrwBe', children: 'Three' },
          ]),
        },
        {
          id: 'wOwIpY3QkWV',
          blockType: 'MuiButtonGroup',
          acceptDrop: 'box',
          props: {
            variant: 'contained',
            color: 'primary',
          },
          ...getGroupedButtons([
            { id: 'fW7cz509giF', children: 'One' },
            { id: 'zRKtJHrkCTD', children: 'Two' },
            { id: 'imU8759xKLc', children: 'Three' },
          ]),
        },
        {
          id: 'TPp0E0kZdTn',
          blockType: 'MuiButtonGroup',
          acceptDrop: 'box',
          props: {
            variant: 'text',
            color: 'secondary',
          },
          ...getGroupedButtons([
            { id: 'RsJ62uH5EKC', children: 'One' },
            { id: 'Mj7w6YiXvYN', children: 'Two' },
            { id: 'ZQqPuau7HcV', children: 'Three' },
          ]),
        },
        {
          id: 'oqeTSkiKK42',
          blockType: 'MuiButtonGroup',
          acceptDrop: 'box',
          props: {
            fullWidth: true,
          },
          ...getGroupedButtons([
            { id: 'b7gGMBctH41', children: 'Full' },
            { id: 'wjofcIWMHar', children: 'Width' },
            { id: 'ahlQW6Lgloz', children: 'Group' },
          ]),
        },
        {
          id: 'yiL7b5o1ft0',
          blockType: 'MuiButtonGroup',
          acceptDrop: 'box',
          props: {
            color: 'primary',
            orientation: 'vertical',
          },
          ...getGroupedButtons([
            { id: 'JVEdikXXFSr', children: 'One' },
            { id: 'AbtQCCsgcnF', children: 'Two' },
            { id: 'EZsp8HruTCw', children: 'Three' },
          ]),
        },
        {
          id: 'qBr9ap3tzpa',
          blockType: 'MuiButtonGroup',
          acceptDrop: 'box',
          props: {
            variant: 'contained',
            color: 'primary',
          },
          componentIds: ['FBg234OthEZ', 'mrBoBodVcLq'],
          components: {
            FBg234OthEZ: {
              id: 'FBg234OthEZ',
              blockId: 'OjRQcT6PRfk',
              blockType: 'MuiButton',
              props: {
                children: 'Squash and merge',
              },
            },
            mrBoBodVcLq: {
              id: 'mrBoBodVcLq',
              blockId: 'OjRQcT6PRfk',
              blockType: 'MuiButton',
              props: {
                size: 'small',
              },
              componentIds: ['SMMRXdklYPL'],
              components: {
                SMMRXdklYPL: {
                  id: 'SMMRXdklYPL',
                  blockId: 'FD0DgoqQCgT',
                  blockType: 'MuiIconArrowDropDown',
                },
              },
            },
          },
        },
      ].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[],
    },
    fabButtons: {
      label: 'Floating Action Buttons',
      blocks: [
        {
          id: 'mcbbLDQhgKT',
          blockType: 'MuiFab',
          acceptDrop: 'box',
          props: {
            color: 'primary',
          },
          componentIds: ['MWN3I07TOpn'],
          components: {
            MWN3I07TOpn: {
              id: 'MWN3I07TOpn',
              blockId: '',
              blockType: 'MuiIconAdd',
            },
          },
        },
        {
          id: 'DVYOhBM1l1o',
          blockType: 'MuiFab',
          acceptDrop: 'box',
          props: {
            color: 'secondary',
          },
          componentIds: ['MWN3I07TOpn'],
          components: {
            MWN3I07TOpn: {
              id: 'IKkDheqTzZw',
              blockType: 'MuiIconEdit',
            },
          },
        },
        {
          id: 'EbS8i2zdJcl',
          blockType: 'MuiFab',
          acceptDrop: 'box',
          props: {
            disabled: true,
          },
          componentIds: ['MWN3I07TOpn'],
          components: {
            MWN3I07TOpn: {
              id: 'pFRgXDJXHJR',
              blockType: 'MuiIconFavorite',
            },
          },
        },
        {
          id: 'oQtx32KXa7G',
          blockType: 'MuiFab',
          acceptDrop: 'box',
          props: {
            variant: 'extended',
            children: 'Navigate',
          },
          componentIds: ['MWN3I07TOpn'],
          components: {
            MWN3I07TOpn: {
              id: 'WTKZbBz4L9f',
              blockType: 'MuiIconNavigation',
            },
          },
        },
      ].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[],
    },
    withIconButtons: {
      label: 'With Icons and Label',
      blocks: [
        {
          id: 'kSKvVtSGOcH',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'secondary',
            children: 'Delete',
            startIcon: {
              blockType: 'MuiIconDelete',
            },
          },
        },
        {
          id: 'p6dRvZosrDy',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'primary',
            children: 'Send',
            endIcon: {
              id: 'dL5iuHUGQwx',
              blockType: 'MuiIconSend',
            },
          },
        },
        {
          id: 'j95yxB46O6g',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'default',
            children: 'Upload',
            startIcon: {
              id: 'DH3s2ODghH0',
              blockType: 'MuiIconCloudUpload',
            },
          },
        },
        {
          id: 'veeZf55oR0S',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'secondary',
            disabled: true,
            children: 'Talk',
            startIcon: {
              id: 'hTsF4lij2EZ',
              blockType: 'MuiIconKeyboardVoice',
            },
          },
        },
        {
          id: 'Pd2D4x6TIVH',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'primary',
            children: 'Save',
            size: 'small',
            endIcon: {
              id: 'zA7zOMdcXCP',
              blockType: 'MuiIconSave',
            },
          },
        },
        {
          id: 'dUZUG7IfhiC',
          blockType: 'MuiButton',
          props: {
            variant: 'contained',
            color: 'primary',
            children: 'Save',
            size: 'large',
            endIcon: {
              id: 'IAfT50wUotZ',
              blockType: 'MuiIconSave',
            },
          },
        },
      ].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[],
    },
    iconButtons: {
      label: 'Icon Buttons',
      blocks: [
        {
          id: 'LcltKz8loYE',
          blockType: 'MuiBasicIconButton',
          acceptDrop: 'box',
          props: {},
          componentIds: ['UuqpKEQpzzP'],
          components: {
            UuqpKEQpzzP: {
              id: 'pFRgXDJXHJR',
              blockId: '',
              blockType: 'MuiIconDelete',
            },
          },
        },
        {
          id: 'uLZV0xJBbaq',
          blockType: 'MuiBasicIconButton',
          acceptDrop: 'box',
          props: {
            color: 'primary',
            disabled: true,
          },
          componentIds: ['UuqpKEQpzzP'],
          components: {
            UuqpKEQpzzP: {
              id: 'Kqu3C7oV6qR',
              blockId: '',
              blockType: 'MuiIconDelete',
            },
          },
        },
        {
          id: 'dplNBaw1PIS',
          blockType: 'MuiBasicIconButton',
          acceptDrop: 'box',
          props: {
            color: 'secondary',
          },
          componentIds: ['UuqpKEQpzzP'],
          components: {
            UuqpKEQpzzP: {
              id: 'vqnCeXThyIM',
              blockId: '',
              blockType: 'MuiIconAlarm',
            },
          },
        },
        {
          id: 'hM3yOm2UTIL',
          blockType: 'MuiBasicIconButton',
          acceptDrop: 'box',
          props: {
            color: 'primary',
          },
          componentIds: ['UuqpKEQpzzP'],
          components: {
            UuqpKEQpzzP: {
              id: 'IxnEWQDP1nJ',
              blockId: '',
              blockType: 'MuiIconAddShoppingCart',
            },
          },
        },
      ].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[],
    },
  },
];
