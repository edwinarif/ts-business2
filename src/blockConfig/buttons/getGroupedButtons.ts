import React from 'react';

export const getGroupedButtons = (
  data: {
    id: string;
    children: string | React.ReactElement;
    props?: { [key: string]: any };
  }[]
) => ({
  componentIds: data.map(child => child.id),
  components: data.reduce(
    (res, child) => ({
      ...res,
      [child.id]: {
        id: child.id,
        blockId: 'oZXhiCgVzZf',
        blockType: 'MuiButton',
        props: {
          ...(child.props || {}),
          children: child.children,
        },
      },
    }),
    {}
  ),
});
