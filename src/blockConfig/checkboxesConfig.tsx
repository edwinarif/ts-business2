import { StaticBlockVariant } from './componentsMapper';
import { GeneralComponentType } from 'types';

export const checkboxesConfig: StaticBlockVariant[] = [
  {
    general: {
      label: '',
      blocks: [
        {
          id: 'vfuTx9p8k5Y',
          blockType: 'MuiCheckbox',
          props: {
            checked: true,
            value: 'primary',
          },
        },
        {
          id: 'REpLESzIdKl',
          blockType: 'MuiCheckbox',
          props: {
            defaultChecked: true,
            value: 'secondary',
            color: 'primary',
          },
        },
        {
          id: 'sxFAY85bffS',
          blockType: 'MuiCheckbox',
          props: {
            value: 'uncontrolled',
          },
        },
        {
          id: 'Ko3gcrG88BL',
          blockType: 'MuiCheckbox',
          props: {
            disabled: true,
            value: 'disabled',
          },
        },
        {
          id: 'h64MHqSqpU0',
          blockType: 'MuiCheckbox',
          props: {
            disabled: true,
            checked: true,
            value: 'disabled checked',
          },
        },
        {
          id: 'k6GsMJEEegL',
          blockType: 'MuiCheckbox',
          props: {
            defaultChecked: true,
            indeterminate: true,
            value: 'indeterminate',
          },
        },
        {
          id: 'P6xg5hvOmu8',
          blockType: 'MuiCheckbox',
          props: {
            defaultChecked: true,
            color: 'default',
            value: 'default',
          },
        },
        {
          id: 'mTqnX3t9pxD',
          blockType: 'MuiCheckbox',
          props: {
            defaultChecked: true,
            size: 'small',
            value: 'small',
          },
        },
      ].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[],
    },
  },
  {
    withFormControlLabel: {
      label: 'With FormControlLabel',
      blocks: [
        {
          id: 'eD0VlD7fZlV',
          blockType: 'MuiCheckbox',
          props: {
            defaultChecked: true,
            size: 'small',
            value: 'small',
          },
        },
      ].map(config => ({ ...config, blockId: '' })) as GeneralComponentType[],
    },
  },
];
