import { omit } from 'utils/mixins';
import { isString } from 'util';
import parseProps from 'utils/parseProps';
import componentsMapper, {
  BlockTypes,
  ComponentPropsType,
} from 'blockConfig/componentsMapper';
import generatePropsFromDefault from 'utils/generatePropsFromDefault';
import { GeneralComponentType } from 'types';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';

const getChildrens = (component: GeneralComponentType): any => {
  if (!getIsBlockComponent(component))
    return {
      componentType: component.componentName,
      componentName: component.componentName,
      content: null,
      isCustomComponent: true,
    };

  const componentConfig = componentsMapper[component.blockType as BlockTypes];
  const { componentName } = componentConfig;
  const componentProps = generatePropsFromDefault(
    component.blockType,
    component.props
  ) as ComponentPropsType;

  const baseProps = componentProps
    ? parseProps(componentProps, componentConfig)
    : '';
  const classProps = component.objCss
    ? `${baseProps} className={classes.${component.className}}`
    : baseProps;
  const props = classProps === '' ? undefined : classProps;

  const base = {
    componentType: component.blockType,
    componentName,
    content:
      componentProps &&
      (componentProps as any).children &&
      isString((componentProps as any).children)
        ? (componentProps as any).children
        : null,
    childrens:
      component.componentIds && component.componentIds.length > 0
        ? component.componentIds.map(id =>
            getChildrens(component.components![id])
          )
        : [],
    props,
  };
  return base.childrens.length === 0 ? omit(base, ['childrens']) : base;
};

export default getChildrens;
