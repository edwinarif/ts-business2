import { GeneralComponentType } from 'types';
import isString from 'lodash/isString';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';

export const getMuiStyles: (components: {
  [key: string]: GeneralComponentType;
}) => string = components => {
  return Object.keys(components).reduce((prev, key) => {
    const component = components[key];
    if (!getIsBlockComponent(component)) return prev;

    if (!component.objCss) {
      if (!component.components) return prev;
      return `${prev}${getMuiStyles(component.components)}`;
    }

    const baseRes = `${prev}${component.className}: {
        ${Object.keys(component.objCss).reduce((prevObj, keyObj) => {
          return `${prevObj}
            ${keyObj}: ${
            isString(component.objCss![keyObj])
              ? `"${component.objCss![keyObj].replace(/^"+|"+$/g, '')}"`
              : component.objCss![keyObj]
          },`;
        }, '')}
      },`;
    if (!component.components) return baseRes;
    return `${baseRes}${getMuiStyles(component.components)}`;
  }, '');
};
