import componentsMapper from 'blockConfig/componentsMapper';

const getAllComponentTypeUnique = (
  res: { [key: string]: any },
  childrens: Array<any>
): { [key: string]: any } => ({
  ...res,
  ...childrens.reduce((prev, curr) => {
    if (curr.childrens) {
      return {
        ...prev,
        [curr.componentType]: {
          isCustomComponent: curr.isCustomComponent,
        },
        ...getAllComponentTypeUnique(res, curr.childrens),
      };
    }

    return {
      ...prev,
      [curr.componentType]: {
        isCustomComponent: curr.isCustomComponent,
      },
    };
  }, {} as { [key: string]: any }),
});

const getImports = (childrens: Array<any>) => {
  const componentTypes = getAllComponentTypeUnique({}, childrens);
  return Object.keys(componentTypes).reduce((prev, curr) => {
    const component = componentTypes[curr];
    const importStatement = component.isCustomComponent
      ? `import ${curr} from 'components/${curr}';`
      : (componentsMapper as any)[curr].importStatement;
    return `${prev}
      ${importStatement}`;
  }, '');
};

export default getImports;
