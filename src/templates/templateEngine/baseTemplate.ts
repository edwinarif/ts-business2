export default (componentIds: string[]) => `
import React from 'react'
{{#if muiStyles}}import { createStyles, makeStyles } from '@material-ui/core/styles';{{/if}}{{{imports}}}

function {{componentName}}(props) {
  {{#if muiStyles}}
  const classes = useStyles();
  {{/if}}

  return (
    ${componentIds.length > 1 ? '<>' : ''}
    {{#if childrens}}
    {{> partialChildrens}}
    {{else}}
    null
    {{/if}}
    ${componentIds.length > 1 ? '</>' : ''}
  )
}

{{#if muiStyles}}
const useStyles = makeStyles(() =>
  createStyles({
    {{{muiStyles}}}
  })
);
{{/if}}

export default {{componentName}}

`;
