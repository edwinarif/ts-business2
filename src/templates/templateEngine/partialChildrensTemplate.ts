export default `
{{#each childrens}}
<{{{componentName}}} {{{props}}}>
  {{content}}
  {{#if childrens}}
    {{> partialChildrens}}
  {{/if}}
</{{{componentName}}}>
{{/each}}
`;
