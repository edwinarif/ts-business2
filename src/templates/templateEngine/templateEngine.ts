import Handlebars from 'handlebars';
import prettier from 'prettier/standalone';
import parserBabel from 'prettier/parser-babylon';
import getChildrens from './getChildrens';
import baseTemplate from './baseTemplate';
import partialChildrensTemplate from './partialChildrensTemplate';
import getImports from './getImports';
import { GeneralComponentType } from 'types';
import { getMuiStyles } from './getMuiStyles';

export default (
  componentIds: string[],
  components: { [id: string]: GeneralComponentType }
) => {
  const childrens = componentIds
    .map(id => getChildrens(components[id]))
    .filter(children => children !== undefined);
  const imports = getImports(childrens);
  const muiStyles = getMuiStyles(components);
  const template = Handlebars.compile(baseTemplate(componentIds));
  const partialChildrens = Handlebars.compile(partialChildrensTemplate);
  Handlebars.registerPartial('partialChildrens', partialChildrens);
  const context = {
    componentName: 'MyComponent',
    childrens,
    imports,
    muiStyles,
  };
  const baseCode = template(context);
  const code = prettier.format(baseCode, {
    parser: 'babel',
    plugins: [parserBabel],
  });

  return code;
};
