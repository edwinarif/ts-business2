import { BlockCategories } from 'blockConfig/componentsMapper';
import { CSSProperties } from '@material-ui/styles';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import {
  ComponentPropsType,
  BlockTypes,
} from 'blockConfig/componentsMapperConfig';

export interface IThemeFirebase {
  name: string;
  config: ThemeOptions;
  ownerId: string;
  createdAt?: string | number | Date;
  updatedAt?: string | number | Date;
}

export type CustomComponentType = {
  id: string;
  componentName: string;
  label?: string;
  customComponentId?: string;
} & NestedComponentType;

export type BlockComponentType = {
  id: string;
  blockType: BlockTypes;
  blockId: string;
  className?: string;
  label?: string;
  category?: BlockCategories;
  subCategory?: string;
  props?: ComponentPropsType;
  strCss?: string;
  objCss?: { [key: string]: any };
  acceptDrop?: string;
  blockStyleCss?: CSSProperties;
  customHtmlBlock?: string;
  isParentRender?: boolean;
  isRenderByParent?: boolean;
  isExcludeAddItem?: boolean;
} & NestedComponentType;

export type GeneralComponentType = BlockComponentType | CustomComponentType;

export type NestedComponentType = {
  components?: { [key: string]: GeneralComponentType };
  componentIds?: string[];
};

export type RenderedComponentType = {
  baseHtml: string;
  baseCss: string;
  isSelected?: boolean;
  isDragging?: boolean;
} & GeneralComponentType;

export interface IPage {
  name: string;
  themeId: string;
  blockIds?: {
    [key: string]: {
      baseBlockId: string;
    };
  };
  rootComponent?: NestedComponentType;
  isAsComponent?: boolean;
  createdAt?: string | number | Date;
  updatedAt?: string | number | Date;
}

export interface IProject {
  name: string;
  defaultPageId?: string;
  pages?: {
    [key: string]: IPage;
  };
  customComponents?: {
    [key: string]: CustomComponentType;
  };
  ownerId: string;
  defaultThemeId?: string;
  createdAt?: string | number | Date;
  updatedAt?: string | number | Date;
}

export type FirebaseUser = firebase.User & {
  isEmpty: boolean;
  isLoaded: boolean;
};

export interface FirebaseState {
  isInitializing?: boolean;
  auth: FirebaseUser;
  data: {
    users?: any;
    projects?: {
      [key: string]: IProject;
    };
    blocks?: {
      [key: string]: GeneralComponentType;
    };
    themes?: {
      [key: string]: IThemeFirebase;
    };
  };
}

export interface DragItem {
  index: number;
  id: string;
  type: string;
}
