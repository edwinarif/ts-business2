import { findComponentById } from './findCollectionById';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { getIsBlockComponent } from './getIsBlockComponent';

export const getIsChildComponent = (localReducer: LocalReducerType) => {
  const { page, dragComponentId, dragHoverComponentId } = localReducer;

  if (
    !page ||
    !page.rootComponent ||
    !page.rootComponent.components ||
    !dragComponentId ||
    !dragHoverComponentId
  )
    return false;

  const currentComponent = findComponentById(
    page.rootComponent.components,
    dragComponentId
  );
  if (
    currentComponent &&
    getIsBlockComponent(currentComponent) &&
    currentComponent.components
  ) {
    const hoverComponent = findComponentById(
      currentComponent.components,
      dragHoverComponentId
    );
    if (hoverComponent) return true;
  }

  return false;
};
