import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { ServerStyleSheets, ThemeProvider } from '@material-ui/core/styles';
import { createMuiTheme } from '@material-ui/core';
import {
  CustomComponentType,
  RenderedComponentType,
  NestedComponentType,
} from 'types';
import { getIsBlockComponent } from './getIsBlockComponent';
import isEmpty from 'lodash/isEmpty';
import componentsMapper, {
  ComponentPropsType,
} from 'blockConfig/componentsMapper';
import generatePropsFromDefault from './generatePropsFromDefault';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import Icon from '@material-ui/core/Icon';
import decamelize from 'decamelize';

const sheets = new ServerStyleSheets();

const getCustomComponent = (
  customComponent: NestedComponentType,
  jsxEl: JSX.Element,
  customComponents?: { [key: string]: CustomComponentType }
) => {
  const { components, componentIds } = customComponent;

  if (!components || isEmpty(components) || !componentIds) return jsxEl;

  const el = componentIds.reduce((res, componentId) => {
    const component = components[componentId];
    if (!component) return res;

    const { children, ...rest } = res.props;

    if (getIsBlockComponent(component)) {
      if (component.blockType && component.blockType.includes('MuiIcon')) {
        const iconName = decamelize(component.blockType.replace('MuiIcon', ''));
        return (
          <res.type {...rest}>
            <Icon id={componentId} style={component.objCss || {}}>
              {iconName}
            </Icon>
            {children}
          </res.type>
        );
      }

      const componentMapper = componentsMapper[component.blockType];
      if (!componentMapper) return res;
      const { Component } = componentMapper;
      const customProps = generatePropsFromDefault(component.blockType, {
        ...component.props,
      } as any) as ComponentPropsType;

      return (
        <res.type {...rest}>
          <Component {...customProps}>
            {customProps.children}
            {component.componentIds &&
              getCustomComponent(component, res, customComponents)}
          </Component>
          {children}
        </res.type>
      );
    } else {
      if (!customComponents || !component.customComponentId) return res;
      const childCustomComponent =
        customComponents[component.customComponentId];
      if (!childCustomComponent) return res;
      return (
        <res.type {...rest}>
          {getCustomComponent(childCustomComponent, <></>, customComponents)}
          {children}
        </res.type>
      );
    }
  }, jsxEl);

  return el;
};

type parseCustomComponentProps = {
  customComponent: CustomComponentType;
  themeConfig: ThemeOptions;
  customComponents?: { [key: string]: CustomComponentType };
};

export const parseCustomComponent = ({
  customComponent,
  themeConfig,
  customComponents,
}: parseCustomComponentProps) => {
  const component = getCustomComponent(
    customComponent,
    <></>,
    customComponents
  );
  const outerTheme = createMuiTheme();
  const theme = createMuiTheme(themeConfig);
  const themedComponent = (
    <ThemeProvider theme={outerTheme}>
      <ThemeProvider theme={() => theme}>{component}</ThemeProvider>
    </ThemeProvider>
  );
  const html = ReactDOMServer.renderToStaticMarkup(
    sheets.collect(themedComponent)
  );
  const style = sheets.toString();
  const block: RenderedComponentType = {
    ...customComponent,
    baseHtml: html.replace(new RegExp('Mui', 'g'), 'Re'),
    baseCss: style.replace(new RegExp('Mui', 'g'), 'Re'),
    isDragging: false,
    isSelected: false,
  };

  return block;
};
