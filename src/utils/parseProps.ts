import {
  ComponentPropsType,
  ComponentMapperTyped,
} from 'blockConfig/componentsMapper';
import { omit } from './mixins';

export default (
  baseProps: ComponentPropsType,
  componentConfig: ComponentMapperTyped<ComponentPropsType>
): string => {
  const props = omit(baseProps, ['children']);

  const res = (Object.keys(props) as Array<keyof ComponentPropsType>).reduce(
    (prev, curr) => {
      if (props[curr] === componentConfig.propsConfig![curr]!.default)
        return prev;
      if (typeof props[curr] === 'string') {
        return `${prev} ${curr}="${props[curr]}"`;
      }
      if (typeof props[curr] === 'boolean' && props[curr])
        return `${prev} ${curr}`;
      if (typeof props[curr] === 'boolean' && !props[curr])
        return `${prev} ${curr}={false}`;
      return `${prev} ${curr}={${props[curr]}}`;
    },
    ''
  );

  return res;
};
