import { GeneralComponentType } from 'types';
import { getIsBlockComponent } from './getIsBlockComponent';
import componentsMapper from 'blockConfig/componentsMapper';

export const getTitle = (
  component: GeneralComponentType,
  blocks: { [key: string]: GeneralComponentType },
  isDefaultComponentMapper?: boolean
) => {
  if (!component) return '';

  if (getIsBlockComponent(component)) {
    if (component.label) return component.label;

    const block = blocks[component.blockId];
    if (block && getIsBlockComponent(block) && block.label) {
      return block.label;
    }

    if (isDefaultComponentMapper)
      return componentsMapper[component.blockType].componentName;
  } else {
    if (component.label) return component.label;
    return component.componentName;
  }

  return '';
};
