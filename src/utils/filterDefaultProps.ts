import componentsMapper, {
  ComponentPropsType,
  BlockTypes,
} from 'blockConfig/componentsMapper';

export default (componentName: BlockTypes, formValues: ComponentPropsType) =>
  (Object.keys(formValues) as Array<keyof ComponentPropsType>).reduce(
    (prev, curr) => {
      if (
        componentsMapper[componentName].propsConfig &&
        formValues[curr] ===
          (componentsMapper[componentName].propsConfig![curr]! as any).default
      )
        return prev;

      return {
        ...prev,
        [curr]: formValues[curr],
      };
    },
    {} as any
  );
