import { GeneralComponentType } from 'types';
import { getIsBlockComponent } from './getIsBlockComponent';

export const findComponentById = (
  collection: { [key: string]: GeneralComponentType },
  id: string
): GeneralComponentType | undefined => {
  if (!collection || collection === {} || !id) return undefined;

  return Object.keys(collection).reduce((prev, curr) => {
    if (prev) return prev;

    const component = collection[curr];

    // TODO: Check this one, feel wrong
    if (!getIsBlockComponent(component)) return prev;

    if (curr === id) return component;

    if (component.components) {
      return findComponentById(component.components, id);
    }

    return undefined;
  }, undefined as GeneralComponentType | undefined);
};
