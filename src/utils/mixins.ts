export function move<T>(array: T[], fromIndex: number, new_index: number): T[] {
  const toIndex =
    new_index < 0
      ? 0
      : new_index > array.length - 1
      ? array.length - 1
      : new_index;

  if (fromIndex === toIndex) return array;

  var copy = Object.assign([], array) as (T | undefined)[];
  if (toIndex >= copy.length) {
    var k = toIndex - copy.length;
    while (k-- + 1) {
      copy.push(undefined);
    }
  }
  copy.splice(toIndex, 0, copy.splice(fromIndex, 1)[0]);
  return copy as T[];
}

export function insert<T>(arr: T[], index: number, newItem: T) {
  if (!arr || arr.length === 0) return [newItem];
  return [...arr.slice(0, index), newItem, ...arr.slice(index)];
}

export function omit(obj: { [key: string]: any }, keys: Array<string>) {
  return keys.reduce(
    (r, key) => {
      delete r[key];
      return r;
    },
    { ...obj }
  );
}

export const omitUndefined = (list: { [key: string]: any }) => {
  const omitKey = (Object.keys(list) as string[]).reduce(
    (prev, key) => {
      if (list[key] === undefined) return [...prev, key];
      return prev;
    },
    [] as string[]
  );

  return omit(list, omitKey);
};

export const shallowCompare = (
  obj1: { [key: string]: any },
  obj2: { [key: string]: any }
) =>
  Object.keys(obj1).length === Object.keys(obj2).length &&
  Object.keys(obj1).every(
    key => obj2.hasOwnProperty(key) && obj1[key] === obj2[key]
  );

export const stringParser = (str: string) => {
  if (str === 'true') return true;
  if (str === 'false') return false;

  const intParsed = parseInt(str);
  if (!isNaN(intParsed)) return intParsed;

  const floatParsed = parseFloat(str);
  if (!isNaN(floatParsed)) return floatParsed;

  return str.replace(/^"+|"+$/g, '');
};
