import { BlockComponentType, GeneralComponentType } from 'types';
import { getIsBlockComponent } from './getIsBlockComponent';

export const staticBlockToComponent: (
  block: GeneralComponentType | undefined
) => BlockComponentType | undefined = (
  block: GeneralComponentType | undefined
) => {
  if (!block || !block.id || !getIsBlockComponent(block)) return undefined;

  const res: BlockComponentType = {
    id: block.id,
    blockId: block.id,
    label: block.label || block.blockType,
    className: '',
    ...block,
  };

  return res;
};
