import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { ServerStyleSheets, ThemeProvider } from '@material-ui/core/styles';
import componentsMapper, {
  ComponentPropsType,
  ComponentMapperTyped,
} from 'blockConfig/componentsMapper';
import { createMuiTheme } from '@material-ui/core';
import {
  GeneralComponentType,
  RenderedComponentType,
  BlockComponentType,
} from 'types';
import generatePropsFromDefault from './generatePropsFromDefault';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import Icon from '@material-ui/core/Icon';
import { getIsBlockComponent } from './getIsBlockComponent';
import decamelize from 'decamelize';

const sheets = new ServerStyleSheets();

const getNestedComponent = (
  blockFirebase: BlockComponentType,
  componentMapper: ComponentMapperTyped<ComponentPropsType>,
  componentProps?: ComponentPropsType
) => {
  const { Component }: { Component: any } = componentMapper;
  const { props: customProps, nodeProps } = generatePropsFromDefault(
    blockFirebase.blockType,
    {
      ...blockFirebase.props,
      ...(componentProps || {}),
    } as any,
    true
  ) as { props: ComponentPropsType; nodeProps: any };

  console.log(customProps, nodeProps);
  const { components, componentIds } = blockFirebase;

  const componentNodeProps = Object.keys(nodeProps).reduce(
    (prev, nodePropsKey) => {
      console.log((nodeProps as any)[nodePropsKey]);
      const nodeComponent = getNestedComponent(
        (nodeProps as any)[nodePropsKey],
        componentMapper
      );
      return {
        ...prev,
        [nodePropsKey]: nodeComponent,
      };
    },
    {}
  );

  if (!componentIds || !components) {
    if (blockFirebase.blockType.includes('MuiIcon')) {
      const iconName = decamelize(
        blockFirebase.blockType.replace('MuiIcon', '')
      );
      console.log(blockFirebase, 'sini', iconName, <Icon>{iconName}</Icon>);
      return <Icon>{iconName}</Icon>;
    }

    return (
      <Component
        {...customProps}
        {...componentNodeProps}
        id={blockFirebase.id}
      />
    );
  }

  return componentIds.reduce((res, componentId) => {
    const component = {
      ...components[componentId],
      id: componentId,
    };
    if (!component || !getIsBlockComponent(component) || !component.blockType)
      return res;

    const { children, ...rest } = res.props;
    const childComponentMapper = componentsMapper[component.blockType];

    if (component.blockType.includes('MuiIcon')) {
      const iconName = decamelize(component.blockType.replace('MuiIcon', ''));
      return (
        <res.type {...rest} id={blockFirebase.id}>
          <Icon id={componentId} style={component.objCss || {}}>
            {iconName}
          </Icon>
          {children || null}
        </res.type>
      );
    }

    return children ? (
      <res.type {...rest} id={blockFirebase.id}>
        {getNestedComponent(component, childComponentMapper)}
        {children}
      </res.type>
    ) : (
      <res.type {...rest} id={blockFirebase.id}>
        {getNestedComponent(component, childComponentMapper)}
      </res.type>
    );
  }, (<Component {...customProps} />) as JSX.Element);
};

export const parseBlockComponent = (
  blockFirebase: GeneralComponentType,
  themeConfig: ThemeOptions,
  componentId: string = '',
  componentProps?: ComponentPropsType
) => {
  if (!blockFirebase || !getIsBlockComponent(blockFirebase)) return;

  const componentMapper = componentsMapper[blockFirebase.blockType];
  if (!componentMapper) return undefined;

  const outerTheme = createMuiTheme();
  const theme = createMuiTheme(themeConfig);
  const component = getNestedComponent(
    blockFirebase,
    componentMapper,
    componentProps
  );
  console.log(component);
  const themedComponent = (
    <ThemeProvider theme={outerTheme}>
      <ThemeProvider theme={() => theme}>{component}</ThemeProvider>
    </ThemeProvider>
  );
  const html = ReactDOMServer.renderToStaticMarkup(
    sheets.collect(themedComponent)
  );
  const style = sheets.toString();
  const block: RenderedComponentType = {
    ...blockFirebase,
    baseHtml: html.replace(new RegExp('Mui', 'g'), 'Re'),
    baseCss: style.replace(new RegExp('Mui', 'g'), 'Re'),
    isDragging: false,
    isSelected: false,
  };

  if (componentId !== '') {
    const res = { ...block, id: componentId };
    return res;
  }

  return block;
};
