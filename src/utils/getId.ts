import shortid from 'shortid';

export const getId: () => string = () => {
  shortid.characters(
    '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_$'
  );
  const newGeneratedId = shortid.generate();
  return newGeneratedId;
};
