import { omit } from './mixins';

export default (state: any, action: any) => {
  switch (action.type) {
    case 'set':
      return {
        ...state,
        [action.key]: action.value,
      };
    case 'add':
      return {
        ...state,
        [action.key]: [...state[action.key], action.value],
      };
    case 'remove':
      return omit(state, [action.key]);
    case `reset`:
      return action.value;
    default:
      return state;
  }
};
