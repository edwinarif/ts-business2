import { RenderedComponentType } from 'types';

export const getParentComponent = (
  currentPath: string,
  renderedNestedComponents: { [key: string]: RenderedComponentType }
): RenderedComponentType | undefined => {
  const pathSplit = currentPath.split('.');
  if (pathSplit[pathSplit.length - 2] === 'components') {
    const parentId = pathSplit[pathSplit.length - 1];
    const parentComponent = renderedNestedComponents[parentId];

    if (parentComponent) return parentComponent;

    return getParentComponent(
      currentPath.replace(`.components.${parentId}`, ''),
      renderedNestedComponents
    );
  }

  return undefined;
};
