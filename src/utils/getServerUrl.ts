export default () =>
  (process.env.NODE_ENV !== 'development'
    ? process.env.REACT_APP_API_URL_PROD
    : process.env.REACT_APP_API_URL_DEV) || 'http://localhost:4000';
