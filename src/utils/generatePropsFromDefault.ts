import componentsMapper, {
  BlockTypes,
  ComponentPropsType,
} from 'blockConfig/componentsMapper';
import isObject from 'lodash/isObject';

export default (
  componentType: BlockTypes,
  customProps?: ComponentPropsType,
  isIncludeNode?: Boolean
): ComponentPropsType | { props: ComponentPropsType; nodeProps: any } => {
  const res = {
    ...(Object.keys(componentsMapper[componentType].propsConfig || {}) as Array<
      keyof ComponentPropsType
    >).reduce((prev, current) => {
      if (customProps && customProps[current] !== undefined) {
        if (!isIncludeNode && (customProps[current]! as any).blockType)
          return prev;

        return { ...prev, [current]: customProps[current] };
      }

      const propsConfig = componentsMapper[componentType].propsConfig!;
      if ((propsConfig[current] && (propsConfig[current]! as any)).value)
        return {
          ...prev,
          [current]: (propsConfig[current]! as any).value,
        };

      return prev;
    }, {}),
  };

  if (!isIncludeNode) return res;

  console.log(res);

  const test = Object.keys(res).reduce(
    (prev, propsKey) => {
      // console.log(propsKey, (res as any)[propsKey]);

      if (propsKey !== 'children' && isObject((res as any)[propsKey])) {
        if ((res as any)[propsKey].blockType !== undefined)
          return {
            props: prev.props,
            nodeProps: {
              ...prev.nodeProps,
              [propsKey]: (res as any)[propsKey],
            },
          };

        return {
          props: prev.props,
          nodeProps: {
            ...prev.nodeProps,
            [propsKey]: Object.keys((res as any)[propsKey]).reduce(
              (prevNodeProps, id) => {
                return {
                  ...prevNodeProps,
                  ...((res as any)[propsKey][id] as any),
                };
              },
              {}
            ),
          },
        };
      }

      // WIP - Object as child

      // if (isObject((res as any)[propsKey]))
      //   return {
      //     props: prev.props,
      //     nodeProps: {
      //       ...prev.nodeProps,
      //       [propsKey]: Object.keys((res as any)[propsKey]).reduce(
      //         (prevNode, nodeId, idx) => {
      //           if (idx === 0) {
      //             const nodePropVal = ((res as any)[propsKey] as any)[nodeId];
      //             console.log(nodePropVal, prevNode);
      //             return {
      //               ...prevNode,
      //             };
      //           }

      //           return prevNode;
      //         },
      //         {}
      //       ),
      //     },
      //   };

      return {
        props: {
          ...prev.props,
          [propsKey]: (res as any)[propsKey],
        },
        nodeProps: prev.nodeProps,
      };
    },
    { props: {}, nodeProps: {} }
  );

  // console.log(test);
  return test;
};
