import { CustomComponentType, GeneralComponentType } from 'types';

export function getIsCustomComponent(
  component: GeneralComponentType
): component is CustomComponentType {
  return (
    (component as any).customComponentId !== undefined ||
    (component as any).componentName !== undefined
  );
}
