import { BlockComponentType, GeneralComponentType } from 'types';

export function getIsBlockComponent(
  component: GeneralComponentType
): component is BlockComponentType {
  return (
    (component as any).componentName === undefined &&
    (component as any).customComponentId === undefined
  );
}
