import {
  NavActionTypes,
  SET_KEY_VALUE,
  ADD_LOADING_BLOCK_ID,
  DELETE_LOADING_BLOCK_ID,
  ADD_COMPONENT,
  SET_MULTI_KEY_VALUE,
  ADD_BLOCK,
  SET_COMPONENT,
  STOP_DRAGGING,
  START_DRAGGING,
  SET_DROP_CHILD_HOVER,
  SET_MERGE,
  SET_MAIN_NAV,
  mainNavCardKeys,
  CLOSE_MAIN_NAV,
} from 'redux/actions/localActions';
import { RenderedComponentType, IProject, IThemeFirebase, IPage } from 'types';

export type LocalReducerType = {
  propsManagerTabIdx: number;
  isAddItemOpen: boolean;
  isAddBlockItemOpen: boolean;
  isAddPageOpen: boolean;
  isSelectThemeOpen: boolean;
  isBlockDragging: boolean;
  isPropsManagerOpen: boolean;
  isLayersCardOpen: boolean;
  currentProjectId: string;
  currentPageId: string;
  currentSelectedComponent: string;
  currentSelectedComponentPath: string;
  currentBlockItemCategory?: string;
  loadingBlockIds: string[];
  renderedBlockComponents: { [key: string]: RenderedComponentType };
  renderedCustomComponents: { [key: string]: RenderedComponentType };
  renderedNestedComponents: { [key: string]: RenderedComponentType };
  project?: IProject;
  page?: IPage;
  themes?: { [key: string]: IThemeFirebase };

  // Drag states
  isDragging: boolean;
  dragComponentId?: string;
  dragBlockId?: string;
  dragIndex?: number;
  dragPath?: string;
  dragHoverComponentId?: string;
  dragHoverIndex?: number;
  dragHoverPath?: string;
  dragDirection?: 'up' | 'down';
  dragBound?: ClientRect & DOMRect;
  dragHoverBound?: ClientRect & DOMRect;
  dragHoverIsEditor?: boolean;
};

const initialState: LocalReducerType = {
  propsManagerTabIdx: 0,
  isAddItemOpen: false,
  isAddBlockItemOpen: false,
  isAddPageOpen: false,
  isSelectThemeOpen: false,
  isBlockDragging: false,
  isPropsManagerOpen: true,
  isLayersCardOpen: false,
  currentProjectId: '-Lste14W3BHAOG7uqC_M',
  currentPageId: '-Lste1D8qrzq_PN2Crn1',
  currentSelectedComponent: '',
  currentSelectedComponentPath: 'rootComponent',
  loadingBlockIds: [],
  renderedBlockComponents: {},
  renderedCustomComponents: {},
  renderedNestedComponents: {},
  isDragging: false,
};

export default (
  state = initialState,
  action: NavActionTypes
): LocalReducerType => {
  switch (action.type) {
    case SET_KEY_VALUE:
      return {
        ...state,
        [action.payload.key]: action.payload.value,
      };
    case SET_MULTI_KEY_VALUE:
    case START_DRAGGING:
      return {
        ...state,
        ...action.payload.reduce(
          (prev, current) => ({
            ...prev,
            [current.key]: current.value,
          }),
          {} as any
        ),
      };
    case SET_MERGE:
      return {
        ...state,
        [action.payload.key]: {
          ...(state[action.payload.key] as any),
          ...action.payload.value,
        },
      };
    case SET_MAIN_NAV:
      return {
        ...state,
        ...(Object.keys(
          mainNavCardKeys
        ) as (keyof typeof mainNavCardKeys)[]).reduce((prev, mainNavKey) => {
          if (action.payload.key === mainNavKey) {
            return {
              ...prev,
              [action.payload.key]:
                action.payload.value === undefined
                  ? !state[action.payload.key]
                  : action.payload.value,
            };
          }

          return { ...prev, [mainNavKey]: false };
        }, {} as { [key in keyof typeof mainNavCardKeys]: boolean }),
      };
    case CLOSE_MAIN_NAV:
      return {
        ...state,
        currentBlockItemCategory: undefined,
        ...(Object.keys(
          mainNavCardKeys
        ) as (keyof typeof mainNavCardKeys)[]).reduce((prev, mainNavKey) => {
          return { ...prev, [mainNavKey]: false };
        }, {} as { [key in keyof typeof mainNavCardKeys]: boolean }),
      };
    case STOP_DRAGGING:
      return {
        ...state,
        isDragging: false,
        isBlockDragging: false,
        dragComponentId: undefined,
        dragIndex: undefined,
        dragPath: undefined,
        dragHoverComponentId: undefined,
        dragHoverIndex: undefined,
        dragHoverPath: undefined,
        dragDirection: undefined,
        dragBound: undefined,
        dragHoverBound: undefined,
        dragHoverIsEditor: undefined,
      };
    case SET_DROP_CHILD_HOVER:
      return {
        ...state,
        dragHoverIndex: action.dragHoverIndex,
        dragHoverBound: action.dragHoverBound,
        dragHoverComponentId: action.dragHoverComponentId,
        dragHoverPath: action.dragHoverPath,
        dragDirection: undefined,
        dragHoverIsEditor: undefined,
      };
    case ADD_LOADING_BLOCK_ID:
      return {
        ...state,
        loadingBlockIds: [...state.loadingBlockIds, action.blockId],
      };
    case DELETE_LOADING_BLOCK_ID:
      return {
        ...state,
        loadingBlockIds: state.loadingBlockIds.filter(
          id => id !== action.blockId
        ),
      };
    case ADD_BLOCK:
      return {
        ...state,
        renderedBlockComponents: {
          ...state.renderedBlockComponents,
          [action.block.id]: action.block,
        },
      };
    case ADD_COMPONENT:
      return {
        ...state,
        renderedCustomComponents: {
          ...state.renderedCustomComponents,
          [action.component.id]: action.component,
        },
      };
    case SET_COMPONENT:
      return {
        ...state,
        renderedCustomComponents: {
          ...state.renderedCustomComponents,
          [action.componentId]: {
            ...state.renderedCustomComponents[action.componentId],
            [action.key]: action.value,
          },
        },
      };
    default:
      return state;
  }
};
