import { LocalReducerType } from 'redux/reducers/localReducer';
import { RenderedComponentType, IProject, IPage } from 'types';

export const SET_KEY_VALUE = 'SET_KEY_VALUE';
export const SET_MULTI_KEY_VALUE = 'SET_MULTI_KEY_VALUE';
export const SET_MAIN_NAV = 'SET_MAIN_NAV';
export const CLOSE_MAIN_NAV = 'CLOSE_MAIN_NAV';
export const SET_MERGE = 'SET_MERGE';
export const ADD_LOADING_BLOCK_ID = 'ADD_LOADING_BLOCK_ID';
export const DELETE_LOADING_BLOCK_ID = 'DELETE_LOADING_BLOCK_ID';
export const ADD_BLOCK = 'ADD_BLOCK';
export const ADD_COMPONENT = 'ADD_COMPONENT';
export const SET_COMPONENT = 'SET_COMPONENT';

export const START_DRAGGING = 'START_DRAGGING';
export const STOP_DRAGGING = 'STOP_DRAGGING';

export const SET_DROP_CHILD_HOVER = 'SET_DROP_CHILD_HOVER';

export const mainNavCardKeys = {
  isAddItemOpen: 'isAddItemOpen',
  isAddBlockItemOpen: 'isAddBlockItemOpen',
  isAddPageOpen: 'isAddPageOpen',
  isSelectThemeOpen: 'isSelectThemeOpen',
  isLayersCardOpen: 'isLayersCardOpen',
};

type PayloadType = {
  key: keyof LocalReducerType;
  value: any;
};

export type SetKeyValueAction = {
  type: typeof SET_KEY_VALUE;
  payload: PayloadType;
};

export type SetMultiKeyValueAction = {
  type: typeof SET_MULTI_KEY_VALUE;
  payload: PayloadType[];
};

export type SetMergeAction = {
  type: typeof SET_MERGE;
  payload: PayloadType;
};

export type SetMainNavAction = {
  type: typeof SET_MAIN_NAV;
  payload: PayloadType;
};

export type CloseMainNavAction = {
  type: typeof CLOSE_MAIN_NAV;
};

export type StartDraggingAction = {
  type: typeof START_DRAGGING;
  payload: PayloadType[];
};

export type StopDraggingAction = {
  type: typeof STOP_DRAGGING;
};

export type SetDropChildHoverAction = {
  type: typeof SET_DROP_CHILD_HOVER;
  dragHoverIndex: number;
  dragHoverBound: ClientRect & DOMRect;
  dragHoverComponentId: string;
  dragHoverPath: string;
};

export type AddLoadingBlockIdAction = {
  type: typeof ADD_LOADING_BLOCK_ID;
  blockId: string;
};

export type DeleteLoadingBlockIdAction = {
  type: typeof DELETE_LOADING_BLOCK_ID;
  blockId: string;
};

export type AddBlockAction = {
  type: typeof ADD_BLOCK;
  block: RenderedComponentType;
};

export type AddComponentAction = {
  type: typeof ADD_COMPONENT;
  component: RenderedComponentType;
};

export type SetComponentAction = {
  type: typeof SET_COMPONENT;
  componentId: string;
  key: keyof RenderedComponentType;
  value: any;
};

export type NavActionTypes =
  | SetKeyValueAction
  | SetMultiKeyValueAction
  | SetMergeAction
  | SetMainNavAction
  | CloseMainNavAction
  | AddLoadingBlockIdAction
  | DeleteLoadingBlockIdAction
  | AddBlockAction
  | AddComponentAction
  | SetComponentAction
  | StartDraggingAction
  | StopDraggingAction
  | SetDropChildHoverAction;

export const setKeyValue = (payload: PayloadType): NavActionTypes => ({
  type: SET_KEY_VALUE,
  payload,
});

export const setMultiKeyValue = (payload: PayloadType[]): NavActionTypes => ({
  type: SET_MULTI_KEY_VALUE,
  payload,
});

export const setMerge = (payload: PayloadType): NavActionTypes => ({
  type: SET_MERGE,
  payload,
});

export const setMainNav = (payload: PayloadType): NavActionTypes => ({
  type: SET_MAIN_NAV,
  payload,
});

export const toggleMainNavCard = (
  key: keyof typeof mainNavCardKeys,
  isOpen?: boolean
) => setMainNav({ key, value: isOpen });

export const setIsAddItemOpen = (isOpen: boolean) =>
  toggleMainNavCard('isAddItemOpen', isOpen);

export const setIsAddBlockItemOpen = (isOpen: boolean) =>
  setKeyValue({ key: 'isAddBlockItemOpen', value: isOpen });

export const setIsAddPageOpen = (isOpen: boolean) =>
  toggleMainNavCard('isAddPageOpen', isOpen);

export const setIsSelectThemeOpen = (isOpen: boolean) =>
  toggleMainNavCard('isSelectThemeOpen', isOpen);

export const setIsLayersCardOpen = (isOpen: boolean) =>
  toggleMainNavCard('isLayersCardOpen', isOpen);

export function closeMainNav(): CloseMainNavAction {
  return { type: CLOSE_MAIN_NAV };
}

export const setPropsManagerTabIdx = (idx: number) =>
  setKeyValue({ key: 'propsManagerTabIdx', value: idx });

export const setIsPropsManagerOpen = (isOpen: boolean) =>
  setKeyValue({ key: 'isPropsManagerOpen', value: true });

export const setCurrentProject = (projectId: string, project: IProject) =>
  setMultiKeyValue([
    { key: 'currentProjectId', value: projectId },
    { key: 'project', value: project },
  ]);

export const setCurrentPage = (pageId: string, page: IPage) =>
  setMultiKeyValue([
    { key: 'currentPageId', value: pageId },
    { key: 'page', value: page },
  ]);

export const setCurrentSelectedComponent = (
  componentId: string,
  componentPath: string
) =>
  setMultiKeyValue([
    { key: 'currentSelectedComponent', value: componentId },
    { key: 'currentSelectedComponentPath', value: componentPath },
  ]);

export const setIsBlockDragging = (isDragging: boolean, blockId?: string) =>
  setMultiKeyValue([
    { key: 'isBlockDragging', value: isDragging },
    { key: 'dragBlockId', value: blockId },
  ]);

export function addLoadingBlockId(blockId: string): AddLoadingBlockIdAction {
  return {
    type: ADD_LOADING_BLOCK_ID,
    blockId,
  };
}

export function deleteLoadingBlockId(
  blockId: string
): DeleteLoadingBlockIdAction {
  return {
    type: DELETE_LOADING_BLOCK_ID,
    blockId,
  };
}

export function addBlock(block: RenderedComponentType): AddBlockAction {
  return {
    type: ADD_BLOCK,
    block,
  };
}

export function addComponent(
  component: RenderedComponentType
): AddComponentAction {
  return {
    type: ADD_COMPONENT,
    component,
  };
}

export function startDragging(
  componentId: string,
  dragIndex: number,
  dragBound: object,
  path: string
): StartDraggingAction {
  return {
    type: START_DRAGGING,
    payload: [
      {
        key: 'isDragging',
        value: true,
      },
      {
        key: 'dragComponentId',
        value: componentId,
      },
      {
        key: 'dragIndex',
        value: dragIndex,
      },
      {
        key: 'dragBound',
        value: dragBound,
      },
      {
        key: 'dragPath',
        value: path,
      },
    ],
  };
}

export function stopDragging(): StopDraggingAction {
  return { type: STOP_DRAGGING };
}

export const setDragDirection = (
  dragHoverComponentId: string | undefined,
  dragHoverIndex: number | undefined,
  dragDirection: 'up' | 'down' | undefined,
  dragHoverBound: (ClientRect & DOMRect) | undefined,
  dragHoverIsEditor: boolean | undefined,
  dragHoverPath: string
) =>
  setMultiKeyValue([
    {
      key: 'dragHoverComponentId',
      value: dragHoverComponentId,
    },
    {
      key: 'dragHoverIndex',
      value: dragHoverIndex,
    },
    {
      key: 'dragDirection',
      value: dragDirection,
    },
    {
      key: 'dragHoverBound',
      value: dragHoverBound,
    },
    {
      key: 'dragHoverIsEditor',
      value: dragHoverIsEditor,
    },
    {
      key: 'dragHoverPath',
      value: dragHoverPath,
    },
  ]);

export function setDropChildHover(
  dragHoverIndex: number,
  dragHoverBound: ClientRect & DOMRect,
  dragHoverComponentId: string,
  dragHoverPath: string
): SetDropChildHoverAction {
  return {
    type: SET_DROP_CHILD_HOVER,
    dragHoverIndex,
    dragHoverBound,
    dragHoverComponentId,
    dragHoverPath,
  };
}

export function setComponent(
  componentId: string,
  key: keyof RenderedComponentType,
  value: any
): SetComponentAction {
  return {
    type: SET_COMPONENT,
    componentId,
    key,
    value,
  };
}
