import * as React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { RouteChildrenProps } from 'react-router';

const RouterContext = React.createContext<RouteChildrenProps<any> | null>(null);

export const HookedBrowserRouter = (props: any) => (
  <BrowserRouter>
    <Route>
      {(routeProps: any) => (
        <RouterContext.Provider value={routeProps}>
          {props.children}
        </RouterContext.Provider>
      )}
    </Route>
  </BrowserRouter>
);

export function useRouter() {
  return React.useContext(RouterContext);
}
