import React, { useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const FileNameTabs = () => {
  const classes = useStyles();
  const [value, setValue] = useState(0);

  return (
    <>
      <Tabs
        value={value}
        onChange={(e, newValue) => setValue(newValue)}
        classes={{
          root: classes.root,
          indicator: classes.rootIndicator,
        }}
        aria-label='simple tabs example'
      >
        <Tab
          label='Home.tsx'
          disableRipple
          classes={{
            root: classes.tabRoot,
            selected: classes.tabSelected,
          }}
        />
        {/* <Tab
          label='About.tsx'
          disableRipple
          classes={{
            root: classes.tabRoot,
            selected: classes.tabSelected,
          }}
        />
        <Tab
          label='HowItWork.tsx'
          disableRipple
          classes={{
            root: classes.tabRoot,
            selected: classes.tabSelected,
          }}
        /> */}
      </Tabs>
    </>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      minHeight: 0,
      backgroundColor: '#eeeeee',
    },
    rootIndicator: {
      height: 0,
    },
    tabRoot: {
      minHeight: 0,
      backgroundColor: '#e0e0e0',
      textTransform: 'unset',
      marginRight: 1,
    },
    tabSelected: {
      backgroundColor: '#fff',
    },
  })
);

export default FileNameTabs;
