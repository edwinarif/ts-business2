import { LocalReducerType } from 'redux/reducers/localReducer';
import { DropTargetMonitor } from 'react-dnd';
import { setDragDirection } from 'redux/actions/localActions';
import at from 'lodash/at';

type Nearest =
  | {
      id: string;
      boundKey: string;
      dest: number;
      bound: ClientRect & DOMRect;
    }
  | undefined;

export const handleDropHoverBase = (
  dispatch: any,
  localReducer: LocalReducerType,
  parentComponentId: string,
  path: string
) => (item: any, monitor: DropTargetMonitor) => {
  const isOverCurrent = monitor.isOver({ shallow: true });
  if (!isOverCurrent) return;

  if (
    (monitor.getDifferenceFromInitialOffset() as any).x === 0 &&
    (monitor.getDifferenceFromInitialOffset() as any).y === 0
  )
    return;

  const {
    project,
    page,
    dragHoverPath,
    dragHoverIndex,
    dragDirection,
  } = localReducer;

  if (!project) return;

  if (dragHoverPath && dragHoverPath !== path) {
    dispatch(
      setDragDirection(
        undefined,
        undefined,
        undefined,
        undefined,
        path === 'rootComponent' ? true : false,
        ''
      )
    );
    return;
  }

  if (!page) return;

  const components = at<any>(page, `${path}.components`)[0];
  const componentIds = at<any>(page, `${path}.componentIds`)[0];
  if (!components || !componentIds) return;

  const clientOffset = monitor.getClientOffset()!;

  // Check last component first
  const lastComponentId = componentIds[componentIds.length - 1];
  const lastComponent = document.getElementById(lastComponentId);
  const lastComponentBound = lastComponent
    ? (lastComponent.getBoundingClientRect() as ClientRect & DOMRect)
    : undefined;
  if (lastComponentBound && clientOffset.y >= lastComponentBound.bottom) {
    if (dragHoverIndex !== componentIds.length - 1 || dragDirection !== 'up') {
      dispatch(
        setDragDirection(
          lastComponentId,
          componentIds.length - 1,
          'up',
          lastComponentBound,
          true,
          path
        )
      );
    }

    return;
  }

  const baseBounds = ([].filter.call(
    document.getElementById(parentComponentId)!.children,
    (el: HTMLElement) => {
      if (el.classList.contains('hoveredPointer')) return false;
      return el.nodeName !== 'STYLE';
    }
  ) as HTMLElement[]).map(child => ({
    id: child.id,
    bound: child.getBoundingClientRect() as ClientRect & DOMRect,
  }));

  const leftBounds = baseBounds.filter(
    ({ bound }) => clientOffset.y >= bound.top && clientOffset.y <= bound.bottom
  );
  const bounds = leftBounds.length === 0 ? baseBounds : leftBounds;
  const boundDists = bounds.map(({ id, bound }) => {
    const topLeft = { x: bound.left + 1, y: bound.top + 1 };
    const topRight = { x: bound.right - 1, y: bound.top + 1 };
    const bottomLeft = { x: bound.left + 1, y: bound.bottom - 1 };
    const bottomRight = { x: bound.right - 1, y: bound.bottom - 1 };

    const topLeftDist =
      (Math.abs(clientOffset.x - topLeft.x) +
        Math.abs(clientOffset.y - topLeft.y)) /
      2;
    const topRightDist =
      (Math.abs(clientOffset.x - topRight.x) +
        Math.abs(clientOffset.y - topRight.y)) /
      2;
    const bottomLeftDist =
      (Math.abs(clientOffset.x - bottomLeft.x) +
        Math.abs(clientOffset.y - bottomLeft.y)) /
      2;
    const bottomRightDist =
      (Math.abs(clientOffset.x - bottomRight.x) +
        Math.abs(clientOffset.y - bottomRight.y)) /
      2;

    return {
      id,
      bound,
      dist: { topLeftDist, topRightDist, bottomLeftDist, bottomRightDist },
    };
  });

  const nearest = boundDists.reduce(
    (prev, bound) => {
      const smallest = (Object.keys(
        bound.dist
      ) as (keyof typeof bound.dist)[]).reduce(
        (prev, key) => {
          if (prev === undefined || bound.dist[key] < prev.dest)
            return {
              id: bound.id,
              boundKey: key,
              dest: bound.dist[key],
              bound: bound.bound,
            };
          return prev;
        },
        undefined as Nearest
      );

      if (prev === undefined || smallest!.dest < prev.dest) return smallest;
      return prev;
    },
    undefined as Nearest
  );

  const hoverIndex = componentIds.indexOf(nearest!.id);
  const direction = nearest!.boundKey.includes('Left') ? 'down' : 'up';
  if (hoverIndex === dragHoverIndex && direction === dragDirection) return;

  dispatch(
    setDragDirection(
      nearest!.id,
      hoverIndex,
      direction,
      nearest!.bound,
      path === 'rootComponent' ? true : false,
      path
    )
  );
  return;
};
