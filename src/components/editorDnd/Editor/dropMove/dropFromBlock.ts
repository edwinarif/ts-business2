import { IPage, GeneralComponentType } from 'types';
import { insert, omitUndefined } from 'utils/mixins';
import { ExtendedFirebaseInstance } from 'react-redux-firebase';
import { LocalReducerType } from 'redux/reducers/localReducer';
import at from 'lodash/at';
import { getId } from 'utils/getId';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';
import { ComponentPropsType } from 'blockConfig/componentsMapper';
import { getIsCustomComponent } from 'utils/getIsCustomComponent';

export const dropFromBlock = (
  firebase: ExtendedFirebaseInstance,
  page: IPage,
  localReducer: LocalReducerType,
  blockId?: string,
  childProps?: ComponentPropsType,
  parentPath?: string,
  idx?: number,
  baseBlockComponents?: GeneralComponentType,
  componentIds?: string[],
  components?: { [key: string]: GeneralComponentType }
) => {
  const {
    currentProjectId,
    currentPageId,
    renderedBlockComponents,
    renderedCustomComponents,
    dragHoverPath,
    dragBlockId,
    isBlockDragging,
  } = localReducer;

  if (!isBlockDragging || !dragBlockId || !dragHoverPath) return;
  // console.log('insert');

  const baseBlock =
    baseBlockComponents ||
    renderedBlockComponents[blockId || dragBlockId] ||
    renderedCustomComponents[dragBlockId];
  const block = {
    ...baseBlock,
    components: baseBlock.components || components,
    componentIds: baseBlock.componentIds || componentIds,
  };

  if (!block) return;

  const firebaseRefPath = `projects/${currentProjectId}/pages/${currentPageId}/${(
    parentPath || dragHoverPath
  ).replace(/\./g, '/')}/components`;

  const parentBlock = at<any>(page, `${parentPath || dragHoverPath}`)[0];

  const baseProps =
    getIsBlockComponent(block) || getIsCustomComponent(block)
      ? omitUndefined({
          ...((getIsBlockComponent(block) && block.props) || childProps
            ? {
                ...(getIsBlockComponent(block) && block.props
                  ? block.props
                  : {}),
                ...(childProps || {}),
              }
            : {}),
        })
      : {};

  const { genericProps, nodeProps } =
    getIsBlockComponent(block) || getIsCustomComponent(block)
      ? Object.keys(baseProps).reduce(
          (prev, propsKey) => {
            if ((baseProps as any)[propsKey].blockType)
              return {
                genericProps: prev.genericProps,
                nodeProps: {
                  ...(prev.nodeProps || {}),
                  [propsKey]: (baseProps as any)[propsKey],
                },
              };

            return {
              genericProps: {
                ...(prev.genericProps || {}),
                [propsKey]: (baseProps as any)[propsKey],
              },
              nodeProps: prev.nodeProps,
            };
          },
          { genericProps: undefined, nodeProps: undefined } as {
            genericProps: object | undefined;
            nodeProps: object | undefined;
          }
        )
      : ({ genericProps: undefined, nodeProps: undefined } as {
          genericProps: object | undefined;
          nodeProps: object | undefined;
        });

  const newRef = firebase.ref(firebaseRefPath).push();
  const newValue = getIsBlockComponent(block)
    ? omitUndefined({
        blockId: block.blockId || block.id,
        blockType: block.blockType,
        label: block.label,
        props: omitUndefined({
          ...genericProps,
          ...Object.keys(nodeProps || {}).reduce(
            (res, nodePropsKey) => ({
              ...res,
              [nodePropsKey]: '',
            }),
            {}
          ),
          children:
            childProps && childProps.children !== undefined
              ? childProps.children
              : block && block.props && block.props.children
              ? block.props.children
              : undefined,
        }),
        className: getId(),
        isRenderByParent:
          parentPath ||
          (parentBlock &&
            (parentBlock.isParentRender || parentBlock.isRenderByParent))
            ? true
            : undefined,
        isParentRender: !parentPath && block.componentIds ? true : undefined,
      })
    : {
        customComponentId: block.id,
        label: block.componentName,
        componentName: block.componentName,
      };

  newRef.set(newValue).then(() => {
    if (
      (getIsBlockComponent(block) || getIsCustomComponent(block)) &&
      newRef.key
    ) {
      if (nodeProps) {
        Object.keys(nodeProps).forEach(nodePropsKey => {
          const firebaseRefProps = `projects/${currentProjectId}/pages/${currentPageId}/${(
            parentPath || dragHoverPath
          ).replace(/\./g, '/')}/components/${
            newRef.key
          }/props/${nodePropsKey}`;
          const newPropsRef = firebase.ref(firebaseRefProps).push();
          newPropsRef.set({
            ...((nodeProps as any)[nodePropsKey] || {}),
          });
        });
      }

      const getAtComponentIds = at<any>(
        page,
        `${parentPath || dragHoverPath}.componentIds`
      );
      if (idx === undefined) {
        firebase.set(
          `projects/${currentProjectId}/pages/${currentPageId}/${(
            parentPath || dragHoverPath
          ).replace(/\./g, '/')}/componentIds`,
          insert<string>(
            getAtComponentIds.length > 0 ? getAtComponentIds[0] : [],
            localReducer.dragDirection === 'down'
              ? localReducer.dragHoverIndex!
              : localReducer.dragHoverIndex! + 1,
            newRef.key
          )
        );
      } else {
        firebase.set(
          `projects/${currentProjectId}/pages/${currentPageId}/${(
            parentPath || dragHoverPath
          ).replace(/\./g, '/')}/componentIds/${idx}`,
          newRef.key
        );
      }

      if (block.components && block.componentIds) {
        block.componentIds.forEach((componentId, idx) => {
          const childBlock = block.components![componentId];
          if (getIsBlockComponent(childBlock)) {
            dropFromBlock(
              firebase,
              page,
              localReducer,
              childBlock.blockId,
              childBlock.props,
              `${parentPath || dragHoverPath}.components.${newRef.key}`,
              idx,
              childBlock,
              childBlock.componentIds,
              childBlock.components
            );
          }
        });
      }
    }
  });
};
