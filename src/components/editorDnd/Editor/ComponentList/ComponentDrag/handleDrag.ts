import { startDragging, stopDragging } from 'redux/actions/localActions';

export const handleDrag = (dispatch: any) => (
  ref: any,
  componentId: string,
  isDragging: boolean,
  index: number,
  path: string
) => () => {
  if (isDragging) {
    // console.log('start dragging', index, componentId, path);
    dispatch(
      startDragging(
        componentId,
        index,
        ref.current!.getBoundingClientRect(),
        path
      )
    );
  } else {
    dispatch(stopDragging());
  }
};
