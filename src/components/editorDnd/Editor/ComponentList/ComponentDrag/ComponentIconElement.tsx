import React, { useState } from 'react';
import { GeneralComponentType } from 'types';
import { useFirebase } from 'react-redux-firebase';
import { useDispatch } from 'react-redux';
import {
  setCurrentSelectedComponent,
  closeMainNav,
} from 'redux/actions/localActions';
// @ts-ignore
import isEqual from 'lodash/isEqual';
import isString from 'lodash/isString';
import { makeStyles, createStyles, Fab, Popper } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import decamelize from 'decamelize';
import at from 'lodash/at';
import { LocalReducerType } from 'redux/reducers/localReducer';

type ComponentIconElementType = {
  componentId: string;
  blockFirebase: GeneralComponentType;
  isSelected: boolean;
  isComponentDragging: boolean;
  isHoveredDrop: boolean;
  isHoveredUp: boolean;
  isHoveredDown: boolean;
  objCss?: { [key: string]: any };
  currentPath: string;
  localReducer: LocalReducerType;
  baseRef: any;
};

const ComponentIconElement = React.memo(
  ({
    componentId,
    blockFirebase,
    isSelected,
    isComponentDragging,
    isHoveredDrop,
    isHoveredUp,
    isHoveredDown,
    objCss,
    currentPath,
    localReducer,
    baseRef,
  }: ComponentIconElementType) => {
    const classes = useStyles({ isSelected, isComponentDragging });
    const dispatch = useDispatch();
    const firebase = useFirebase();
    const [isHover, setIsHover] = useState(false);

    const { currentProjectId, currentPageId, project } = localReducer;

    if (!blockFirebase || !project) return null;

    const anchorEl = document.getElementById(componentId);
    const handleClick = (e: React.MouseEvent<any, MouseEvent>) => {
      dispatch(
        setCurrentSelectedComponent(
          componentId,
          `${currentPath}.components.${componentId}`
        )
      );
      dispatch(closeMainNav());
      e.stopPropagation();
    };

    const parsedObjCss = objCss
      ? Object.keys(objCss).reduce(
          (prev, key) => ({
            ...prev,
            [key]: isString(objCss![key])
              ? objCss![key].replace(/^"+|"+$/g, '')
              : objCss![key],
          }),
          {} as any
        )
      : {};

    const handleDelete = () => {
      const componentIds = at<any>(
        project,
        `pages.${currentPageId}.${currentPath}.componentIds`
      )[0];
      firebase
        .ref(
          `projects/${currentProjectId}/pages/${currentPageId}/${currentPath.replace(
            /\./g,
            '/'
          )}`
        )
        .update({
          componentIds: componentIds.filter((id: any) => id !== componentId),
          [`components/${componentId}`]: null,
        });
    };

    const handleMouse = (isEnter: boolean) => (e: any) => {
      if (isEnter) {
        setIsHover(true);
      } else {
        setIsHover(false);
      }
    };

    const iconName = decamelize(
      ((blockFirebase as any).blockType as string).replace('MuiIcon', '')
    );
    return (
      <>
        {/* {console.log('rerender element', componentId, nestedComponent, element)} */}
        <div
          id={componentId}
          ref={baseRef}
          onClick={handleClick}
          onMouseEnter={handleMouse(true)}
          onMouseLeave={handleMouse(false)}
          style={getContainerStyle(parsedObjCss)}
        >
          <Icon
            style={{
              ...getStyle(
                isSelected,
                isComponentDragging,
                isHoveredDrop,
                isHoveredUp,
                isHoveredDown,
                isHover
              ),
              ...parsedObjCss,
            }}
          >
            {iconName}
          </Icon>
        </div>
        <Popper
          id={`popper-${componentId}`}
          open={isSelected}
          anchorEl={anchorEl}
          placement='bottom-end'
          className={classes.popperMain}
        >
          <div className={classes.popper}>
            <Fab
              color='default'
              aria-label='delete'
              size='small'
              className={classes.popperFab}
              onClick={handleDelete}
            >
              <Icon className={classes.popperFabIcon}>delete</Icon>
            </Fab>
            <Fab
              color='default'
              aria-label='select parent'
              size='small'
              className={classes.popperFab}
            >
              <Icon className={classes.popperFabIcon}>arrow_upward</Icon>
            </Fab>
          </div>
        </Popper>
      </>
    );
  },
  (prevProps, nextProps) => {
    return (
      prevProps.isComponentDragging === nextProps.isComponentDragging &&
      prevProps.isSelected === nextProps.isSelected &&
      prevProps.isHoveredDrop === nextProps.isHoveredDrop &&
      prevProps.isHoveredUp === nextProps.isHoveredUp &&
      prevProps.isHoveredDown === nextProps.isHoveredDown &&
      isEqual(prevProps.blockFirebase, nextProps.blockFirebase) &&
      isEqual(prevProps.objCss, nextProps.objCss)
    );
  }
);

const getContainerStyle = (parsedObjCss: any) => ({
  display: 'inline-block',
  width: parsedObjCss && parsedObjCss.width ? parsedObjCss.width : 24,
  height: parsedObjCss && parsedObjCss.height ? parsedObjCss.height : 24,
});

const getStyle = (
  isComponentSelected: boolean,
  isComponentDragging: boolean,
  isHoveredDrop: boolean,
  isHoveredUp: boolean,
  isHoveredDown: boolean,
  isHover: boolean
) => ({
  opacity: (() => {
    if (isComponentDragging) return 0.4;
    return 1;
  })(),
  cursor: 'move',
  outline: (() => {
    if (isHoveredUp || isHoveredDown) return '2px dashed rgba(255,193,7,0.7)';
    if (isComponentDragging) return '2px dashed rgba(0,0,0,0.3)';
    if (isHoveredDrop) return '3px solid rgba(255,193,7,0.7)';
    if (isComponentSelected) return '3px solid rgba(100,181,246,1)';
    if (isHover) return '2px dashed rgba(100,181,246,1)';
    return undefined;
  })(),
});

const useStyles = makeStyles(() =>
  createStyles({
    popperMain: {
      zIndex: 1100,
    },
    popper: {
      visibility: ({ isSelected, isComponentDragging }: any) =>
        isSelected && !isComponentDragging ? 'visible' : 'hidden',
      zIndex: 999,
      marginTop: 10,
    },
    popperFab: {
      marginLeft: 8,
      width: 32,
      height: 32,
      minWidth: 'unset',
      minHeight: 'unset',
    },
    popperFabIcon: {
      fontSize: 16,
      minWidth: 'unset',
      minHeight: 'unset',
    },
  })
);

export default ComponentIconElement;
