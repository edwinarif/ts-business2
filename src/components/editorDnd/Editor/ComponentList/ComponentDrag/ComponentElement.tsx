import React, { useState, useEffect } from 'react';
import {
  GeneralComponentType,
  NestedComponentType,
  RenderedComponentType,
} from 'types';
import { useFirebase } from 'react-redux-firebase';
import { useDispatch } from 'react-redux';
import {
  setCurrentSelectedComponent,
  closeMainNav,
  setMerge,
} from 'redux/actions/localActions';
// @ts-ignore
import { Parser as HtmlParser } from 'html-to-react';
import { parseBlockComponent } from 'utils/parseBlockComponent';
import isEqual from 'lodash/isEqual';
import { ComponentPropsType } from 'blockConfig/componentsMapper';
import isValidCss from 'utils/isValidCss';
import { CSSProperties } from '@material-ui/styles';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import { Portal } from 'react-portal';
import isString from 'lodash/isString';
import { makeStyles, createStyles, Fab, Popper } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import at from 'lodash/at';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { parseCustomComponent } from 'utils/parseCustomComponent';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';
import ComponentList from '..';
import isEmpty from 'lodash/isEmpty';
import { getParentComponent } from 'utils/getParentComponent';
import { getIsCustomComponent } from 'utils/getIsCustomComponent';

type ComponentElementType = {
  componentId: string;
  themeConfig: ThemeOptions;
  blockFirebase: GeneralComponentType;
  isSelected: boolean;
  isComponentDragging: boolean;
  isHoveredDrop: boolean;
  isHoveredUp: boolean;
  isHoveredDown: boolean;
  customProps: ComponentPropsType;
  strCss?: string;
  objCss?: { [key: string]: any };
  nestedComponent: NestedComponentType;
  currentPath: string;
  localReducer: LocalReducerType;
  baseRef: any;
  isBlockComponent?: boolean;
  isParentRender?: boolean;
  isRenderByParent?: boolean;
};

const ComponentElement = ({
  componentId,
  themeConfig,
  blockFirebase,
  isSelected,
  isComponentDragging,
  isHoveredDrop,
  isHoveredUp,
  isHoveredDown,
  customProps,
  strCss,
  objCss,
  nestedComponent,
  currentPath,
  localReducer,
  baseRef,
  isParentRender,
  isRenderByParent,
}: ComponentElementType) => {
  const classes = useStyles({ isSelected, isComponentDragging });
  const dispatch = useDispatch();
  const firebase = useFirebase();
  const [isHover, setIsHover] = useState(false);
  const [component, setComponent] = useState<RenderedComponentType | undefined>(
    undefined
  );

  const {
    currentProjectId,
    currentPageId,
    project,
    renderedCustomComponents,
    renderedNestedComponents,
  } = localReducer;

  /** Get Html & Css */
  useEffect(() => {
    if (project) {
      if (getIsBlockComponent(blockFirebase)) {
        console.log(blockFirebase, isRenderByParent, isParentRender);
        if (isRenderByParent) {
          const parentComponent = getParentComponent(
            currentPath,
            renderedNestedComponents
          );

          if (parentComponent) {
            var htmlObject = document.createElement('div');
            htmlObject.innerHTML = parentComponent.baseHtml;
            const childHtml = htmlObject.querySelector(`#${blockFirebase.id}`);
            const res = {
              ...blockFirebase,
              baseHtml: childHtml ? childHtml.outerHTML : '',
              baseCss: '',
            };
            setComponent(res);
          } else {
            setComponent(undefined);
          }
        } else {
          const renderedComponent = parseBlockComponent(
            blockFirebase,
            themeConfig,
            componentId,
            customProps
          );

          if (isParentRender) {
            dispatch(
              setMerge({
                key: 'renderedNestedComponents',
                value: {
                  [componentId]: renderedComponent,
                },
              })
            );
          }

          setComponent(renderedComponent);
        }
      } else {
        if (
          !getIsBlockComponent(blockFirebase) &&
          blockFirebase.customComponentId
        ) {
          setComponent(
            renderedCustomComponents[blockFirebase.customComponentId]
          );
        } else {
          setComponent(
            parseCustomComponent({
              customComponent: blockFirebase,
              themeConfig,
              customComponents: project.customComponents,
            }) || undefined
          );
        }
      }
    }
  }, [
    blockFirebase,
    componentId,
    currentPath,
    customProps,
    dispatch,
    isParentRender,
    isRenderByParent,
    project,
    renderedCustomComponents,
    renderedNestedComponents,
    setComponent,
    themeConfig,
  ]);

  if (!blockFirebase || !project) return null;
  if (
    !component ||
    !(getIsBlockComponent(blockFirebase) || getIsCustomComponent(blockFirebase))
  )
    return null;

  const htmlParser = new HtmlParser();
  const element = htmlParser.parse(component.baseHtml);

  const anchorEl = document.getElementById(componentId);

  const handleClick = (e: React.MouseEvent<any, MouseEvent>) => {
    dispatch(
      setCurrentSelectedComponent(
        componentId,
        `${currentPath}.components.${componentId}`
      )
    );
    dispatch(closeMainNav());
    e.stopPropagation();
  };

  const parsedObjCss = objCss
    ? Object.keys(objCss).reduce(
        (prev, key) => ({
          ...prev,
          [key]: isString(objCss![key])
            ? objCss![key].replace(/^"+|"+$/g, '')
            : objCss![key],
        }),
        {} as any
      )
    : {};

  const handleDelete = () => {
    const componentIds = at<any>(
      project,
      `pages.${currentPageId}.${currentPath}.componentIds`
    )[0];
    firebase
      .ref(
        `projects/${currentProjectId}/pages/${currentPageId}/${currentPath.replace(
          /\./g,
          '/'
        )}`
      )
      .update({
        componentIds: componentIds.filter((id: any) => id !== componentId),
        [`components/${componentId}`]: null,
      });
  };

  const handleMouse = (isEnter: boolean) => (e: any) => {
    if (isEnter) {
      setIsHover(true);
    } else {
      setIsHover(false);
    }
  };

  if (!element) return null;

  return (
    <>
      {/* {console.log('rerender element', componentId, nestedComponent, element)} */}
      <element.type
        {...element.props}
        id={componentId}
        ref={baseRef}
        style={{
          ...getStyle(
            isSelected,
            isComponentDragging,
            isHoveredDrop,
            isHoveredUp,
            isHoveredDown,
            getIsBlockComponent(component) && component.blockStyleCss
              ? component.blockStyleCss
              : {},
            component.baseCss.includes('min-height'),
            isHover
          ),
          ...parsedObjCss,
        }}
        onClick={handleClick}
        onMouseEnter={handleMouse(true)}
        onMouseLeave={handleMouse(false)}
        children={
          <>
            {nestedComponent &&
            nestedComponent.components &&
            nestedComponent.componentIds &&
            nestedComponent.componentIds.length > 0 ? (
              <ComponentList
                currentPath={`${currentPath}.components.${componentId}`}
              />
            ) : null}
            {getIsBlockComponent(blockFirebase) && blockFirebase.props
              ? blockFirebase.props.children || null
              : null}
            {getIsCustomComponent(blockFirebase) && element.props.children}
          </>
        }
      />
      <Popper
        id={`popper-${componentId}`}
        open={isSelected}
        anchorEl={anchorEl}
        placement='bottom-end'
        className={classes.popperMain}
      >
        <div className={classes.popper}>
          <Fab
            color='default'
            aria-label='delete'
            size='small'
            className={classes.popperFab}
            onClick={handleDelete}
          >
            <Icon className={classes.popperFabIcon}>delete</Icon>
          </Fab>
          <Fab
            color='default'
            aria-label='select parent'
            size='small'
            className={classes.popperFab}
          >
            <Icon className={classes.popperFabIcon}>arrow_upward</Icon>
          </Fab>
        </div>
      </Popper>
      <Portal node={document && document.getElementById('styles')}>
        <>
          <style>{component.baseCss}</style>
          {strCss && isValidCss(strCss) && (
            <style>{`#${componentId}{${strCss}}`}</style>
          )}
        </>
      </Portal>
    </>
  );
};

const getStyle = (
  isComponentSelected: boolean,
  isComponentDragging: boolean,
  isHoveredDrop: boolean,
  isHoveredUp: boolean,
  isHoveredDown: boolean,
  blockStyleCss: CSSProperties,
  isContainMinHeight: boolean,
  isHover: boolean
) => ({
  ...blockStyleCss,
  minHeight: isContainMinHeight ? undefined : 32,
  opacity: (() => {
    if (isComponentDragging) return 0.4;
    return 1;
  })(),
  cursor: 'move',
  outline: (() => {
    if (isHoveredUp || isHoveredDown) return '2px dashed rgba(255,193,7,0.7)';
    if (isComponentDragging) return '2px dashed rgba(0,0,0,0.3)';
    if (isHoveredDrop) return '3px solid rgba(255,193,7,0.7)';
    if (isComponentSelected) return '3px solid rgba(100,181,246,1)';
    if (isHover) return '2px dashed rgba(100,181,246,1)';
    return blockStyleCss && blockStyleCss.outline
      ? blockStyleCss.outline
      : undefined;
  })(),
});

const useStyles = makeStyles(() =>
  createStyles({
    popperMain: {
      zIndex: 1100,
    },
    popper: {
      visibility: ({ isSelected, isComponentDragging }: any) =>
        isSelected && !isComponentDragging ? 'visible' : 'hidden',
      zIndex: 999,
      marginTop: 10,
    },
    popperFab: {
      marginLeft: 8,
      width: 32,
      height: 32,
      minWidth: 'unset',
      minHeight: 'unset',
    },
    popperFabIcon: {
      fontSize: 16,
      minWidth: 'unset',
      minHeight: 'unset',
    },
  })
);

const areEquals = (prevProps: any, nextProps: any) => {
  const getRenderByParent = () => {
    if (!nextProps.isRenderByParent) return true;

    const pathSplit = nextProps.currentPath.split('.');
    if (pathSplit[pathSplit.length - 2] !== 'components') return true;

    const parentId = pathSplit[pathSplit.length - 1];

    return (
      prevProps.localReducer.renderedNestedComponents[parentId].baseHtml ===
      nextProps.localReducer.renderedNestedComponents[parentId].baseHtml
    );
  };

  return (
    prevProps.isComponentDragging === nextProps.isComponentDragging &&
    prevProps.isSelected === nextProps.isSelected &&
    prevProps.isHoveredDrop === nextProps.isHoveredDrop &&
    prevProps.isHoveredUp === nextProps.isHoveredUp &&
    prevProps.isHoveredDown === nextProps.isHoveredDown &&
    prevProps.themeConfig === nextProps.themeConfig &&
    prevProps.strCss === nextProps.strCss &&
    (prevProps.nestedComponent.componentIds || []).length ===
      (nextProps.nestedComponent.componentIds || []).length &&
    isEqual(prevProps.blockFirebase, nextProps.blockFirebase) &&
    isEqual(prevProps.customProps, nextProps.customProps) &&
    isEqual(
      prevProps.nestedComponent.components || {},
      nextProps.nestedComponent.components || {}
    ) &&
    isEqual(prevProps.objCss, nextProps.objCss) &&
    getRenderByParent() &&
    (!nextProps.isBlockComponent
      ? !isEmpty(nextProps.localReducer.renderedCustomComponents) &&
        nextProps.localReducer.renderedCustomComponents[
          nextProps.componentId
        ] &&
        isEqual(
          prevProps.localReducer.renderedCustomComponents[
            prevProps.componentId
          ],
          nextProps.localReducer.renderedCustomComponents[nextProps.componentId]
        )
      : true)
  );
};

export default React.memo(ComponentElement, areEquals);
