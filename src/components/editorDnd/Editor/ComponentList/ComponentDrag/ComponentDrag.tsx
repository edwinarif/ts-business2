import React, { useRef } from 'react';
import { makeStyles, createStyles } from '@material-ui/core';
import clsx from 'clsx';
import ComponentElement from './ComponentElement';
import { useDrop, useDrag } from 'react-dnd';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import { getIsChildComponent } from 'utils/getIsChildComponent';
import { handleDropHoverBase } from '../../handleDropHoverBase';
import ComponentIconElement from './ComponentIconElement';

type ComponentDragType = {
  themeConfig: ThemeOptions;
  componentData: any;
  onDropHover: any;
  onDragging: any;
  handleDrop: any;
  dragHoverBound: any;
  currentPath: string;
};

const ComponentDrag = ({
  componentData,
  themeConfig,
  onDropHover,
  onDragging,
  handleDrop,
  dragHoverBound,
  currentPath,
}: ComponentDragType) => {
  const dispatch = useDispatch();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );

  const { dragComponentId, dragHoverComponentId } = localReducer;

  const {
    componentId,
    blockFirebase,
    index,
    isSelected,
    isComponentDragging,
    isHovered,
    isHoveredUp,
    isHoveredDown,
    customProps,
    components,
    componentIds,
    strCss,
    objCss,
    isBlockComponent,
    isParentRender,
    isRenderByParent,
  } = componentData;

  const classes = useStyles({
    bound: dragHoverBound,
    isHoveredDown,
    isSelected,
    isComponentDragging,
  });

  const ref = useRef<HTMLElement>(null);

  const [{ isOverCurrent }, drop] = useDrop({
    accept: (blockFirebase && blockFirebase.acceptDrop) || '',
    hover: (item, monitor) => {
      if (!components) {
        onDropHover(ref, componentId, index, currentPath)(item, monitor);
        return;
      }

      handleDropHoverBase(
        dispatch,
        localReducer,
        componentId,
        `${currentPath}.components.${componentId}`
      )(item, monitor);
      return;
    },
    drop: handleDrop,
    collect: monitor => ({
      isOverCurrent: monitor.isOver({ shallow: true }),
    }),
    canDrop: (item, monitor) => {
      const isChildComponent = getIsChildComponent(localReducer);
      if (isChildComponent) {
        return false;
      }

      return true;
    },
  });

  const [, drag] = useDrag({
    item: {
      id: componentId,
      name: componentId,
      type: 'box',
    },
    collect: monitor => {
      return {
        isDragging: monitor.isDragging(),
      };
    },
    begin: onDragging(ref, componentId, true, index, currentPath),
    end: onDragging(ref, componentId, false, index, currentPath),
  });
  drag(drop(ref));

  return (
    <>
      {isHovered && isHoveredDown && (
        <div className={clsx(classes.divHovered, 'hoveredPointer')} />
      )}
      {isOverCurrent &&
        !isHoveredUp &&
        !isHoveredDown &&
        !components &&
        componentId !== 'rootComponent' &&
        dragHoverComponentId !== dragComponentId && (
          <div className={classes.dropChildPointer} />
        )}
      {blockFirebase.blockType &&
      blockFirebase.blockType.includes('MuiIcon') ? (
        <ComponentIconElement
          componentId={componentId}
          blockFirebase={blockFirebase}
          isSelected={isSelected}
          isComponentDragging={isComponentDragging}
          isHoveredDrop={isOverCurrent && !isHoveredUp && !isHoveredDown}
          isHoveredUp={isHoveredUp}
          isHoveredDown={isHoveredDown}
          currentPath={currentPath}
          objCss={objCss}
          localReducer={localReducer}
          baseRef={ref}
        />
      ) : (
        <ComponentElement
          componentId={componentId}
          themeConfig={themeConfig}
          blockFirebase={blockFirebase}
          isSelected={isSelected}
          isComponentDragging={isComponentDragging}
          isHoveredDrop={isOverCurrent && !isHoveredUp && !isHoveredDown}
          isHoveredUp={isHoveredUp}
          isHoveredDown={isHoveredDown}
          customProps={customProps}
          nestedComponent={{ components, componentIds }}
          currentPath={currentPath}
          strCss={strCss}
          objCss={objCss}
          localReducer={localReducer}
          baseRef={ref}
          isBlockComponent={isBlockComponent}
          isParentRender={isParentRender}
          isRenderByParent={isRenderByParent}
        />
      )}
      {isHovered && isHoveredUp && (
        <div className={clsx(classes.divHovered, 'hoveredPointer')} />
      )}
    </>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      minHeight: 32,
    },
    divHovered: {
      position: 'fixed',
      zIndex: 999,
      top: ({ bound }: any) => (bound ? bound.top : 100),
      left: ({ bound, isHoveredDown }: any) =>
        bound ? (isHoveredDown ? bound.left : bound.right) : undefined,
      width: 0,
      height: ({ bound }: any) => (bound ? bound.height : 12),
      minHeight: 12,
      boxShadow: '0px 0px 0px 4px rgba(255,193,7,0.7)',
    },
    dropChildPointer: {
      position: 'fixed',
      zIndex: 999,
      top: ({ bound }: any) =>
        bound ? bound.top + bound.height / 2 : undefined,
      width: ({ bound }: any) =>
        bound ? bound.width - bound.width / 8 : undefined,
      left: ({ bound }: any) =>
        bound ? bound.left + bound.width / 16 : undefined,
      height: 0,
      boxShadow: '0px 0px 0px 2px rgba(255,193,7,0.2)',
    },
  })
);

export default ComponentDrag;
