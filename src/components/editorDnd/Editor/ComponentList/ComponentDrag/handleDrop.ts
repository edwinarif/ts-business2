import { stopDragging, setIsAddItemOpen } from 'redux/actions/localActions';
import { IPage } from 'types';
import { DropTargetMonitor } from 'react-dnd';
import { dropMove } from 'components/editorDnd/Editor/dropMove/dropMove';
import { ExtendedFirebaseInstance } from 'react-redux-firebase';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { dropFromBlock } from 'components/editorDnd/Editor/dropMove/dropFromBlock';

export const handleDrop = (
  dispatch: any,
  firebase: ExtendedFirebaseInstance,
  localReducer: LocalReducerType,
  page: IPage
) => (item: any, monitor: DropTargetMonitor) => {
  dispatch(stopDragging());
  if (!monitor.isOver({ shallow: true })) return;

  if (
    !localReducer.isBlockDragging &&
    localReducer.dragHoverComponentId !== localReducer.dragComponentId
  ) {
    // console.log('component moved');
    dropMove(dispatch, firebase, page, localReducer);
    return;
  }

  if (localReducer.isBlockDragging) {
    // console.log('insert new component from block');
    dropFromBlock(firebase, page, localReducer);
    dispatch(setIsAddItemOpen(false));
    return;
  }

  return;
};
