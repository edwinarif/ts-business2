import { AppState } from 'index';
import at from 'lodash/at';
import generatePropsFromDefault from 'utils/generatePropsFromDefault';
import { useSelector } from 'react-redux';
import { GeneralComponentType } from 'types';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { omitUndefined } from 'utils/mixins';

function useParseComponent(props: any) {
  const { currentPath } = props;
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );

  const {
    project,
    currentPageId,
    page,
    currentSelectedComponent,
    dragComponentId,
    dragHoverIndex,
    dragDirection,
    renderedBlockComponents,
  } = localReducer;

  if (!page || !currentPath || !project || !project.pages![currentPageId])
    return {};

  const components = at<{ [key: string]: GeneralComponentType } | undefined>(
    page as any,
    `${currentPath}.components`
  )[0];
  const componentIds = at<string[] | undefined>(
    page as any,
    `${currentPath}.componentIds`
  )[0];

  if (!components || !componentIds) return {};

  const componentsData = componentIds.reduce((res, componentId: string) => {
    const firebaseComponent = components[componentId];
    if (!firebaseComponent === undefined) return res;

    const index = componentIds.indexOf(componentId);
    const blockId = at<any>(
      page,
      `${currentPath}.components.${componentId}.blockId`
    )[0];

    const baseComponent = {
      componentId,
      index,
      isSelected: componentId === currentSelectedComponent,
      isComponentDragging: dragComponentId === componentId,
      isHovered: dragHoverIndex === index,
      isHoveredUp: dragHoverIndex === index && dragDirection === 'up',
      isHoveredDown: dragHoverIndex === index && dragDirection === 'down',
    };

    if (getIsBlockComponent(firebaseComponent)) {
      if (
        !renderedBlockComponents[blockId] &&
        !firebaseComponent.blockType.includes('MuiIcon')
      )
        return res;

      return {
        ...res,
        [componentId]: {
          ...baseComponent,
          blockFirebase: firebaseComponent.blockType.includes('MuiIcon')
            ? {
                ...firebaseComponent,
                id: componentId,
              }
            : {
                ...renderedBlockComponents[blockId],
                id: componentId || renderedBlockComponents[blockId],
                components:
                  firebaseComponent.components ||
                  renderedBlockComponents[blockId].components,
                componentIds:
                  firebaseComponent.componentIds ||
                  renderedBlockComponents[blockId].componentIds,
                props: firebaseComponent.props,
              },
          customProps: firebaseComponent.props
            ? generatePropsFromDefault(
                firebaseComponent.blockType,
                firebaseComponent.props
              )
            : {},
          isBlockComponent: true,
          ...omitUndefined({
            isParentRender: firebaseComponent.isParentRender,
            isRenderByParent: firebaseComponent.isRenderByParent,
            components: firebaseComponent.components,
            componentIds: firebaseComponent.componentIds,
            strCss: firebaseComponent.strCss,
            objCss: firebaseComponent.objCss,
          }),
        },
      };
    }

    return {
      ...res,
      [componentId]: {
        ...baseComponent,
        blockFirebase: components[componentId],
        isBlockComponent: false,
      },
    };
  }, {} as { [key: string]: any });

  return componentsData;
}

export default useParseComponent;
