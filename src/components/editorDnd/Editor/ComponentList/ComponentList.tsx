import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFirebase } from 'react-redux-firebase';
import { setMerge } from 'redux/actions/localActions';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { AppState } from 'index';
import at from 'lodash/at';
import ComponentDrag from './ComponentDrag';
import { handleDrop } from './ComponentDrag/handleDrop';
import { handleDropHover } from './ComponentDrag/handleDropHover';
import { handleDrag } from './ComponentDrag/handleDrag';
import useParseComponent from './useParseComponent';

const ComponentList = ({ currentPath }: any) => {
  const firebase = useFirebase();
  const dispatch = useDispatch();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const { page, dragHoverBound, themes } = localReducer;

  const themeConfig = themes && page ? themes[page.themeId] : undefined;
  const localComponentIds = at<string[] | undefined>(
    page as any,
    `${currentPath}.componentIds`
  )[0];

  const componentsData = useParseComponent({ currentPath });

  useEffect(() => {
    if (page && page.themeId && componentsData && !themeConfig) {
      firebase.ref(`/themes/${page.themeId}`).once('value', snapshot => {
        dispatch(
          setMerge({
            key: 'themes',
            value: {
              [page.themeId]: {
                ...snapshot.val(),
                id: page.themeId,
              },
            },
          })
        );
      });
    }
  }, [componentsData, themeConfig, page, dispatch, firebase]);

  if (!page || !themeConfig || !localComponentIds) return null;

  return (
    <>
      {localComponentIds.map(componentId => {
        if (componentsData[componentId]) {
          return (
            <React.Fragment key={componentId}>
              <ComponentDrag
                themeConfig={themeConfig.config}
                componentData={componentsData[componentId]}
                onDropHover={handleDropHover(dispatch, localReducer)}
                onDragging={handleDrag(dispatch)}
                handleDrop={handleDrop(dispatch, firebase, localReducer, page)}
                dragHoverBound={dragHoverBound}
                currentPath={currentPath}
              />
            </React.Fragment>
          );
        }

        return null;
      })}
    </>
  );
};

export default ComponentList;
