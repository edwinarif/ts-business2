import React from 'react';
import { useDrag } from 'react-dnd';
import { createStyles, makeStyles } from '@material-ui/core';
import { useDispatch } from 'react-redux';
// @ts-ignore
import { Parser as HtmlParser } from 'html-to-react';
import { setIsBlockDragging, stopDragging } from 'redux/actions/localActions';
import clsx from 'clsx';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';
import { RenderedComponentType } from 'types';

const BlockDrag = ({
  renderedBlockComponent,
}: {
  renderedBlockComponent: RenderedComponentType;
}) => {
  const dispatch = useDispatch();

  const [{ isDragging }, drag] = useDrag({
    item: {
      id: renderedBlockComponent.id,
      blockType: getIsBlockComponent(renderedBlockComponent)
        ? renderedBlockComponent.blockType
        : undefined,
      type: 'box',
    },
    begin: () => {
      dispatch(setIsBlockDragging(true, renderedBlockComponent.id));
    },
    end: () => {
      dispatch(stopDragging());
      dispatch(setIsBlockDragging(false, undefined));
    },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const classes = useStyles({ isDragging });

  if (!renderedBlockComponent) return null;

  const htmlParser = new HtmlParser();
  const element = htmlParser.parse(renderedBlockComponent.baseHtml);

  return (
    <div ref={drag} className={clsx(classes.root)}>
      <element.type
        {...((renderedBlockComponent as any).customHtmlBlock
          ? {}
          : element.props)}
        className={clsx(element.props.className)}
        style={{ ...((renderedBlockComponent as any).blockStyleCss || {}) }}
      />
      <style>{renderedBlockComponent.baseCss}</style>
    </div>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      minHeight: 36,
      minWidth: 36,
      padding: 12,
      cursor: 'move !important',
      opacity: ({ isDragging }: any) => (isDragging ? 0.4 : 1),
      '&:hover': {
        boxShadow: '2px 2px 5px 0px rgba(204,204,204,1)',
      },
    },
  })
);

export default BlockDrag;
