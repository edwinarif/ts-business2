import React from 'react';
import { useDrag } from 'react-dnd';
import { createStyles, makeStyles, Icon } from '@material-ui/core';
import { useDispatch } from 'react-redux';
// @ts-ignore
import { setIsBlockDragging, stopDragging } from 'redux/actions/localActions';
import clsx from 'clsx';

const BlockIconDrag = ({ iconData }: { iconData: any }) => {
  const dispatch = useDispatch();

  const [{ isDragging }, drag] = useDrag({
    item: {
      id: iconData.id,
      blockType: `MuiIcon${iconData.name}`,
      type: 'box',
    },
    begin: () => {
      dispatch(setIsBlockDragging(true, iconData.id));
    },
    end: () => {
      dispatch(stopDragging());
      dispatch(setIsBlockDragging(false, undefined));
    },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const classes = useStyles({ isDragging });

  if (!iconData) return null;

  return (
    <div ref={drag} className={clsx(classes.root)}>
      <Icon>{iconData.snakeCase}</Icon>
    </div>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      minHeight: 36,
      minWidth: 36,
      padding: 12,
      cursor: 'move !important',
      opacity: ({ isDragging }: any) => (isDragging ? 0.4 : 1),
      '&:hover': {
        boxShadow: '2px 2px 5px 0px rgba(204,204,204,1)',
      },
    },
  })
);

export default BlockIconDrag;
