import React, { useState } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { GeneralComponentType } from 'types';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
} from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import { getTitle } from 'utils/getTitle';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';

type ListRecursiveProps = {
  component: GeneralComponentType;
  blocks: { [key: string]: GeneralComponentType };
  deepLevel: number;
};

const ListRecursive = (props: ListRecursiveProps) => {
  const { component, blocks, deepLevel } = props;
  const classes = useStyles({
    deepLevel,
    haveChild: getIsBlockComponent(component) && !!component.componentIds,
  });
  const [isOpen, setIsOpen] = useState(true);

  return (
    <List component='div' dense disablePadding>
      <ListItem
        button={!!component.componentIds as any}
        className={classes.listItemButton}
        onClick={() => {
          if (component.componentIds) setIsOpen(!isOpen);
        }}
      >
        <ListItemIcon className={classes.listItemIcon}>
          {component.componentIds ? (
            <>{isOpen ? <Icon>expand_less</Icon> : <Icon>expand_more</Icon>}</>
          ) : (
            <Icon className={classes.radioUnchecked}>
              radio_button_unchecked
            </Icon>
          )}
        </ListItemIcon>
        <ListItemText primary={getTitle(component, blocks, true)} />
      </ListItem>
      {component.componentIds && component.components && (
        <Collapse in={isOpen} timeout='auto' unmountOnExit>
          {component.componentIds.map(componentId => {
            const childComponent = component.components![componentId];
            return (
              <ListRecursive
                key={`listRecursive-${componentId}`}
                component={childComponent}
                blocks={blocks}
                deepLevel={deepLevel + 1}
              />
            );
          })}
        </Collapse>
      )}
    </List>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    listItemIcon: {
      minWidth: ({ haveChild }: any) => (haveChild ? 32 : 24),
      marginLeft: ({ haveChild }: any) => (haveChild ? 'inherit' : 8),
    },
    listItemButton: {
      paddingLeft: ({ deepLevel }: any) => (deepLevel - 1) * 18 + 4,
      paddingTop: 0,
      paddingBottom: 0,
    },
    radioUnchecked: {
      fontSize: 8,
    },
  })
);

export default ListRecursive;
