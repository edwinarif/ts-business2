import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Card, CardContent, Grow, AppBar, Tabs } from '@material-ui/core';
import DragCloseCardHeader from 'components/cards/generals/DragCloseCardHeader';
import TabPanel from 'components/TabPanel';
import { CardContentProps } from '@material-ui/core/CardContent';
import clsx from 'clsx';
import { TypographyProps } from '@material-ui/core/Typography';

type ChildCardType = {
  isOpen: boolean;
  onClickClose: () => void;
  cardWidth?: number;
  cardContentProps?: CardContentProps;
  cardLeftPosition?: number;
  title?: string;
  titleTypographyProps?: Partial<TypographyProps>;
  isAddItemOpen?: boolean;
  isSelectThemeOpen?: boolean;
  isLayersCardOpen?: boolean;
  isAddPageOpen?: boolean;
};

const ChildCard: React.FC<ChildCardType> = ({
  isOpen,
  onClickClose,
  cardContentProps,
  cardWidth,
  cardLeftPosition,
  title,
  titleTypographyProps,
  isAddItemOpen,
  isSelectThemeOpen,
  isLayersCardOpen,
  isAddPageOpen,
  ...props
}) => {
  const classes = useStyles({ cardWidth, cardLeftPosition });

  const getHeaderTitle = () => {
    if (isAddItemOpen) return 'Components';
    if (isSelectThemeOpen) return 'Themes';
    if (isLayersCardOpen) return 'Layers';
    if (isAddPageOpen) return 'Pages';
    return '';
  };

  return (
    <Grow in={isOpen}>
      <Card elevation={2} className={classes.root}>
        <DragCloseCardHeader
          className={classes.header}
          onClickClose={onClickClose}
          title={title || getHeaderTitle()}
          titleTypographyProps={
            titleTypographyProps || {
              variant: 'subtitle1',
            }
          }
          customClasses={{
            action: classes.headerAction,
            title: classes.headerTitle,
          }}
        />
        <CardContent
          className={clsx(
            cardContentProps ? cardContentProps.className : '',
            classes.cardContent
          )}
          {...cardContentProps}
        >
          <AppBar position='sticky' className={classes.appBar}>
            <Tabs value={0} onChange={() => {}}></Tabs>
          </AppBar>
          <TabPanel value={0} index={0}>
            {props.children}
          </TabPanel>
        </CardContent>
      </Card>
    </Grow>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'absolute',
      maxHeight: '70vh',
      maxWidth: 450,
      width: ({ cardWidth }: any) =>
        cardWidth !== undefined ? cardWidth : 450,
      left: ({ cardLeftPosition }: any) =>
        cardLeftPosition !== undefined ? cardLeftPosition : 92,
      top: 2,
      zIndex: 1101,
      minWidth: 100,
      minHeight: 100,
    },
    appBar: {
      visibility: 'hidden',
      height: 0,
    },
    cardContent: {
      maxHeight: 'calc(70vh - 32px)',
      overflowY: 'auto',
      padding: 0,
    },
    header: {
      backgroundColor: 'rgba(117,117,117,0.6)',
      padding: '4px 24px',
      color: 'rgba(255,255,255,0.7)',
      minHeight: 36,
    },
    headerAction: {
      marginTop: 0,
      marginBottom: 0,
    },
    headerTitle: {},
  })
);

export default ChildCard;
