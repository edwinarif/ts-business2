import React from 'react';
import {
  CardHeader,
  IconButton,
  makeStyles,
  createStyles,
} from '@material-ui/core';
import { CardHeaderProps } from '@material-ui/core/CardHeader';
import Icon from '@material-ui/core/Icon';

const DragCloseCardHeader = (
  props: CardHeaderProps & {
    onClickClose?: () => void;
    hideClose?: boolean;
    customClasses?: any;
  }
) => {
  const { onClickClose, hideClose, customClasses, ...rest } = props;
  const classes = useStyles();

  return (
    <CardHeader
      action={
        <div className={classes.root}>
          {/* <DragHandle className={classes.dragHandle} /> */}
          {!hideClose && (
            <IconButton size='small' onClick={onClickClose}>
              <Icon className={classes.closeIcon}>close</Icon>
            </IconButton>
          )}
        </div>
      }
      classes={{
        ...(customClasses || {}),
        action: classes.action,
      }}
      {...rest}
    />
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
    },
    dragHandle: {
      width: '0.8em',
      height: '0.8em',
      marginRight: 5,
    },
    closeIcon: {
      fontSize: '0.8em',
      color: '#fff',
    },
    action: {
      alignSelf: 'unset',
      marginTop: 'unset',
    },
  })
);

export default DragCloseCardHeader;
