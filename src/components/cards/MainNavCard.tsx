import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { CardContent, Fab } from '@material-ui/core';
import { toggleMainNavCard, mainNavCardKeys } from 'redux/actions/localActions';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import Card, { CardProps } from '@material-ui/core/Card';
import DragCloseCardHeader from 'components/cards/generals/DragCloseCardHeader';
import Icon from '@material-ui/core/Icon';
import AddPageCard from './AddPageCard';
import AddItemCard from './AddItemCard';
import clsx from 'clsx';
import SelectThemeCard from './SelectThemeCard';
import LayersCard from './LayersCard';

const MainNavCard = (props: CardProps) => {
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const {
    isAddItemOpen,
    isSelectThemeOpen,
    isLayersCardOpen,
    isAddPageOpen,
    isBlockDragging,
    isDragging,
    project,
    page,
  } = localReducer;

  const classes = useStyles({
    isAddItemOpen,
    isBlockDragging: isBlockDragging || isDragging,
  });
  const dispatch = useDispatch();
  // const [isJustEnter, setIsJustEnter] = useState(false);

  const handleOpenCards = (cardKey: keyof typeof mainNavCardKeys) => () => {
    // if (!isJustEnter) {
    dispatch(toggleMainNavCard(cardKey));
    // }
  };

  // const handleHoverCards = (cardKey: keyof typeof mainNavCardKeys) => () => {
  //   if (!localReducer[cardKey]) {
  //     setIsJustEnter(true);
  //     dispatch(toggleMainNavCard(cardKey, true));
  //     setTimeout(() => {
  //       setIsJustEnter(false);
  //     }, 700);
  //   }
  // };

  return (
    <div className={classes.mainWrapper}>
      <div className={classes.cardWrapper}>
        <Card elevation={0} square className={classes.cardRoot} {...props}>
          <DragCloseCardHeader className={classes.cardHeader} hideClose />
          <CardContent className={classes.cardContent}>
            {page && (
              <>
                <Fab
                  aria-label='additem'
                  className={clsx(
                    classes.fab,
                    isAddItemOpen ? classes.fabActive : ''
                  )}
                  size='small'
                  onClick={handleOpenCards('isAddItemOpen')}
                  // onMouseEnter={handleHoverCards('isAddItemOpen')}
                >
                  <Icon>add</Icon>
                </Fab>
                <Fab
                  aria-label='selectTheme'
                  className={clsx(
                    classes.fab,
                    classes.fabTheme,
                    isSelectThemeOpen ? classes.fabActive : ''
                  )}
                  size='small'
                  onClick={handleOpenCards('isSelectThemeOpen')}
                  // onMouseEnter={handleHoverCards('isSelectThemeOpen')}
                >
                  <Icon>invert_colors</Icon>
                </Fab>
                <Fab
                  aria-label='layer'
                  className={clsx(
                    classes.fab,
                    classes.fabLayer,
                    isLayersCardOpen ? classes.fabActive : ''
                  )}
                  size='small'
                  onClick={handleOpenCards('isLayersCardOpen')}
                  // onMouseEnter={handleHoverCards('isLayersCardOpen')}
                >
                  <Icon>layers</Icon>
                </Fab>
              </>
            )}
            {project && (
              <Fab
                aria-label='addpage'
                className={clsx(
                  classes.fab,
                  isAddPageOpen ? classes.fabActive : ''
                )}
                size='small'
                onClick={handleOpenCards('isAddPageOpen')}
                // onMouseEnter={handleHoverCards('isAddPageOpen')}
              >
                <Icon>post_add</Icon>
              </Fab>
            )}
          </CardContent>
        </Card>
      </div>
      <AddPageCard />
      <AddItemCard />
      <SelectThemeCard />
      <LayersCard />
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    mainWrapper: {
      position: 'fixed',
      zIndex: 1101,
      left: 0,
      top: 49,
    },
    cardWrapper: {
      // display: (props: any) => (props.isBlockDragging ? 'none' : 'unset'),
    },
    cardRoot: {
      position: 'absolute',
      height: 'calc(100vh - 73px)',
      zIndex: 1101,
      opacity: (props: any) => (props.isAddItemOpen ? 1 : 0.99),
      '&:hover': {
        opacity: 1,
      },
    },
    cardHeader: {
      backgroundColor: theme.palette.secondary.main,
      padding: 0,
    },
    cardContent: {
      width: 88,
      padding: 16,
    },
    fab: {
      margin: theme.spacing(1),
      backgroundColor: 'rgba(117,117,117,0.9)',
      color: '#bdbdbd',
      '&:hover': {
        color: '#fff',
        backgroundColor: theme.palette.secondary.main,
      },
    },
    fabActive: {
      color: '#fff',
      backgroundColor: theme.palette.secondary.main,
    },
    fabTheme: {
      // backgroundColor: 'rgba(251,192,45,1)',
      // color: '#fff',
      // '&:hover': {
      //   backgroundColor: 'rgba(251,192,45,1)',
      // },
    },
    fabLayer: {
      // backgroundColor: 'rgba(117,117,117,1)',
      // color: '#fff',
      // '&:hover': {
      //   backgroundColor: 'rgba(117,117,117,1)',
      // },
    },
  })
);

export default MainNavCard;
