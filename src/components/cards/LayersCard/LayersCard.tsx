import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import ChildCard from '../generals/ChildCard';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { setIsLayersCardOpen } from 'redux/actions/localActions';
import TreeView from '@material-ui/lab/TreeView';
import Icon from '@material-ui/core/Icon';
import RecursiveTreeItem from './RecursiveTreeItem';

const LayersCard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { isLayersCardOpen, isBlockDragging, page } = useSelector<
    AppState,
    LocalReducerType
  >(state => state.localReducer);

  if (!page || !page.rootComponent) return null;

  return (
    <ChildCard
      isOpen={isLayersCardOpen && !isBlockDragging}
      onClickClose={() => dispatch(setIsLayersCardOpen(false))}
      cardContentProps={{
        className: classes.cardContent,
      }}
      isLayersCardOpen={isLayersCardOpen}
    >
      <div className={classes.wrapper}>
        <TreeView
          defaultCollapseIcon={<Icon>expand_more</Icon>}
          defaultExpandIcon={<Icon>chevron_right</Icon>}
        >
          {page.rootComponent.componentIds &&
            page.rootComponent.componentIds.map(componentKey => {
              const component = page.rootComponent!.components![componentKey];
              return (
                <RecursiveTreeItem
                  key={`recTreeItem-${componentKey}`}
                  id={componentKey}
                  component={component}
                  componentPath='rootComponent'
                />
              );
            })}
        </TreeView>
      </div>
    </ChildCard>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    cardContent: {
      padding: '0 !important',
    },
    wrapper: {
      padding: 24,
    },
  })
);

export default LayersCard;
