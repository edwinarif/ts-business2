import React from 'react';
import { GeneralComponentType } from 'types';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import TreeItem from '@material-ui/lab/TreeItem';
import { setCurrentSelectedComponent } from 'redux/actions/localActions';
import { getTitle } from 'utils/getTitle';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';

const RecursiveTreeItem = ({
  id,
  component,
  componentPath,
}: {
  id: string;
  component: GeneralComponentType;
  componentPath: string;
}) => {
  const dispatch = useDispatch();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const { renderedBlockComponents } = localReducer;

  const handleClick = () => {
    dispatch(
      setCurrentSelectedComponent(id, `${componentPath}.components.${id}`)
    );
  };

  if (!getIsBlockComponent(component)) {
    // TODO: CustomComponent Tree
    return null;
  }

  return component.componentIds ? (
    <TreeItem
      nodeId={id}
      label={getTitle(component, renderedBlockComponents)}
      onClick={handleClick}
    >
      {component.componentIds.map(componentKey => {
        const childComponent = component.components![componentKey];
        return (
          <RecursiveTreeItem
            key={`recTreeItem-${componentKey}`}
            id={componentKey}
            component={childComponent}
            componentPath={`${componentPath}.components.${id}`}
          />
        );
      })}
    </TreeItem>
  ) : (
    <TreeItem
      nodeId={id}
      label={getTitle(component, renderedBlockComponents)}
      onClick={handleClick}
    />
  );
};

export default RecursiveTreeItem;
