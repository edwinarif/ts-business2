import React, { useState, Dispatch, useEffect } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Typography, TextField, IconButton } from '@material-ui/core';
import isEmpty from 'lodash/isEmpty';
import clsx from 'clsx';
import { omit, stringParser } from 'utils/mixins';
import { useSelector } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import at from 'lodash/at';
import { GeneralComponentType } from 'types';
import { useFirebase } from 'react-redux-firebase';
import Icon from '@material-ui/core/Icon';
import isString from 'lodash/isString';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';

const ObjCss = () => {
  const firebase = useFirebase();
  const classes = useStyles();
  const {
    page,
    currentProjectId,
    currentPageId,
    currentSelectedComponentPath,
  } = useSelector<AppState, LocalReducerType>(state => state.localReducer);

  const component = at<any>(page, currentSelectedComponentPath)[0] as
    | GeneralComponentType
    | undefined;

  const [objCss, setObjCss] = useState<{ [key: string]: any }>({});
  const [newKey, setNewKey] = useState<string | undefined>(undefined);
  const [isBlur, setIsBlur] = useState(false);
  const [isEditingClassName, setIsEditingClassName] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [editKey, setEditKey] = useState<string | undefined>(undefined);
  const [editValueKey, setEditValueKey] = useState<string | undefined>(
    undefined
  );

  useEffect(() => {
    setObjCss(
      component && getIsBlockComponent(component) && component.objCss
        ? Object.keys(component.objCss).reduce(
            (prev, key) => ({
              ...prev,
              [key]: isString(component.objCss![key])
                ? `"${component.objCss![key].replace(/^"+|"+$/g, '')}"`
                : component.objCss![key],
            }),
            {} as any
          )
        : {}
    );
  }, [component]);

  if (!page || !component || !getIsBlockComponent(component)) return null;

  const objCssRef = firebase.ref(
    `projects/${currentProjectId}/pages/${currentPageId}/${currentSelectedComponentPath.replace(
      /\./g,
      '/'
    )}/objCss`
  );

  const setBlurTimeout = () => {
    setIsBlur(true);
    setTimeout(() => {
      setIsBlur(false);
    }, 500);
  };

  const handleCloseIsEditing = () => {
    setIsEditing(false);
    setBlurTimeout();
  };

  const handleCloseNewKey = (e: any) => {
    if (!isEmpty(e.target.value)) {
      const newObj = {
        ...objCss,
        [newKey!]: stringParser(e.target.value),
      };
      setObjCss(newObj);
      objCssRef.set(newObj);
    }

    setIsEditing(false);
    setNewKey(undefined);
    setBlurTimeout();
  };

  const handleOpenIsEditing = () => {
    if (!isEditing && !isBlur && !newKey && !isEditingClassName) {
      setIsEditing(true);
    }
    if (isEditingClassName) {
      setIsEditingClassName(false);
    }
    setIsBlur(false);
  };

  const handleKeyDown = (e: any) => {
    if ((e.key === 'Tab' || e.key === 'Enter') && !isEmpty(e.target.value)) {
      e.preventDefault();
      setNewKey(e.target.value);
    }
  };

  const handleKeyDownNewKey = (e: any) => {
    if ((e.key === 'Tab' || e.key === 'Enter') && !isEmpty(e.target.value)) {
      e.preventDefault();
      const newObj = {
        ...objCss,
        [newKey!]: stringParser(e.target.value),
      };
      setObjCss(newObj);
      objCssRef.set(newObj);
      setNewKey(undefined);
      if (e.key === 'Tab') {
        setIsEditing(true);
      } else {
        setIsEditing(false);
      }
    }
  };

  const handleEditKeyFn = (setFn: Dispatch<any>, key: string) => (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    if (isEditingClassName) {
      setIsEditingClassName(false);
    }
    setFn(key);
  };

  const pushEditKey = (key: string, newKey: string) => {
    if (!isEmpty(newKey)) {
      const oldValue = objCss[key];
      const newObj = {
        ...omit(objCss, [key]),
        [newKey]: oldValue,
      };
      setObjCss(newObj);
      objCssRef.set(newObj);
    } else {
      setObjCss(omit(objCss, [key]));
      objCssRef.set(omit(objCss, [key]));
    }
    setEditKey(undefined);
  };

  const handleKeyDownEditKey = (key: string) => (e: any) => {
    if ((e.key === 'Tab' || e.key === 'Enter') && !isEmpty(e.target.value)) {
      e.preventDefault();
      pushEditKey(key, e.target.value);
    }
  };

  const handleBlurEditKey = (key: string) => (e: any) => {
    pushEditKey(key, e.target.value);
    setBlurTimeout();
  };

  const pushEditKeyValue = (key: string, value: string) => {
    if (!isEmpty(value)) {
      setObjCss({ ...objCss, [key]: stringParser(value) });
      objCssRef.set({ ...objCss, [key]: stringParser(value) });
    } else {
      setObjCss(omit(objCss, [key]));
      objCssRef.set(omit(objCss, [key]));
    }
    setEditValueKey(undefined);
  };

  const handleKeyDownEditKeyValue = (key: string) => (e: any) => {
    if ((e.key === 'Tab' || e.key === 'Enter') && !isEmpty(e.target.value)) {
      e.preventDefault();
      pushEditKeyValue(key, e.target.value);
    }
  };

  const handleBlurEditKeyValue = (key: string) => (e: any) => {
    pushEditKeyValue(key, e.target.value);
    setBlurTimeout();
  };

  const handleDelete = (key: string) => (e: any) => {
    e.stopPropagation();
    setObjCss(omit(objCss, [key]));
    objCssRef.set(omit(objCss, [key]));
  };

  const classNameRef = firebase.ref(
    `projects/${currentProjectId}/pages/${currentPageId}/${currentSelectedComponentPath.replace(
      /\./g,
      '/'
    )}/className`
  );

  const handleKeyDownEditClassName = (e: any) => {
    if ((e.key === 'Tab' || e.key === 'Enter') && !isEmpty(e.target.value)) {
      e.preventDefault();
      classNameRef.set(e.target.value);
      setIsEditingClassName(false);
    }
  };

  const handleBlurEditClassName = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    if (!isEmpty(e.target.value)) {
      classNameRef.set(e.target.value);
    }
  };

  return (
    <div className={classes.root} onClick={handleOpenIsEditing}>
      {isEditingClassName ? (
        <>
          <TextField
            autoFocus
            variant='outlined'
            margin='dense'
            defaultValue={component.className}
            onBlur={handleBlurEditClassName}
            inputProps={{ className: classes.textField, size: 8 }}
            className={classes.textFieldLeft}
            onKeyDown={handleKeyDownEditClassName}
            onClick={(e: any) => {
              e.stopPropagation();
            }}
          />
          {': {'}
        </>
      ) : (
        <div
          onClick={(e: any) => {
            e.stopPropagation();
            setIsEditingClassName(true);
          }}
          className={classes.editClassName}
        >
          <Typography>
            {component.className}: {'{'}
          </Typography>
        </div>
      )}
      {Object.keys(objCss).map(key => (
        <div
          key={`csskey-${key}`}
          className={clsx(classes.mainList, classes.rootEdit)}
        >
          <div className={clsx(classes.mainContent, classes.rootEdit)}>
            {editKey === key ? (
              <TextField
                autoFocus
                variant='outlined'
                margin='dense'
                defaultValue={key}
                onBlur={handleBlurEditKey(key)}
                inputProps={{ className: classes.textField, size: 6 }}
                className={classes.textFieldLeft}
                onKeyDown={handleKeyDownEditKey(key)}
                onClick={(e: any) => {
                  e.stopPropagation();
                }}
              />
            ) : (
              <div
                onClick={handleEditKeyFn(setEditKey, key)}
                className={classes.rootEdit2}
              >
                {key}
              </div>
            )}
            :
            {editValueKey === key ? (
              <TextField
                autoFocus
                variant='outlined'
                margin='dense'
                defaultValue={objCss[key]}
                onBlur={handleBlurEditKeyValue(key)}
                inputProps={{ className: classes.textField, size: 6 }}
                className={classes.textFieldRight}
                onKeyDown={handleKeyDownEditKeyValue(key)}
                onClick={(e: any) => {
                  e.stopPropagation();
                }}
              />
            ) : (
              <div
                className={classes.textFieldRight}
                onClick={handleEditKeyFn(setEditValueKey, key)}
              >
                {objCss[key]}
              </div>
            )}
            ,
          </div>
          <IconButton
            aria-label='delete'
            size='small'
            className={classes.removeButton}
            onClick={handleDelete(key)}
          >
            <Icon fontSize='inherit'>close</Icon>
          </IconButton>
        </div>
      ))}
      {isEditing && (
        <div className={classes.rootEdit}>
          <TextField
            autoFocus
            variant='outlined'
            margin='dense'
            placeholder='key'
            onBlur={handleCloseIsEditing}
            inputProps={{ className: classes.textField, size: 6 }}
            className={classes.textFieldLeft}
            onKeyDown={handleKeyDown}
            onClick={(e: any) => {
              e.stopPropagation();
            }}
          />
          <div>:</div>
        </div>
      )}
      {newKey && (
        <div className={clsx(classes.rootEdit, classes.rootEdit2)}>
          <div>{`${newKey}: `}</div>
          <TextField
            autoFocus
            variant='outlined'
            margin='dense'
            placeholder='value'
            onBlur={handleCloseNewKey}
            inputProps={{ className: classes.textField, size: 6 }}
            className={classes.textFieldRight}
            onKeyDown={handleKeyDownNewKey}
            onClick={(e: any) => {
              e.stopPropagation();
            }}
          />
          <div> ,</div>
        </div>
      )}
      <div>
        <Typography display='inline'>}</Typography>
        <IconButton aria-label='add' size='small'>
          <Icon fontSize='inherit'>add_circle_outline</Icon>
        </IconButton>
      </div>
    </div>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {},
    rootEdit: {
      display: 'flex',
      alignItems: 'center',
    },
    rootEdit2: {
      marginLeft: 12,
    },
    textField: {
      paddingTop: 3.5,
      paddingBottom: 3.5,
    },
    textFieldLeft: {
      marginRight: 4,
      marginTop: 2,
      marginBottom: 2,
    },
    textFieldRight: {
      marginLeft: 4,
      marginTop: 2,
      marginBottom: 2,
    },
    mainList: {
      '&:hover > button': {
        display: 'block',
      },
    },
    mainContent: {
      flex: 1,
    },
    removeButton: {
      display: 'none',
      marginLeft: 12,
    },
    editClassName: {
      display: 'inline-block',
    },
  })
);

export default ObjCss;
