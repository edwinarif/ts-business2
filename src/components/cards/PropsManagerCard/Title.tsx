import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import at from 'lodash/at';
import { GeneralComponentType } from 'types';
import { Typography, IconButton, TextField } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import isEmpty from 'lodash/isEmpty';
import { useFirebase } from 'react-redux-firebase';
import { getTitle } from 'utils/getTitle';

const Title = () => {
  const classes = useStyles();
  const firebase = useFirebase();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const {
    page,
    currentProjectId,
    currentPageId,
    currentSelectedComponent,
    currentSelectedComponentPath,
    renderedBlockComponents,
  } = localReducer;

  const [isEdit, setIsEdit] = useState(false);
  useEffect(() => {
    setIsEdit(false);
  }, [currentSelectedComponent]);

  const component = at<any>(page, currentSelectedComponentPath)[0] as
    | GeneralComponentType
    | undefined;

  const labelRef = firebase.ref(
    `projects/${currentProjectId}/pages/${currentPageId}/${currentSelectedComponentPath.replace(
      /\./g,
      '/'
    )}/label`
  );

  const handleCloseNewKey = (e: any) => {
    if (!isEmpty(e.target.value)) {
      labelRef.set(e.target.value);
    }

    setIsEdit(false);
  };

  const handleKeyDown = (e: any) => {
    if ((e.key === 'Tab' || e.key === 'Enter') && !isEmpty(e.target.value)) {
      e.preventDefault();
      labelRef.set(e.target.value);
      setIsEdit(false);
    }
  };

  if (!component) return null;

  return isEdit ? (
    <TextField
      autoFocus
      variant='outlined'
      margin='dense'
      placeholder='value'
      defaultValue={getTitle(component, renderedBlockComponents)}
      onBlur={handleCloseNewKey}
      inputProps={{ className: classes.textField }}
      onKeyDown={handleKeyDown}
      onClick={(e: any) => {
        e.stopPropagation();
      }}
    />
  ) : (
    <Typography
      onClick={() => {
        setIsEdit(true);
      }}
      variant='h5'
      color='primary'
      display='block'
      align='right'
      className={classes.componentLabel}
    >
      <IconButton size='small' className={classes.button}>
        <Icon fontSize='inherit' color='primary' className={classes.editIcon}>
          edit
        </Icon>
      </IconButton>
      {getTitle(component, renderedBlockComponents, true)}
    </Typography>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    componentLabel: {
      fontWeight: 400,
      fontSize: '1.15rem',
      cursor: 'pointer',
      '&:hover > button': {
        visibility: 'visible',
      },
    },
    button: {
      marginRight: 8,
      visibility: 'hidden',
    },
    editIcon: {
      fontSize: 14,
    },
    textField: {
      paddingTop: 3.5,
      paddingBottom: 3.5,
    },
  })
);

export default Title;
