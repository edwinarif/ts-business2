import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { AppState } from 'index';
import componentsMapper, {
  ComponentPropsType,
} from 'blockConfig/componentsMapper';
import TextFieldType from 'components/dialogs/MuiComponentDialog/TextFieldType';
import SwitchType from 'components/dialogs/MuiComponentDialog/SwitchType';
import SelectType from 'components/dialogs/MuiComponentDialog/SelectType';
import { Grid, Divider } from '@material-ui/core';
import { useFirebase } from 'react-redux-firebase';
import at from 'lodash/at';
import generatePropsFromDefault from 'utils/generatePropsFromDefault';
import { GeneralComponentType } from 'types';
import { LocalReducerType } from 'redux/reducers/localReducer';
import debounce from 'lodash/debounce';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';

const PropsManager = () => {
  const firebase = useFirebase();
  const classes = useStyles();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );

  const {
    currentSelectedComponent,
    currentSelectedComponentPath,
    currentProjectId,
    currentPageId,
    page,
  } = localReducer;

  const [propsConfig, setPropsConfig] = useState<any>(undefined);
  const [configKeys, setConfigKeys] = useState<any>();
  const [propsState, setPropsState] = useState<any>();
  const [propsManagerText, setPropsManagerText] = useState<any>();

  const [updateValue] = useState<any>(() =>
    debounce(
      (
        propsId,
        value,
        currentProjectId,
        currentPageId,
        currentSelectedComponentPath
      ) => {
        firebase
          .ref(
            `projects/${currentProjectId}/pages/${currentPageId}/${currentSelectedComponentPath.replace(
              /\./g,
              '/'
            )}/props/${propsId}`
          )
          .set(value);
      },
      1000
    )
  );

  useEffect(() => {
    setPropsManagerText(undefined);
    const firebaseComponentBase = at<any>(page, currentSelectedComponentPath);
    const firebaseComponent =
      firebaseComponentBase.length > 0
        ? (firebaseComponentBase[0] as GeneralComponentType)
        : undefined;

    if (!firebaseComponent) return;
    if (!getIsBlockComponent(firebaseComponent)) {
      setPropsConfig(undefined);
      setConfigKeys(undefined);
      setPropsState(undefined);
      setPropsManagerText(undefined);
      return;
    }

    if (!firebaseComponent.blockType) return;
    const componentProps = generatePropsFromDefault(
      firebaseComponent.blockType,
      firebaseComponent.props
    ) as ComponentPropsType;
    if (componentsMapper[firebaseComponent.blockType].propsConfig) {
      const propsConfig = componentsMapper[firebaseComponent.blockType]
        .propsConfig!;
      const configKeys = Object.keys(propsConfig) as Array<
        keyof typeof propsConfig
      >;
      const propsState = configKeys.reduce(
        (prev, curr) => ({
          ...prev,
          [curr]:
            componentProps[curr] !== undefined
              ? componentProps[curr]
              : (propsConfig[curr]! as any).default,
        }),
        {} as any
      );

      setPropsConfig(propsConfig);
      setConfigKeys(configKeys);
      setPropsState(propsState);

      setPropsManagerText(
        configKeys.reduce((prev, key) => {
          if ((propsConfig[key]! as any).inputType === 'TextField') {
            return {
              ...prev,
              [key]: propsState[key],
            };
          }
          return prev;
        }, {} as any)
      );
    } else {
      setPropsConfig(undefined);
    }
  }, [currentSelectedComponent, currentSelectedComponentPath, page]);

  if (!currentSelectedComponent || !propsConfig || !configKeys) return null;

  const getPath = (id: string) =>
    `projects/${currentProjectId}/pages/${currentPageId}/${currentSelectedComponentPath.replace(
      /\./g,
      '/'
    )}/props/${id}`;

  const handleChange = (id: string) => (event: any) => {
    firebase.set(getPath(id), event.target.value);
  };

  const handleChecked = (id: string) => (
    event: React.ChangeEvent,
    value: boolean
  ) => {
    firebase.set(getPath(id), value);
  };

  const handleChangeText = (id: string, type?: string) => (e: any) => {
    const val = type === 'number' ? Number(e.target.value) : e.target.value;
    setPropsManagerText({
      ...propsManagerText,
      [id]: val,
    });
    updateValue(
      id,
      val,
      currentProjectId,
      currentPageId,
      currentSelectedComponentPath
    );
  };

  return (
    <>
      <Grid container className={classes.container}>
        {configKeys.map(
          (key: any) =>
            propsConfig[key] &&
            propsConfig[key].inputType && (
              <div key={`item-${key}`} className={classes.rootItem}>
                {propsConfig[key].inputType === 'TextField' && (
                  <Grid item className={classes.gridTextField}>
                    {propsManagerText && (
                      <TextFieldType
                        fieldId={key as string}
                        value={(propsManagerText[key] as string) || ''}
                        label={propsConfig[key].label}
                        handleChange={handleChangeText}
                        type={propsConfig[key].type || 'text'}
                      />
                    )}
                  </Grid>
                )}
                {propsConfig[key].inputType === 'Switch' && (
                  <Grid item className={classes.gridSwitch}>
                    <SwitchType
                      key={`item-${key}`}
                      fieldId={key as string}
                      checked={propsState[key] as boolean}
                      label={propsConfig[key].label}
                      onChange={handleChecked}
                    />
                  </Grid>
                )}
                {propsConfig[key].inputType === 'Select' && (
                  <Grid item className={classes.gridSelect}>
                    <SelectType
                      key={`item-${key}`}
                      fieldId={key as string}
                      value={
                        propsState[key] === undefined ||
                        propsState[key] === false
                          ? ''
                          : propsState[key]
                      }
                      label={propsConfig[key].label}
                      onChange={handleChange}
                      options={propsConfig[key].options || []}
                    />
                  </Grid>
                )}
              </div>
            )
        )}
      </Grid>
      <Divider />
    </>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    container: {
      marginTop: 18,
    },
    rootItem: {
      marginRight: 18,
    },
    gridTextField: {
      marginBottom: 18,
    },
    gridSwitch: {},
    gridSelect: {
      marginBottom: 18,
    },
  })
);

export default PropsManager;
