import React, { useState } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import at from 'lodash/at';
import { GeneralComponentType } from 'types';
import { Button } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import SaveAsComponentDialog from 'components/dialogs/SaveAsComponentDialog';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';

const SaveAsComponent = () => {
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const { page, currentSelectedComponentPath } = localReducer;

  const component = at<any>(page, currentSelectedComponentPath)[0] as
    | GeneralComponentType
    | undefined;

  const classes = useStyles();
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  if (!component || !getIsBlockComponent(component) || !component.componentIds)
    return null;

  return (
    <>
      <div className={classes.root}>
        <Button
          startIcon={<Icon>save</Icon>}
          variant='outlined'
          size='small'
          onClick={() => setIsDialogOpen(true)}
        >
          Save As Component
        </Button>
      </div>
      <SaveAsComponentDialog
        isOpen={isDialogOpen}
        handleClose={() => setIsDialogOpen(false)}
      />
    </>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: 5,
    },
  })
);

export default SaveAsComponent;
