import React from 'react';
import { Card, CardContent, Divider } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import PropsManager from './PropsManager';
import TabPanel from 'components/TabPanel';
import ObjCss from './ObjCss';
// import StrCss from './StrCss';
import Title from './Title';
import SaveAsComponent from './SaveAsComponent';

const PropsManagerCard = () => {
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const {
    isPropsManagerOpen,
    propsManagerTabIdx,
    page,
    project,
    currentSelectedComponent,
  } = localReducer;

  const classes = useStyles({ isPropsManagerOpen });

  return (
    <div className={classes.root}>
      <Card elevation={0} square className={classes.cardRoot}>
        <CardContent className={classes.content}>
          <>
            <TabPanel value={propsManagerTabIdx} index={0}>
              <div className={classes.contentWrapper}>
                {project &&
                  page &&
                  currentSelectedComponent &&
                  currentSelectedComponent !== 'rootComponent' && (
                    <>
                      <Title />
                      <SaveAsComponent />
                      <Divider />
                      <PropsManager />
                      <ObjCss />
                      {/* <Divider />
                      <StrCss /> */}
                    </>
                  )}
              </div>
            </TabPanel>
          </>
        </CardContent>
      </Card>
    </div>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      position: 'absolute',
      top: 49,
      right: 0,
      zIndex: 1101,
      width: 286,
    },
    cardRoot: {},
    header: {
      backgroundColor: 'rgba(238,238,238,1)',
      paddingTop: 10,
      paddingBottom: 4,
    },
    contentWrapper: {
      padding: 24,
      paddingTop: 18,
    },
    content: {
      height: 'calc(100vh - 73px)',
      overflowY: 'auto',
      padding: 0,
    },
    layerWrapper: {
      paddingTop: 24,
    },
    tabRoot: {
      minWidth: 150,
    },
  })
);

export default PropsManagerCard;
