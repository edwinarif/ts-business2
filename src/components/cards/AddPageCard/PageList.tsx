import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Icon from '@material-ui/core/Icon';
import { ListItemSecondaryAction, IconButton } from '@material-ui/core';
import { IProject } from 'types';
import { useDispatch } from 'react-redux';
import { setCurrentPage, setIsAddPageOpen } from 'redux/actions/localActions';

const PageList = ({ project }: { project?: IProject }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const pageKeys = Object.keys(project && project.pages ? project.pages : {});

  const handleClick = (pageId: string) => () => {
    if (project) {
      const page = project.pages![pageId];
      dispatch(setCurrentPage(pageId, page));
      dispatch(setIsAddPageOpen(false));
    }
  };

  return (
    <List className={classes.root} dense>
      {pageKeys.map(key => (
        <ListItem button key={`pageItem-${key}`} onClick={handleClick(key)}>
          <ListItemAvatar className={classes.avatar}>
            <Icon>insert_drive_file</Icon>
          </ListItemAvatar>
          <ListItemText
            primary={project!.pages![key].name}
            primaryTypographyProps={{
              variant: 'h5',
              className: classes.pageName,
            }}
            secondary={`Last update: ${new Date(
              project!.pages![key].updatedAt as number
            ).toUTCString()}`}
            secondaryTypographyProps={{
              className: classes.secondaryText,
            }}
          />
          <ListItemSecondaryAction>
            <IconButton edge='end' aria-label='more'>
              <Icon>more_vert</Icon>
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
      minHeight: 100,
      paddingTop: 16,
      paddingBottom: 16,
    },
    avatar: {
      color: 'rgba(117,117,117,0.6)',
      minWidth: 42,
    },
    pageName: {
      color: theme.palette.primary.main,
    },
    secondaryText: {
      fontSize: '0.75rem',
    },
  })
);

export default PageList;
