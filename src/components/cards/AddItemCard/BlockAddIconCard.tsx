import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { AppState } from 'index';
import { useSelector, useDispatch } from 'react-redux';
import { setIsAddBlockItemOpen } from 'redux/actions/localActions';
import { Grid, Typography, Divider } from '@material-ui/core';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';
import ChildCard from '../generals/ChildCard';
import { blockCategories } from 'blockConfig/componentsMapper';
import clsx from 'clsx';
import categoriesArr from 'blockConfig/icons/categoriesArr';
import decamelize from 'decamelize';
// import BlockIconDrag from 'components/editorDnd/BlockIconDrag';

const BlockAddIconCard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const {
    page,
    isAddBlockItemOpen,
    isBlockDragging,
    isDragging,
    currentBlockItemCategory,
  } = localReducer;

  if (!page || !currentBlockItemCategory) return null;

  const category = blockCategories[currentBlockItemCategory];

  const categoryBlocks = Object.keys(categoriesArr).reduce(
    (res, subCategoryId) => {
      return {
        ...res,
        [subCategoryId]: ((categoriesArr as any)[subCategoryId]
          .icons as any).map((iconData: any) => ({
          ...iconData,
          snakeCase: decamelize(iconData.name),
        })),
      };
    },
    {} as { [subCategory: string]: any }
  );

  const subCategories = Object.keys(categoryBlocks).map(categoryBlockId =>
    categoryBlockId === 'noCategory'
      ? { id: 'noCategory', title: '' }
      : {
          id: categoryBlockId,
          title: category.subCategories![categoryBlockId].title,
        }
  );

  return (
    <ChildCard
      isOpen={
        isAddBlockItemOpen &&
        currentBlockItemCategory !== 'customComponents' &&
        !isBlockDragging &&
        !isDragging
      }
      onClickClose={() => dispatch(setIsAddBlockItemOpen(false))}
      cardLeftPosition={370}
      cardWidth={450}
      title={category.title}
    >
      <div className={classes.contentWrapper}>
        <h1>Icons</h1>
        {subCategories &&
          subCategories.map(subCategory => (
            <React.Fragment key={`subCategory-${subCategory.id}`}>
              {subCategory.id !== 'noCategory' ? (
                <Typography variant='h5' className={classes.subCategoryTitle}>
                  {subCategory.title}
                </Typography>
              ) : (
                <div className={classes.noCategory} />
              )}
              <Grid container spacing={0}>
                {categoryBlocks[subCategory.id].map((block: any) => (
                  <Grid
                    item
                    key={`block-${block.id}`}
                    alignItems='center'
                    className={clsx(
                      getIsBlockComponent(block)
                        ? block.props
                          ? (block.props as any).fullWidth
                            ? classes.fullWidthGrid
                            : undefined
                          : undefined
                        : undefined
                    )}
                  >
                    {/* <BlockIconDrag iconData={block} /> */}
                    <Typography
                      variant='caption'
                      gutterBottom
                      paragraph
                      align='center'
                      display='block'
                      className={classes.blockLabel}
                    >
                      {block.name}
                    </Typography>
                  </Grid>
                ))}
              </Grid>
              <Divider className='divider' />
            </React.Fragment>
          ))}
      </div>
    </ChildCard>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    contentWrapper: {
      padding: '16px 24px',
    },
    fullWidthGrid: {
      width: '100%',
    },
    blockLabel: {
      marginTop: 8,
      marginLeft: 12,
      marginRight: 12,
    },
    subCategoryTitle: {
      marginBottom: 24,
      color: 'rgba(0,0,0,0.75)',
    },
    divider: {
      marginTop: 24,
      marginBottom: 24,
    },
    noCategory: {
      paddingTop: 12,
    },
  })
);

export default BlockAddIconCard;
