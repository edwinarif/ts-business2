import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { AppState } from 'index';
import { useSelector, useDispatch } from 'react-redux';
import { setIsAddBlockItemOpen } from 'redux/actions/localActions';
import { Grid, Typography } from '@material-ui/core';
import BlockDrag from 'components/editorDnd/BlockDrag';
import { LocalReducerType } from 'redux/reducers/localReducer';
import isEmpty from 'lodash/isEmpty';
import { getIsBlockComponent } from 'utils/getIsBlockComponent';
import ChildCard from '../generals/ChildCard';

const CustomAddItemCard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const {
    page,
    isAddBlockItemOpen,
    isBlockDragging,
    isDragging,
    currentBlockItemCategory,
    renderedCustomComponents,
  } = localReducer;

  if (!page || isEmpty(renderedCustomComponents)) return null;

  const customComponents = Object.keys(renderedCustomComponents).map(
    key => renderedCustomComponents[key]
  );

  return (
    <ChildCard
      isOpen={
        isAddBlockItemOpen &&
        currentBlockItemCategory === 'customComponents' &&
        !isBlockDragging &&
        !isDragging
      }
      onClickClose={() => dispatch(setIsAddBlockItemOpen(false))}
      cardLeftPosition={370}
      cardWidth={450}
      title={'Custom Components'}
    >
      <div className={classes.contentWrapper}>
        <Grid container spacing={2}>
          {customComponents.map(block => (
            <Grid
              item
              key={`block-${block.id}`}
              alignItems='center'
              className={classes.blockGrid}
            >
              <div>
                <BlockDrag renderedBlockComponent={block} />
              </div>
              <Typography
                variant='caption'
                gutterBottom
                paragraph
                align='center'
                className={classes.blockLabel}
              >
                {getIsBlockComponent(block) ? block.label : ''}
              </Typography>
            </Grid>
          ))}
        </Grid>
      </div>
    </ChildCard>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    contentWrapper: {
      padding: 24,
    },
    blockGrid: {
      display: 'flex',
      flexDirection: 'column',
    },
    blockLabel: {
      marginTop: 8,
    },
  })
);

export default CustomAddItemCard;
