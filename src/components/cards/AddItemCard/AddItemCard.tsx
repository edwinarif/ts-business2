import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { AppState } from 'index';
import { useSelector, useDispatch } from 'react-redux';
import { setIsAddItemOpen, setKeyValue } from 'redux/actions/localActions';
import ChildCard from '../generals/ChildCard';
import { List, ListSubheader, ListItem, ListItemText } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import { blockCategories, BlockCategories } from 'blockConfig/componentsMapper';
import BlockAddItemCard from './BlockAddItemCard';
import CustomAddItemCard from './CustomAddItemCard';
import BlockAddIconCard from './BlockAddIconCard';

const AddItemCard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {
    isAddItemOpen,
    isBlockDragging,
    isDragging,
    currentBlockItemCategory,
  } = useSelector<AppState, LocalReducerType>(state => state.localReducer);

  const categories = Object.keys(blockCategories) as BlockCategories[];

  const handleCategoryClick = (category: BlockCategories) => () => {
    dispatch(
      setKeyValue({
        key: 'currentBlockItemCategory',
        value: category,
      })
    );
    dispatch(
      setKeyValue({
        key: 'isAddBlockItemOpen',
        value: true,
      })
    );
  };

  return (
    <>
      <ChildCard
        isOpen={isAddItemOpen && !isBlockDragging && !isDragging}
        onClickClose={() => dispatch(setIsAddItemOpen(false))}
        cardWidth={275}
        isAddItemOpen={isAddItemOpen}
      >
        <div className={classes.contentWrapper}>
          <List
            component='nav'
            aria-labelledby='nested-list-subheader'
            subheader={
              <ListSubheader
                component='div'
                id='nested-list-subheader'
                className={classes.listSubheader}
              >
                Component Type:
              </ListSubheader>
            }
          >
            {categories.map(category => (
              <ListItem
                key={`blockCategory-${category}`}
                button
                dense
                onClick={handleCategoryClick(category)}
                // onMouseEnter={handleCategoryClick(category)}
                className={classes.listItem}
              >
                <ListItemText
                  primary={blockCategories[category].title}
                  primaryTypographyProps={{
                    variant: 'h6',
                  }}
                  secondary={blockCategories[category].subTitle}
                  classes={{
                    primary: classes.listItemTextPrimary,
                    secondary: classes.listItemTextSecondary,
                  }}
                />
                <Icon>chevron_right</Icon>
              </ListItem>
            ))}
          </List>
        </div>
      </ChildCard>
      {currentBlockItemCategory === 'icons' ? (
        <BlockAddIconCard />
      ) : (
        <BlockAddItemCard />
      )}
      <CustomAddItemCard />
    </>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    contentWrapper: {
      padding: 0,
    },
    listSubheader: {
      lineHeight: 'unset',
      paddingLeft: 24,
      backgroundColor: '#fff',
      paddingTop: 16,
      paddingBottom: 12,
    },
    listItem: {
      paddingLeft: 24,
    },
    listItemTextPrimary: {
      color: theme.palette.primary.main,
    },
    listItemTextSecondary: {
      fontSize: '0.75rem',
    },
  })
);

export default AddItemCard;
