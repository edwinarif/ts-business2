import React, { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { setIsSelectThemeOpen, setMerge } from 'redux/actions/localActions';
import ChildCard from 'components/cards/generals/ChildCard';
import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
} from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import { useFirebase } from 'react-redux-firebase';
import PaletteColors from './PaletteColors';
import { Palette } from '@material-ui/core/styles/createPalette';

const THEME_IDS_CHANGE_LATER = ['theme1', 'theme2'];

const SelectThemeCard = () => {
  const classes = useStyles();
  const firebase = useFirebase();
  const dispatch = useDispatch();
  const {
    isSelectThemeOpen,
    isBlockDragging,
    themes,
    currentProjectId,
    currentPageId,
  } = useSelector<AppState, LocalReducerType>(state => state.localReducer);

  useEffect(() => {
    THEME_IDS_CHANGE_LATER.forEach((id: string) => {
      if (!themes || !themes[id]) {
        firebase.ref(`/themes/${id}`).once('value', snapshot => {
          dispatch(
            setMerge({
              key: 'themes',
              value: {
                [id]: {
                  ...snapshot.val(),
                  id,
                },
              },
            })
          );
        });
      }
    });
  }, [dispatch, themes, firebase]);

  const handleClick = (id: string) => () => {
    firebase
      .ref(`/projects/${currentProjectId}/pages/${currentPageId}/themeId`)
      .set(id);
    dispatch(setIsSelectThemeOpen(false));
  };

  return (
    <ChildCard
      isOpen={isSelectThemeOpen && !isBlockDragging}
      onClickClose={() => dispatch(setIsSelectThemeOpen(false))}
      cardContentProps={{
        className: classes.cardContent,
      }}
      isSelectThemeOpen={isSelectThemeOpen}
    >
      <div className={classes.wrapper}>
        <List className={classes.list}>
          {Object.keys(themes || {}).map(id => (
            <ListItem
              button
              key={`theme-${id}`}
              onClick={handleClick(id)}
              className={classes.listItem}
            >
              <ListItemText
                primary={
                  <div className={classes.title}>{themes![id].name}</div>
                }
                primaryTypographyProps={{
                  variant: 'h5',
                }}
                secondary={
                  <PaletteColors
                    palette={(themes![id].config as any).palette as Palette}
                  />
                }
                disableTypography
              />
              <ListItemSecondaryAction>
                <IconButton
                  edge='end'
                  aria-label='edit'
                  className={classes.iconButtonRight}
                >
                  <Icon>edit</Icon>
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </div>
    </ChildCard>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cardContent: {
      padding: 0,
      paddingTop: 12,
    },
    wrapper: {},
    list: {
      width: '100%',
      minHeight: 100,
      paddingTop: 0,
      paddingBottom: 0,
    },
    listItem: {
      paddingLeft: 24,
    },
    iconButtonRight: {
      marginRight: -4,
    },
    title: {
      color: theme.palette.primary.main,
      fontFamily: "'Noto Serif', serif",
      fontSize: '0.95rem',
      fontWeight: 400,
      letterSpacing: '0.05rem',
    },
  })
);

export default SelectThemeCard;
