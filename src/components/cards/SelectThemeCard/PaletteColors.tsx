import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Palette } from '@material-ui/core/styles/createPalette';
import { Typography } from '@material-ui/core';

const PaletteColors = ({ palette }: { palette: Palette }) => {
  const classes = useStyles();

  if (!palette) return null;

  return (
    <div className={classes.root}>
      <Typography variant='caption'>Colors: </Typography>
      {palette.primary && (
        <>
          {palette.primary.main && (
            <div
              className={classes.boxColor}
              style={{ backgroundColor: palette.primary.main }}
            />
          )}
          {palette.secondary.main && (
            <div
              className={classes.boxColor}
              style={{ backgroundColor: palette.secondary.main }}
            />
          )}
        </>
      )}
    </div>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 8,
    },
    boxColor: {
      marginLeft: 8,
      width: 18,
      height: 18,
      boxShadow: '1px 1px 4px 0px rgba(119,119,119,1)',
    },
  })
);

export default PaletteColors;
