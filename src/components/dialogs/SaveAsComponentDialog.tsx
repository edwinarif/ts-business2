import React from 'react';
import { createStyles, makeStyles, useTheme } from '@material-ui/core/styles';
import {
  Button,
  TextField,
  useMediaQuery,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import at from 'lodash/at';
import { GeneralComponentType, CustomComponentType } from 'types';
import Icon from '@material-ui/core/Icon';
import ListRecursive from 'components/ListRecursive';
import { useForm } from 'react-hook-form';
import { useFirebase } from 'react-redux-firebase';
import {
  setCurrentSelectedComponent,
  setMerge,
} from 'redux/actions/localActions';
import { parseCustomComponent } from 'utils/parseCustomComponent';

type SaveAsComponentDialogProps = {
  isOpen: boolean;
  handleClose: () => void;
};

const SaveAsComponentDialog = ({
  isOpen,
  handleClose,
}: SaveAsComponentDialogProps) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const firebase = useFirebase();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const { register, handleSubmit, errors } = useForm();

  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const {
    page,
    currentSelectedComponentPath,
    currentSelectedComponent,
    currentProjectId,
    currentPageId,
    renderedBlockComponents,
    project,
    themes,
  } = localReducer;

  const component = at<any>(page, currentSelectedComponentPath)[0] as
    | GeneralComponentType
    | undefined;

  if (!page || !component || !project) return null;

  const onSave = (data: any) => {
    const componentName = data.componentName;
    const componentRef = firebase.ref(
      `projects/${currentProjectId}/customComponents`
    );
    const newComponentRef = componentRef.push();
    newComponentRef
      .set({
        componentName,
        createdAt: Date.now(),
        componentIds: [currentSelectedComponent],
        components: {
          [currentSelectedComponent]: component,
        },
      })
      .then(() => {
        if (newComponentRef.key) {
          const parentPath = `projects/${currentProjectId}/pages/${currentPageId}/${currentSelectedComponentPath.replace(
            `.components.${currentSelectedComponent}`,
            ''
          )}`;
          const componentsRef = firebase.ref(
            `${parentPath.replace(/\./g, '/')}/components`
          );
          const newComponentsRef = componentsRef.push();
          newComponentsRef
            .set({
              customComponentId: newComponentRef.key,
              label: componentName,
            })
            .then(() => {
              if (newComponentsRef.key) {
                dispatch(
                  setCurrentSelectedComponent(
                    newComponentsRef.key,
                    currentSelectedComponentPath.replace(
                      currentSelectedComponent,
                      newComponentsRef.key
                    )
                  )
                );

                const parentRef = firebase.ref(parentPath.replace(/\./g, '/'));
                parentRef.transaction(parent => {
                  if (parent) {
                    parent.componentIds = parent.componentIds.map(
                      (componentId: any) => {
                        if (componentId === currentSelectedComponent)
                          return newComponentsRef.key;
                        return componentId;
                      }
                    );
                    parent.components[currentSelectedComponent] = null;
                  }

                  return parent;
                });
              }
            });

          const customComponent = {
            id: newComponentRef.key,
            componentName,
            componentIds: [currentSelectedComponent],
            components: {
              [currentSelectedComponent]: component,
            },
          } as CustomComponentType;
          const currentThemeConfig = themes![page.themeId].config;
          const renderedComponent = parseCustomComponent({
            customComponent,
            themeConfig: currentThemeConfig,
            customComponents: project.customComponents,
          });
          dispatch(
            setMerge({
              key: 'renderedCustomComponents',
              value: {
                [newComponentRef.key]: renderedComponent,
              },
            })
          );
        }
      });
  };

  return (
    <Dialog fullScreen={fullScreen} open={isOpen}>
      <DialogTitle id='form-dialog-title' className={classes.title}>
        Save As Component
      </DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin='dense'
          id='componentName'
          label='Component Name'
          fullWidth
          InputProps={{
            inputRef: register({ required: true }),
            name: 'componentName',
          }}
          error={errors && errors.componentName ? true : false}
          helperText={errors && errors.componentName ? '* Required' : undefined}
        />
        <DialogContentText className={classes.componentTreeTitle}>
          Component Tree:
        </DialogContentText>
        <ListRecursive
          component={component}
          blocks={renderedBlockComponents}
          deepLevel={1}
        />
      </DialogContent>
      <DialogActions className={classes.dialogActions}>
        <Button onClick={handleClose} color='primary'>
          Cancel
        </Button>
        <Button
          onClick={handleSubmit(onSave)}
          color='primary'
          startIcon={<Icon>save</Icon>}
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    title: {
      paddingBottom: 0,
    },
    componentTreeTitle: {
      marginTop: 20,
    },
    listItemIcon: {
      minWidth: 32,
    },
    dialogActions: {
      padding: 18,
    },
  })
);

export default SaveAsComponentDialog;
