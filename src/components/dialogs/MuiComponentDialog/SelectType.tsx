import React from 'react';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

type SelectTypeProps = {
  fieldId: string;
  value: any;
  options: Array<any>;
  label?: string;
  onChange: (id: string) => (event: any) => void;
};

const SelectType = ({
  fieldId,
  value,
  options,
  label,
  onChange,
}: SelectTypeProps) => {
  return (
    <FormControl>
      <InputLabel htmlFor='variant'>{label || fieldId}</InputLabel>
      <Select value={value} onChange={onChange(fieldId)}>
        {options.map((option, idx) => (
          <MenuItem key={`option-${fieldId}-${idx}`} value={option}>
            {option}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default SelectType;
