import React from 'react';
import { FormControlLabel, Switch } from '@material-ui/core';

type SwitchTypeProps = {
  fieldId: string;
  checked: boolean;
  label?: string;
  onChange: (id: string) => (event: any, value: boolean) => void;
};

const SwitchType = ({ fieldId, checked, label, onChange }: SwitchTypeProps) => {
  return (
    <FormControlLabel
      control={<Switch checked={checked} onChange={onChange(fieldId)} />}
      label={label || fieldId}
      color='primary'
    />
  );
};

export default SwitchType;
