import React from 'react';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';

type TextFieldTypeProps = {
  fieldId: string;
  handleChange: (id: string, type?: string) => (event: any) => void;
};

const TextFieldType = ({
  fieldId,
  label,
  handleChange,
  type,
  ...textFieldProps
}: TextFieldProps & TextFieldTypeProps) => {
  return (
    <TextField
      id={`textField-${fieldId}`}
      label={label || fieldId}
      onChange={handleChange(fieldId, type)}
      type={type || 'text'}
      {...textFieldProps}
    />
  );
};

export default TextFieldType;
