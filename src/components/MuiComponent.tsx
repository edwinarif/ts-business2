import React from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
import clsx from 'clsx';
import { ComponentMapBase } from 'blockConfig/componentsMapper';

type MuiComponentProps = {
  props?: any;
} & ComponentMapBase;

const MuiComponent = ({ Component, style, props }: MuiComponentProps) => {
  const classBase = makeStyles(() =>
    createStyles({ addRoot: style ? style : {} })
  )();
  const classes = useStyles();

  const className = clsx(classes.root, classBase.addRoot);

  return <Component {...props} className={className} />;
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      minHeight: 24,
      minWidth: 24,
    },
  })
);

export default MuiComponent;
